//
//  GLProgManager.h
//  OSSG
//
//  Created by Guilherme R. Lampert on 29/04/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "OSSG.h"

// ======================================================
// Helpers:
// ======================================================

// Vertex attribute pair <mane string, index>
typedef struct sVertexAttrib {
	const char * name;
	int index;
} VertexAttrib;

// Custom built-in shade programs used by the app.
// Once you add a new entry here, make sure you update the list on the corresponding .m
typedef enum eGLProgId {
	GLPROG_COLOR_REPLACE,
	GLPROG_FADE_SCREEN,
	GLPROG_BACKGROUND_OBJECTS,
	GLPROG_BACKGROUND_IMAGE,
	NUM_GLPROGS
} GLProgId;

// ======================================================
// GLProgManager interface:
// ======================================================

@interface GLProgManager : NSObject

- (id)    init;
- (void)  enableProgram: (GLProgId) prog;
- (void)  restore; // Restore previous GL program for Cocos2d to proceed.

// Get/set shader program uniforms for currently enabled program:
- (GLint) getProgramParamHandle: (GLProgId) prog paramName: (NSString *) name;
- (BOOL)  setProgramParam: (GLint) paramId intValue:   (int) val;
- (BOOL)  setProgramParam: (GLint) paramId floatValue: (float) val;
- (BOOL)  setProgramParam: (GLint) paramId floatVec4:  (const float *) val;
- (BOOL)  setProgramParam: (GLint) paramId floatVec3:  (const float *) val;
- (BOOL)  setProgramParam: (GLint) paramId floatMat4:  (const float *) val;

@end
