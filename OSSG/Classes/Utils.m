//
//  Utils.h
//  Low-level C and system utilities.
//
//  Created by Guilherme R. Lampert on 27/03/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import <stdio.h>          // Standard C IO
#import <stdlib.h>         // Standard C library
#import <errno.h>          // 'errno' and error codes
#import <sys/types.h>      // System types (pid_t, etc)
#import <sys/stat.h>       // stat/fstat
#import <time.h>           // time()/time_t
#import <mach/mach_time.h> // mach_absolute_time()/mach_timebase_info()
#import <Foundation/Foundation.h>
#import <TargetConditionals.h>
#import "OSSG.h"

// ======================================================

// Time counters for AbsoluteTimeMillisecs:
static int64_t clockFreq;
static int64_t baseTime;

// GL VBO used by DrawFullScreenQuad:
static GLuint vbo;

// ======================================================
// SystemCommonInit(): Initializes low-level system stuff we need.
// ======================================================

void SystemCommonInit(void)
{
	// Init the time counters:
	{
		baseTime = (int64_t)mach_absolute_time();

		// Get the clock frequency once, at startup.
		// This value doesn't change while the system is running.
		mach_timebase_info_data_t timeBaseInfo;
		const kern_return_t result = mach_timebase_info(&timeBaseInfo);

		if (result == KERN_SUCCESS)
		{
			clockFreq = (timeBaseInfo.numer / timeBaseInfo.denom);
		}
		else
		{
			OSSG_ERROR(@"High resolution clock is unavailable!");
			clockFreq = 1;
		}
	}

	// Create VBO for a full screen quadrilateral:
	{
		const float verts[] = {
			// First triangle:
			 1.0,  1.0,
			-1.0,  1.0,
			-1.0, -1.0,
			// Second triangle:
			-1.0, -1.0,
			 1.0, -1.0,
			 1.0,  1.0
		};
		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	OSSG_LOG(@"System intialization completed.");
}

// ======================================================
// DrawFullScreenQuad(): Draw a GL quad that fills the screen.
// ======================================================

void DrawFullScreenQuad(void)
{
	// Bind:
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, NULL);

	// Draw 6 vertexes => 2 triangles:
	glDrawArrays(GL_TRIANGLES, 0, 6);

	// Cleanup:
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Increment so that Cocos2d stats overlay gets updated:
	CC_INCREMENT_GL_DRAWS(1);
}

// ======================================================
// ClampF(): Clamp input between min/max range.
// ======================================================

float ClampF(float x, float minimum, float maximum)
{
	return (x < minimum) ? minimum : (x > maximum) ? maximum : x;
}

// ======================================================
// MapRangeF(): Maps a decimal range to another.
// ======================================================

float MapRangeF(float x, float inMin, float inMax, float outMin, float outMax)
{
	return (x - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
}

// ======================================================
// EqualsWithinTol(): Test for FP equivalence.
// ======================================================

BOOL EqualsWithinTol(float val1, float val2)
{
	return abs(val1 - val2) < FLT_EPSILON;
}

// ======================================================
// LerpF(): Linear interpolation between two floats.
// ======================================================

float LerpF(float a, float b, float t)
{
	return (a + t * (b - a));
}

// ======================================================
// SeedRandGen(): Seeds the pseudo-random generator.
// ======================================================

void SeedRandGen(void)
{
	// Seed based on current time, but improve
	// bits distribution first:

	unsigned int seed = 1337;
	time_t now = time(NULL);
	unsigned char * p = (unsigned char *)&now;
	for (size_t i = 0; i < sizeof(now); ++i)
	{
		seed = seed * (0xff + 2U) + p[i];
	}

	srand(seed);
}

// ======================================================
// RandInRange(): Pseudo-random number is [lower,upper] range.
// ======================================================

double RandInRange(double lowerBound, double upperBound)
{
	double d = ((double)rand() + 0.5) / RAND_MAX;
	return (lowerBound + (upperBound - lowerBound) * d);
}

// ======================================================
// PointLerp(): Liner interpolation of CGPoints.
// ======================================================

void PointLerp(CGPoint * result, const CGPoint * a, const CGPoint * b, float t)
{
	result->x = (a->x + t * (b->x - a->x));
	result->y = (a->y + t * (b->y - a->y));
}

// ======================================================
// PointEq(): Compare 2 CGPoints for equality.
// ======================================================

BOOL PointEq(const CGPoint * a, const CGPoint * b)
{
	return (a->x == b->x) && (a->y == b->y);
}

// ======================================================
// AbsoluteTimeMillisecs(): Milliseconds since app startup.
// ======================================================

int64_t AbsoluteTimeMillisecs(void)
{
	// clockFreq is in nanosecond precision (Not Bad!)
	return ((((int64_t)mach_absolute_time() - baseTime) * clockFreq) / 1000000);
}

// ======================================================
// AbsoluteTimeSeconds(): Seconds since app startup.
// ======================================================

double AbsoluteTimeSeconds(void)
{
	return ((double)AbsoluteTimeMillisecs() * 0.001);
}

// ======================================================
// ResourceExists(): Test if resource exists and is a file.
// ======================================================

BOOL ResourceExists(NSString * filePath)
{
	OSSG_ASSERT(filePath != nil);

#if TARGET_IPHONE_SIMULATOR

	int result;
	struct stat statBuf;

	// NOTE: Assume default resources path!
	NSString * fullPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:filePath];
	result = stat([fullPath cStringUsingEncoding:NSUTF8StringEncoding], &statBuf);
	return ((result == 0) && S_ISREG(statBuf.st_mode)) ? YES : NO;

#else // !TARGET_IPHONE_SIMULATOR

	// We don't need this test on the actual device.
	// It is assumed that all bugs have been fixed by deployment time :P
	return YES;

#endif // TARGET_IPHONE_SIMULATOR
}

// ======================================================
// GetUserHomePath(): Get the full path to the user home directory.
// ======================================================

NSString * GetUserHomePath(void)
{
	NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	return paths[0];
}

// ======================================================
// SystemAlert(): System popup dialog (non-blocking).
// ======================================================

void SystemAlert(NSString * title, NSString * message, ...)
{
	// See http://stackoverflow.com/questions/1787254/showing-an-alert-in-an-iphone-top-level-exception-handler
	// for more on exception handling and error reporting for iOS.

	va_list args;
	va_start(args, message);
    NSString * contents = [[NSString alloc] initWithFormat:message arguments:args];
	va_end(args);

	// Also direct to standard log:
	NSLog(@"%@: %@", title, contents);

	// Enqueue message popup:
	dispatch_async(dispatch_get_main_queue(), ^{
		UIView * view = [[CCDirector sharedDirector] view];
		UIAlertView * alert = [[UIAlertView alloc] initWithTitle:title
		                                                 message:contents
		                                                delegate:nil
		                                       cancelButtonTitle:@"OK"
		                                       otherButtonTitles:nil];
		[view addSubview:alert];
		[alert show];
	});
}

// ======================================================
// Custom UIAlertView delegate:
// ======================================================

@interface AlertViewDelegate : NSObject<UIAlertViewDelegate>
@property(nonatomic, copy) void(^callback)(BOOL selection);
@end
@implementation AlertViewDelegate
- (void) alertView: (UIAlertView *) alertView clickedButtonAtIndex: (NSInteger) buttonIndex
{
	if (_callback)
	{
		// NO  = Cancel
		// YES = Continue
		_callback((buttonIndex == 0) ? NO : YES);
	}
}
@end
static AlertViewDelegate * alertDelegate = nil;

// ======================================================
// ShowCancelContinueSystemDlg(): Accepts a user callback.
// ======================================================

void ShowCancelContinueSystemDlg(NSString * title, NSString * message, void(^callback)(BOOL))
{
	if (alertDelegate == nil)
	{
		alertDelegate = [AlertViewDelegate new];
	}

	alertDelegate.callback = callback;

	dispatch_async(dispatch_get_main_queue(), ^{
		UIView * view = [[CCDirector sharedDirector] view];
		UIAlertView * alert = [[UIAlertView alloc] initWithTitle:title
		                                                 message:message
		                                                delegate:alertDelegate
		                                       cancelButtonTitle:@"Cancel"
		                                       otherButtonTitles:@"Continue", nil];
		[view addSubview:alert];
		[alert show];
	});
}
