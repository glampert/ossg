//
//  GameEntity.h
//  OSSG
//
//  Created by Guilherme R. Lampert on 27/03/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "OSSG.h"

// ======================================================
// Enums / constants / helpers:
// ======================================================

// Forward declarations:
@class RenderEntity;
@class EntityAI;

// Basic Game Entity types:
typedef enum eGameEntityType {
	GE_PLAYER,
	GE_ENEMY,
	GE_PROJECTILE,
	GE_ITEM_POWERUP,
	GE_SHIELD,
	GE_UNKNOWN
	// etcetera...
} GameEntityType;

// Weapons enumerator:
typedef enum eWeaponId {
	WEAP_MAIN,
	WEAP_SECONDARY1,
	WEAP_SECONDARY2,
	NUM_WEAPONS
} WeaponId;

// Unique id type:
typedef unsigned int GameEntityId;

// Loads a PLIST with error checking. 'filename' doesn't need to have the .plist extension.
NSDictionary * LoadDictionaryFromPLIST(NSString * filename);

// ======================================================
// ProjectileDescriptor interface:
// ======================================================

@interface ProjectileDescriptor : NSObject

// Rendering params:
@property(nonatomic, strong) NSString * spriteName;
@property(nonatomic, strong) NSString * trailSpriteName;
@property(nonatomic) struct sTrailRenderParams trailParams;
@property(nonatomic) float  spriteScale;

// Projectile physics / behavior:
@property(nonatomic) int    batteriesCost;
@property(nonatomic) int    missileCost;
@property(nonatomic) int    damageInflicted;  // Damage inflicted. Usually inherited from parent
@property(nonatomic) double fireCoolDownTime; // Time gap between shots, in seconds.
@property(nonatomic) double lastTimeFired;    // Last time weapon was fired, in seconds. Internal use.
@property(nonatomic) float  acceleration;     // Projectile acceleration.
@property(nonatomic) float  turnSpeed;        // For homing missiles.
@property(nonatomic) enum   eAIType  aiType;  // For homing missiles / "smart" projectiles.
@property(nonatomic) enum   eSoundId sound;   // Sound played when fired.

// Easy way to grab a pointer to the 'trailParams' property:
- (const struct sTrailRenderParams *) getTrailParamsRef;

// Deep copy self into a new instance:
- (ProjectileDescriptor *) clone;

@end

// ======================================================
// GameEntity interface:
// ======================================================

//
// Represents a generic game entity.
// This beast is used for most interactive game elements,
// such as the player, enemies and interactive UI elements.
//
// This class is monolithic and very game specific.
// Some effort should be made to move logic outside of it
// from now on.
//
// Despite that, GameEntity is fairly component oriented.
// There are no subclasses of it. GameEntity behavior is defined
// by its data. A GameEntity instance can be constructed
// from an NSDictionary filled with configuration parameters.
// The dictionary can also be loaded from a PLIST file, for convenience.
//
@interface GameEntity : NSObject

// Common:
+ (void)           commonInit;
+ (GameEntityId)   genUniqueId;

// Entity factories (class methods):
+ (GameEntity *)   spawn:                   (GameEntityType) type;
+ (GameEntity *)   spawnWithArgs:           (NSDictionary *) args;
+ (GameEntity *)   spawnWithFile:           (NSString     *) plist;
+ (void)           remove:                  (GameEntity   *) entity;

// Instance methods:
- (id)             init;
- (GameEntity *)   clone; // Deep copy self into a new instance.
- (void)           frameUpdate;
- (GameEntityId)   uid;
- (BOOL)           isAlive;
- (void)           setAlive:                (BOOL) value;
- (RenderEntity *) getRenderEntity;
- (void)           setGraphics:             (NSString *) spriteName;
- (void)           setGraphics:             (NSString *) spriteName withScale: (float) scale;
- (void)           setType:                 (GameEntityType) tp;
- (void)           moveToPosition:          (CGPoint) pos setTargetAngle: (BOOL) yesNo;
- (void)           stopMoving;
- (void)           setPosition:             (CGPoint) pos;
- (void)           setPositionFromString:   (NSString *) pos;
- (CGPoint)        getPosition;
- (CGPoint)        getFreeFireVector;
- (void)           setFreeFireVectorX:      (float) x andY: (float) y;
- (int)            getDamageInflicted;
- (void)           setDamageInflicted:      (int) amount;
- (int)            getCreditsForDeath;
- (void)           setCreditsForDeath:      (int) amount;
- (int)            getNumMissiles;
- (void)           setNumMissiles:          (int) amount;
- (int)            getNumBatteries;
- (void)           setNumBatteries:         (int) amount;
- (int)            getHealth;
- (void)           setHealth:               (int) amount;
- (int)            getMaxHealth;
- (void)           setMaxHealth:            (int) amount;
- (float)          getDamageAmount;         // in [0,1] range
- (GameEntityType) getType;
- (void)           setParent:               (GameEntity *) ent;
- (GameEntity *)   getParent;
- (BOOL)           receiveHit:              (GameEntity *) projectile;
- (BOOL)           intersects:              (GameEntity *) ent;
- (BOOL)           pointIntersects:         (CGPoint) pt;
- (void)           setTakesHits:            (BOOL) enable;
- (void)           setSortOrderForType;
- (void)           setAI:                   (EntityAI *) ai;
- (void)           fireWeapon:              (WeaponId) which;
- (void)           setShieldBatteryCost:    (int) cost;
- (int)            getShieldBatteryCost;
- (void)           setShieldHealth:         (int) amount;
- (int)            getShieldHealth;
- (void)           setShieldScale:          (float) scale;
- (float)          getShieldScale;
- (BOOL)           hasShield;
- (GameEntity *)   getShield;
- (BOOL)           activateShield;          // Active it if entity has enough batteries
- (void)           setDefaults;             // Internal use.

// Used by AIs:
- (void) setProjectileDescriptor: (ProjectileDescriptor *) pd forWeapon: (WeaponId) which;
- (ProjectileDescriptor *) getProjectileDescriptor: (WeaponId) which;

@end
