//
//  AppDelegate.m
//  OSSG - Default application delegate.
//
//  Created by Guilherme R. Lampert on 27/03/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "AppDelegate.h"
#import "OSSG.h"
#import "GameGUI.h"
#import "GameGUI_Menus.h"

// ======================================================
// AppDelegate implementation:
// ======================================================

@implementation AppDelegate

- (BOOL) application: (UIApplication *) application didFinishLaunchingWithOptions: (NSDictionary *) launchOptions
{
	// This is the only app delegate method you need to implement when inheriting from CCAppDelegate.
	// This method is a good place to add one time setup code that only runs when your app is first launched.

	// Setup Cocos2D with reasonable defaults for everything.
	// There are a number of simple options you can change.
	// If you want more flexibility, you can configure Cocos2D yourself instead of calling setupCocos2dWithOptions:.
	[self setupCocos2dWithOptions: @{
		// Use a depth buffer. Only needed if mixing 2D and 3D drawing.
		CCSetupDepthFormat: @(GL_DEPTH_COMPONENT24_OES),
		// NSNumber with bool. Specifies whether miltisampling is enabled.
//		CCSetupMultiSampling: @(YES),
		// NSNumber with integer. Specifies number of samples when multisampling is enabled.
//		CCSetupNumberOfSamples: @(2),
		// Show the FPS and draw call count label:
//		CCSetupShowDebugStats: @(YES),
		// Use a 16 bit color buffer:
//		CCSetupPixelFormat: kEAGLColorFormatRGB565,
		// Use a simplified coordinate system that is shared across devices:
//		CCSetupScreenMode: CCScreenModeFixed,
		// Run in portrait mode:
//		CCSetupScreenOrientation: CCScreenOrientationPortrait,
		// Run at a reduced frame-rate:
//		CCSetupAnimationInterval: @(1.0/30.0),
		// Run the fixed timestep extra fast:
//		CCSetupFixedUpdateInterval: @(1.0/180.0),
		// Make iPad's act like they run at a 2x content scale. (iPad retina 4x)
//		CCSetupTabletScale2X: @(YES),
	}];

	return YES;
}

- (CCScene *) startScene
{
	// NOTE: startScene actually happens BEFORE didFinishLaunchingWithOptions!
	// So Session setup should be done here...

	[GameSession createSharedGameSession];
	[Session commonInit];
	[Session gameInit];

	/*
	 * To start directly into a new game stage, without going
	 * thru the menus, uncomment the following lines:
	 */
//	[Session createNewGameWithShip:@"light_fighter"];
//	[Session playerInit];
//	return [GameStage new];

	// Start in the home menu:
	return [Gui startHomeMenu];
}

@end
