//
//  GameStage.h
//  OSSG
//
//  Created by Guilherme R. Lampert on 27/03/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "OSSG.h"

// Forward declarations:
@class GameEntity;

// ======================================================
// GameStage interface:
// ======================================================

//
// A stage is a single "game level".
// The GameStage is also the current Cocos scene being displayed.
// It occupies the whole screen.
//
@interface GameStage : CCScene

@property(nonatomic, readonly) BOOL hasBoss; // When YES, the stage has a boss at the end.

- (id)   init;
- (void) update:         (CCTime) deltaTime;
- (BOOL) checkHits:      (GameEntity *) projectile;
- (BOOL) stageCleared;
- (int)  entityCount;
- (int)  wavesLeft;

// GameEntity linking:
- (BOOL) isEntityLinked: (GameEntity *) ent;
- (void) linkEntity:     (GameEntity *) ent;
- (void) unlinkEntity:   (GameEntity *) ent;

@end
