//
//  GameEntity.m
//  OSSG
//
//  Created by Guilherme R. Lampert on 27/03/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "GameEntity.h"
#import "EnemyAIs.h"

// ======================================================
// Local shared data / helpers:
// ======================================================

// Dictionaries used for string => enum conversions.
// Common to all GameEntity instances.
static NSDictionary * gameEntityTypeFromString = nil;
static NSDictionary * aiTypeFromString         = nil;

// Unique id counter.
static GameEntityId guid = 0;

// ======================================================
// LoadDictionaryFromPLIST():
// ======================================================

NSDictionary * LoadDictionaryFromPLIST(NSString * filename)
{
	OSSG_ASSERT(filename != nil);

	// Append default extension if not present:
	NSString * fileExt = [filename pathExtension];
	if ([fileExt caseInsensitiveCompare:@"plist"] != NSOrderedSame)
	{
		filename = [filename stringByAppendingFormat:@".plist"];
	}

	if (!ResourceExists(filename))
	{
		OSSG_ERROR(@"File '%@' is not present / does not name a file!", filename);
		return nil;
	}

	NSString * fullPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:filename];
	return [NSDictionary dictionaryWithContentsOfFile:fullPath];
}

// ======================================================
// ProjectileDescriptor implementation:
// ======================================================

// This is just a simple container for data.
// We don't use a struct because objc with ARC won't let you
// put objc objects into a C struct. I find that rather annoying...
@implementation ProjectileDescriptor

- (const struct sTrailRenderParams *) getTrailParamsRef
{
	return &_trailParams;
}

- (ProjectileDescriptor *) clone
{
	ProjectileDescriptor * pd = [ProjectileDescriptor new];
	pd.spriteName       = self.spriteName;
	pd.trailSpriteName  = self.trailSpriteName;
	pd.trailParams      = self.trailParams;
	pd.spriteScale      = self.spriteScale;
	pd.batteriesCost    = self.batteriesCost;
	pd.missileCost      = self.missileCost;
	pd.damageInflicted  = self.damageInflicted;
	pd.fireCoolDownTime = self.fireCoolDownTime;
	pd.lastTimeFired    = self.lastTimeFired;
	pd.acceleration     = self.acceleration;
	pd.turnSpeed        = self.turnSpeed;
	pd.aiType           = self.aiType;
	pd.sound            = self.sound;
	return pd;
}

@end

// ======================================================
// GameEntity implementation:
// ======================================================

@implementation GameEntity
{
	EntityAI     * AI;              // AI controller or nil.
	GameEntity   * parent;          // Parent or nil if none.
	RenderEntity * renderEntity;    // Visual component of this entity.
	GameEntityType type;            // Type flag. Internal use.
	CGPoint        freeFireVector;  // Used by entities that fire weapons.
	BOOL           alive;           // YES after [init]. Set to NO by [remove].
	BOOL           takeHits;        // Interacts with projectiles.
	int            health;          // Not everything has health, but...
	int            maxHealth;       // Max health set at creation.
	int            damageInflicted; // Amount of damage inflicted by collision with this entity.
	int            creditsForDeath; // How many credits the player gets for killing this guy.
	int            numMissiles;     // Missile ammo.
	int            numBatteries;    // Batteries ammo.
	GameEntityId   uniqueId;        // Unique identifier. Sequential, 64bit.

	// Entities like the player and bosses can have an energy shield around the ship.
	// Nil if shield not enabled. It always follows the entity, centered at the center of its sprite.
	GameEntity * shield;
	int   shieldCost;   // In batteries
	int   shieldHealth; // From 0 to 100
	float shieldScale;  // Scale of shield sprite. By default: 1

	// Weapon properties. If slot is nil, doesn't have that weapon.
	ProjectileDescriptor * projectileDesc[NUM_WEAPONS];
}

//
// Methods:
//

+ (void) commonInit
{
	// Initialize shared GameEntity state:

	gameEntityTypeFromString = [NSDictionary dictionaryWithObjectsAndKeys:
		[NSNumber numberWithInt:GE_PLAYER]       , @"player"       ,
		[NSNumber numberWithInt:GE_ENEMY]        , @"enemy"        ,
		[NSNumber numberWithInt:GE_PROJECTILE]   , @"projectile"   ,
		[NSNumber numberWithInt:GE_ITEM_POWERUP] , @"item_powerup" ,
		[NSNumber numberWithInt:GE_SHIELD]       , @"shield"       ,
		[NSNumber numberWithInt:GE_UNKNOWN]      , @"unknown"      ,
		nil];

	aiTypeFromString = [NSDictionary dictionaryWithObjectsAndKeys:
		[NSNumber numberWithInt:AI_NONE]           , @"none"           ,
		[NSNumber numberWithInt:AI_HOMING_MISSILE] , @"homing_missile" ,
		[NSNumber numberWithInt:AI_STRAIGHT_MOVER] , @"straight_mover" ,
		[NSNumber numberWithInt:AI_WAVE_MOVER]     , @"wave_mover"     ,
		[NSNumber numberWithInt:AI_DRONE_MINE]     , @"drone_mine"     ,
		[NSNumber numberWithInt:AI_BOSS]           , @"boss"           ,
		nil];
}

+ (GameEntityId) genUniqueId
{
	return guid++;
}

+ (GameEntity *) newPlayer
{
	// Instantiate/set type:
	GameEntity * ent = [GameEntity new];
	[ent setType:GE_PLAYER];
	[ent setGraphics:nil];
	[ent setSortOrderForType];
	return ent;
}

+ (GameEntity *) newEnemy
{
	// Instantiate/set type:
	GameEntity * ent = [GameEntity new];
	[ent setType:GE_ENEMY];
	return ent;
}

+ (GameEntity *) newProjectile
{
	// Instantiate/set type:
	GameEntity * ent = [GameEntity new];
	[ent setType:GE_PROJECTILE];
	return ent;
}

+ (GameEntity *) newItemPowerup
{
	// Instantiate/set type:
	GameEntity * ent = [GameEntity new];
	[ent setType:GE_ITEM_POWERUP];
	return ent;
}

+ (GameEntity *) newShield
{
	// Instantiate/set type:
	GameEntity * ent = [GameEntity new];
	[ent setType:GE_SHIELD];
	return ent;
}

+ (GameEntity *) spawn: (GameEntityType) type
{
	switch (type)
	{
	case GE_PLAYER :
		return [GameEntity newPlayer];

	case GE_ENEMY :
		return [GameEntity newEnemy];

	case GE_PROJECTILE :
		return [GameEntity newProjectile];

	case GE_ITEM_POWERUP :
		return [GameEntity newItemPowerup];

	case GE_SHIELD :
		return [GameEntity newShield];

	default :
		OSSG_ERROR(@"Invalid GameEntityType!");
		return nil;
	} // switch (type)
}

+ (void) remove: (GameEntity *) entity
{
	// The player is never actually removed, just hid.
	if (entity->type == GE_PLAYER)
	{
		[entity getRenderEntity].visible = NO;
		[entity setAlive:NO];
		return;
	}

	// TODO: Possibly add a recycling list for entities.
	// [setDefaults] and recycle it. We can to also recycle the RenderEntity.

	// Let parent know its shield is gone
	if (entity->type == GE_SHIELD)
	{
		entity->parent->shield = nil;
	}

	if ([[entity getRenderEntity] isTrailRenderEnabled])
	{
		// Wait 2 seconds before killing the trail renderer.
		// This should be enough time for it to finish nicely.
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)),
		dispatch_get_main_queue(), ^{
			[[entity getRenderEntity] disableTrailRender:YES];
			[entity setDefaults];
		});
	}
	else
	{
		[entity setDefaults];
		entity = nil;
	}
}

- (void) setDefaults
{
	AI              = nil;
	parent          = nil;
	renderEntity    = nil;
	type            = GE_UNKNOWN;
	freeFireVector  = CGPointZero;
	alive           = NO;
	takeHits        = YES;
	health          = 100;
	maxHealth       = 100;
	damageInflicted = 0;
	creditsForDeath = 0;
	numMissiles     = 0;
	numBatteries    = 0;
	shield          = nil;
	shieldCost      = 1;
	shieldHealth    = 0;
	shieldScale     = 1.0f;

	for (int i = 0; i < NUM_WEAPONS; ++i)
	{
		projectileDesc[i] = nil;
	}
}

- (id) init
{
	self = [super init];
	if (!self)
	{
		return nil;
	}

	// The unique id is only set once:
	uniqueId = [GameEntity genUniqueId];

	// Set the rest to defaults:
	[self setDefaults];

	// Alive must be true after creation.
	alive = YES;

	return self;
}

- (GameEntity *) clone
{
	// The UID will not be cloned
	// 'ent' has its own id.
	GameEntity * ent = [GameEntity new];
	[ent setAI:[AI clone]];
	ent->renderEntity    = [renderEntity clone];
	ent->parent          = parent; // Parent must not be cloned!
	ent->type            = type;
	ent->freeFireVector  = freeFireVector;
	ent->alive           = alive;
	ent->takeHits        = takeHits;
	ent->health          = health;
	ent->maxHealth       = maxHealth;
	ent->damageInflicted = damageInflicted;
	ent->creditsForDeath = creditsForDeath;
	ent->numMissiles     = numMissiles;
	ent->numBatteries    = numBatteries;
	ent->shield          = [shield clone];
	ent->shieldCost      = shieldCost;
	ent->shieldHealth    = shieldHealth;
	ent->shieldScale     = shieldScale;

	for (int i = 0; i < NUM_WEAPONS; ++i)
	{
		ent->projectileDesc[i] = [projectileDesc[i] clone];
	}

	return ent;
}

- (void) setupAnimations: (NSString *) spriteSheet withScale: (float) spriteScale spawnArgs: (NSDictionary *) args
{
	[RenderEntity loadSpriteSheet:spriteSheet];
	renderEntity.scale = spriteScale;

	// Load each animation. Each one is defined by a dictionary:
	for (NSString * animKey in args.allKeys)
	{
		// Any key starting with "anim-" is assumed to be an animation dictionary;
		if ([animKey rangeOfString:@"anim-"].location != NSNotFound)
		{
			id k = [args objectForKey:animKey];

			if ([k isKindOfClass:[NSDictionary class]])
			{
				OSSG_LOG(@"Reading animation key '%@'...", animKey);

				NSDictionary * animDict  = (NSDictionary *)k;
				const BOOL   isBindPose  = [[animDict objectForKey:@"is-bind-pose"] boolValue];
				const double frameDelay  = [[animDict objectForKey:@"per-frame-delay"] doubleValue];
				NSString * animationName = [animDict  objectForKey:@"animation-name"];
				NSMutableArray * animationFrameKeys = [NSMutableArray array];

				// Animation frames are <frame_id-x, anim_name> pairs.
				// The key for every animation fame must start with "frame_id".
				for (NSString * frameKey in animDict.allKeys)
				{
					if ([frameKey rangeOfString:@"frame_id"].location != NSNotFound)
					{
						[animationFrameKeys addObject:frameKey];
					}
				}

				if ([animationFrameKeys count] > 0)
				{
					[animationFrameKeys sortUsingComparator:^NSComparisonResult(id a, id b) {
						return [((NSString *)a) compare:((NSString *)b) options:NSNumericSearch];
					}];

					NSMutableArray * animationFrames = [NSMutableArray array];
					for (NSString * frameKey in animationFrameKeys)
					{
						[animationFrames addObject:[animDict objectForKey:frameKey]];
					}

					[renderEntity
						loadAnimation:animationFrames
						  spriteSheet:spriteSheet
							 animName:animationName
						   frameDelay:frameDelay
							 bindPose:isBindPose];
				}
				else
				{
					OSSG_LOG(@"Warning: Animation dictionary had no animation frames!");
				}
			}
			else
			{
				OSSG_ERROR(@"'anim-' entry is not a dictionary!");
			}
		}
	}
}

- (void) setupTrailRender: (NSDictionary *) params
{
	OSSG_ASSERT(params != nil);

	NSString *   baseTexture = [params objectForKey:@"base-texture"];
	NSString *   rgba        = [params objectForKey:@"rgba"];

	const double fadeTimeSec = [[params objectForKey:@"fade-time"] doubleValue];
	const double minSegments = [[params objectForKey:@"min-segments"] doubleValue];
	const int    width       = [[params objectForKey:@"width"] intValue];

	baseTexture = [NSString stringWithFormat:@"%@.%@", baseTexture, STD_SPRITE_SHEET_FILE_EXT];
	const char * colorCStr = [rgba cStringUsingEncoding:NSUTF8StringEncoding];
	const char * texCStr   = [baseTexture cStringUsingEncoding:NSUTF8StringEncoding];

	float r = 0.0f, g = 0.0f, b = 0.0f, a = 0.0f;
	if (sscanf(colorCStr, "(%f,%f,%f,%f)", &r, &g, &b, &a) != 4)
	{
		OSSG_LOG(@"Error: Failed to parse color from a string!");
	}

	const TrailRenderParams trailParams = {
		/* baseTexture = */ texCStr,
		/* rgba        = */ r,g,b,a,
		/* fadeTimeSec = */ fadeTimeSec,
		/* minSegments = */ minSegments,
		/* width       = */ width,
		/* fastMode    = */ NO
	};
	[renderEntity enableTrailRender:&trailParams];
}

+ (GameEntity *) spawnWithArgs: (NSDictionary *) args
{
#define HAS_KEY(dict, key) ([(dict) objectForKey:(key)] != nil)

	GameEntityType entityType;
	AIType aiType;

	if (HAS_KEY(args, @"entity-type"))
	{
		entityType = (GameEntityType)[[gameEntityTypeFromString objectForKey:[args objectForKey:@"entity-type"]] intValue];
	}
	else
	{
		OSSG_ERROR(@"Missing 'entity-type' key! Defaulting to GE_UNKNOWN...");
		entityType = GE_UNKNOWN;
	}

	if (HAS_KEY(args, @"ai-type"))
	{
		aiType = (AIType)[[aiTypeFromString objectForKey:[args objectForKey:@"ai-type"]] intValue];
	}
	else
	{
		aiType = AI_NONE;
	}

	// Numerical params:
	const int    initialHealth   = HAS_KEY(args, @"initial-health")    ? [[args objectForKey:@"initial-health"]    intValue]    : 100;
	const int    maxHealth       = HAS_KEY(args, @"max-health")        ? [[args objectForKey:@"max-health"]        intValue]    : 100;
	const int    damageInflicted = HAS_KEY(args, @"damage-inflicted")  ? [[args objectForKey:@"damage-inflicted"]  intValue]    : 0;
	const int    creditsForDeath = HAS_KEY(args, @"credits-for-death") ? [[args objectForKey:@"credits-for-death"] intValue]    : 0;
	const double spriteScale     = HAS_KEY(args, @"sprite-scale")      ? [[args objectForKey:@"sprite-scale"]      doubleValue] : 1;
	const double spawnAngle      = HAS_KEY(args, @"spawn-angle")       ? [[args objectForKey:@"spawn-angle"]       doubleValue] : 0;
	const double turnSpeed       = HAS_KEY(args, @"turn-speed")        ? [[args objectForKey:@"turn-speed"]        doubleValue] : 0;
	const double acceleration    = HAS_KEY(args, @"acceleration")      ? [[args objectForKey:@"acceleration"]      doubleValue] : 0;

	// String params:
	NSString * spawnPosition = HAS_KEY(args, @"spawn-position") ? [args objectForKey:@"spawn-position"] : @"(0,0)";
	NSString * spriteSheet   = HAS_KEY(args, @"sprite-sheet")   ? [args objectForKey:@"sprite-sheet"]   : @"";

	// Boolean params:
	const BOOL takesHits           = HAS_KEY(args, @"takes-hits")          ? [[args objectForKey:@"takes-hits"]          boolValue] : YES;
	const BOOL animated            = HAS_KEY(args, @"animated")            ? [[args objectForKey:@"animated"]            boolValue] : NO;
	const BOOL constraintToWorld   = HAS_KEY(args, @"constraint-to-world") ? [[args objectForKey:@"constraint-to-world"] boolValue] : YES;
	const BOOL displayDamageEffect = HAS_KEY(args, @"damage-effect")       ? [[args objectForKey:@"damage-effect"]       boolValue] : NO;

	// Debug printing:
	OSSG_LOG(@"---------- Creating entity with params: ----------");
	OSSG_LOG(@"entityType..........: %d (%@)", entityType, [args objectForKey:@"entity-type"]);
	OSSG_LOG(@"aiType..............: %d (%@)", aiType,     [args objectForKey:@"ai-type"]);
	OSSG_LOG(@"initialHealth.......: %d",      initialHealth);
	OSSG_LOG(@"maxHealth...........: %d",      maxHealth);
	OSSG_LOG(@"damageInflicted.....: %d",      damageInflicted);
	OSSG_LOG(@"creditsForDeath.....: %d",      creditsForDeath);
	OSSG_LOG(@"spawnPosition.......: %@",      spawnPosition);
	OSSG_LOG(@"spriteSheet.........: %@",      spriteSheet);
	OSSG_LOG(@"spriteScale.........: %f",      spriteScale);
	OSSG_LOG(@"takesHits...........: %d",      takesHits);
	OSSG_LOG(@"animated............: %d",      animated);
	OSSG_LOG(@"constraintToWorld...: %d",      constraintToWorld);
	OSSG_LOG(@"displayDamageEffect.: %d",      displayDamageEffect);
	OSSG_LOG(@"acceleration........: %f",      acceleration);
	OSSG_LOG(@"turnSpeed...........: %f",      turnSpeed);
	OSSG_LOG(@"--------------------------------------------------");

	// Finish instantiation:
	GameEntity * ent = [GameEntity spawn:entityType];
	if (ent)
	{
		if (!animated)
		{
			// Simple one-frame sprite:
			[ent setGraphics:spriteSheet withScale:spriteScale];
		}
		else
		{
			// Animated sprite sheet:
			[ent getRenderEntity].entityType = entityType;
			[ent setupAnimations:spriteSheet withScale:spriteScale spawnArgs:args];
		}

		// Add a trail renderer if one was declared in the plist:
		id trailParams = [args objectForKey:@"trail-render"];
		if ((trailParams != nil) && [trailParams isKindOfClass:[NSDictionary class]])
		{
			[ent setupTrailRender:(NSDictionary *)trailParams];
		}

		[ent setAI:[EntityAI createAIOfType:aiType]];
		[ent setHealth:initialHealth];
		[ent setMaxHealth:maxHealth];
		[ent setDamageInflicted:damageInflicted];
		[ent setCreditsForDeath:creditsForDeath];
		[ent setTakesHits:takesHits];
		[ent setPositionFromString:spawnPosition];
		[ent setSortOrderForType];

		RenderEntity * re = [ent getRenderEntity];
		re.entityType          = entityType;
		re.rotation            = spawnAngle;
		re.turnSpeed           = turnSpeed;
		re.acceleration        = acceleration;
		re.constraintToWorld   = constraintToWorld;
		re.displayDamageEffect = displayDamageEffect;
		[re setTargetAngle:spawnAngle];

		if (entityType == GE_ENEMY)
		{
			re.allowMovement = NO;
			re.allowRotation = NO;
		}
	}

	return ent;
}

+ (GameEntity *) spawnWithFile: (NSString *) plist
{
	return [GameEntity spawnWithArgs:LoadDictionaryFromPLIST(plist)];
}

- (void) projectileUpdate
{
	CGPoint p = renderEntity.position;

	// "Intelligent" projectiles like homing missiles
	// have a dedicated IA module. The rest just move
	// in a strait trajectory.
	if (AI != nil)
	{
		[AI think];
	}
	else
	{
		// Update global position:
		const float accel = (renderEntity.acceleration * Time.deltaTime);
		p.x += (freeFireVector.x * accel);
		p.y += (freeFireVector.y * accel);
		[self setPosition:p];
	}

	// Remove if outside the screen:
	const CGSize screen = Stage.contentSize;
	if ((p.x < -20) || (p.x > screen.width + 20))
	{
		[Stage unlinkEntity:self];
	}
	else if ((p.y < -20) || (p.y > screen.height + 20))
	{
		[Stage unlinkEntity:self];
	}
	else
	{
		// Remove if hit or destroyed:
		const BOOL hitSomething = [Stage checkHits:self];
		if (hitSomething || (health <= 0))
		{
			[Stage unlinkEntity:self];
		}
	}
}

- (BOOL) isEnemyBoss
{
	if (type != GE_ENEMY)
	{
		return NO;
	}
	if ((AI == nil) || ![AI isKindOfClass:[AIBoss class]])
	{
		return NO;
	}
	return YES;
}

- (void) enemyUpdate
{
	[AI think];

	if (![self isAlive])
	{
		// Remove enemy:
		[self setHealth:0];
		[Stage unlinkEntity:self];
		return;
	}

	//
	// Test collision with the player/shield:
	//
	GameEntity * playerObject = [Session.player hasShield] ? [Session.player getShield] : Session.player;
	if ([self intersects:playerObject])
	{
		// Fly some particles from the impact location:
		[self spawnShotHitParticleSystem:[self getPosition]];
		[playerObject spawnShotHitParticleSystem:[playerObject getPosition]];

		// If not god-mode, decrease player health:
		if (!Session.godMode)
		{
			int h = [playerObject getHealth];
			h -= [self getDamageInflicted];
			[playerObject setHealth:h];
			if (h <= 0)
			{
				[Stage unlinkEntity:playerObject];
			}
		}

		if ([self isEnemyBoss])
		{
			// Take damage:
			if ([playerObject isAlive])
			{
				health -= [playerObject getDamageInflicted];
				if (health <= 0)
				{
					[Stage unlinkEntity:self];
				}
			}
		}
		else
		{
			// Kill enemy:
			[self setHealth:0];
			[Stage unlinkEntity:self];
		}
	}
}

- (void) itemPowerupUpdate
{
	// Move right to left:
	CGPoint p = renderEntity.position;
	p.x -= (renderEntity.acceleration * Time.deltaTime);
	[self setPosition:p];

	if (p.x < -20) // Remove if outside the screen:
	{
		[Stage unlinkEntity:self];
	}
	else if ([self intersects:Session.player]) // Remove if hit player:
	{
		// Give bonuses:
		[Session.player setNumBatteries: [Session.player getNumBatteries] + [self getNumBatteries]];
		[Session.player setNumMissiles:  [Session.player getNumMissiles]  + [self getNumMissiles]];

		[SoundSys playSoundAsync:SND_GOT_POWERUP];
		[Stage unlinkEntity:self];
	}
	else
	{
		// Keep floating in space...
	}
}

- (void) shieldUpdate
{
	if ((parent == nil) || ![parent isAlive])
	{
		[Stage unlinkEntity:self];
		return;
	}

	// Move with parent:
	renderEntity.position = [parent getRenderEntity].position;

	// Set color according to damage:
	float r,g,b;
	[renderEntity getDamageColorRed:&r green:&g blue:&b];
	renderEntity.color = [CCColor colorWithRed:r green:g blue:b];

	// Adjust scale:
	if (renderEntity.scale < shieldScale)
	{
		float s = renderEntity.scale;
		s += Time.deltaTime * 1.1f;
		renderEntity.scale = s;
	}
}

- (void) frameUpdate
{
	// Render update:
	renderEntity.damageAmount = [self getDamageAmount];
	[renderEntity frameUpdate];

	// Behavior update:
	switch (type)
	{
	case GE_ENEMY :
		[self enemyUpdate];
		break;

	case GE_PROJECTILE :
		[self projectileUpdate];
		break;

	case GE_ITEM_POWERUP :
		[self itemPowerupUpdate];
		break;

	case GE_SHIELD :
		[self shieldUpdate];
		break;

	default :
		break;
	} // switch (type)
}

- (GameEntityId) uid
{
	return uniqueId;
}

- (BOOL) isAlive
{
	return alive;
}

- (void) setAlive: (BOOL) value
{
	alive = value;
}

- (RenderEntity *) getRenderEntity
{
	return renderEntity;
}

- (void) setGraphics: (NSString *) spriteName
{
	if (spriteName)
	{
		renderEntity = [RenderEntity createWithSpriteName:spriteName];
	}
	else
	{
		renderEntity = [RenderEntity createEmpty];
	}
}

- (void) setGraphics: (NSString *) spriteName withScale: (float) scale
{
	if (spriteName)
	{
		renderEntity = [RenderEntity createWithSpriteName:spriteName];
	}
	else
	{
		renderEntity = [RenderEntity createEmpty];
	}

	renderEntity.scale = scale;
}

- (void) setType: (GameEntityType) tp
{
	type = tp;
}

- (void) moveToPosition: (CGPoint) pos setTargetAngle: (BOOL) yesNo
{
	[renderEntity moveToPosition:pos setTargetAngle:yesNo];
}

- (void) stopMoving
{
	[renderEntity stopMoving];
}

- (void) setPosition: (CGPoint) pos
{
	[renderEntity moveToPosition:pos setTargetAngle:NO];
	renderEntity.position = pos;
}

- (void) setPositionFromString: (NSString *) pos
{
	OSSG_ASSERT(pos != nil);
	const char * cstr = [pos cStringUsingEncoding:NSUTF8StringEncoding];
	float x = 0.0f, y = 0.0f;

	if (sscanf(cstr, "(%f,%f)", &x, &y) != 2)
	{
		OSSG_LOG(@"Error: Failed to parse entity position from a string!");
	}

	[self setPosition:ccp(x,y)];
}

- (CGPoint) getPosition
{
	return renderEntity.position;
}

- (CGPoint) getFreeFireVector
{
	return freeFireVector;
}

- (void) setFreeFireVectorX: (float) x andY: (float) y
{
	freeFireVector.x = x;
	freeFireVector.y = y;
}

- (int) getDamageInflicted
{
	return damageInflicted;
}

- (void) setDamageInflicted: (int) amount
{
	damageInflicted = amount;
}

- (int) getCreditsForDeath
{
	return creditsForDeath;
}

- (void) setCreditsForDeath: (int) amount
{
	creditsForDeath = amount;
}

- (int) getNumMissiles
{
	return numMissiles;
}

- (void) setNumMissiles: (int) amount
{
	numMissiles = amount;
}

- (int) getNumBatteries
{
	return numBatteries;
}

- (void) setNumBatteries: (int) amount
{
	numBatteries = amount;
}

- (int) getHealth
{
	return health;
}

- (void) setHealth: (int) amount
{
	health = amount;
}

- (int) getMaxHealth
{
	return maxHealth;
}

- (void) setMaxHealth: (int) amount
{
	maxHealth = amount;
}

- (float) getDamageAmount
{
	return (1.0f - (health / 100.0f));
}

- (void) setProjectileDescriptor: (ProjectileDescriptor *) pd forWeapon: (WeaponId) which
{
	OSSG_ASSERT((int)which < NUM_WEAPONS);
	projectileDesc[(int)which] = pd;
}

- (ProjectileDescriptor *) getProjectileDescriptor: (WeaponId) which
{
	OSSG_ASSERT((int)which < NUM_WEAPONS);
	return projectileDesc[(int)which];
}

- (GameEntityType) getType
{
	return type;
}

- (void) setParent: (GameEntity *) ent
{
	parent = ent;
}

- (GameEntity *) getParent
{
	return parent;
}

- (BOOL) shouldDropItemPowerup
{
	// Obviously, only enemies will drop powerups.
	if (type != GE_ENEMY)
	{
		return NO;
	}

	// One in three chances:
	return ((rand() % 3 + 1) == 3);
}

- (void) dropItemPowerup
{
	GameEntity * powerup = [GameEntity spawn:GE_ITEM_POWERUP];

	const int sel = rand() % 2 + 1; // [1,2]
	switch (sel)
	{
	case 1 : // BATTERY
		[powerup setGraphics:@"icon_battery" withScale:0.4f];
		[powerup setNumBatteries:RandInRange(1, 3)];
		break;

	case 2 : // MISSILE
		[powerup setGraphics:@"icon_missile" withScale:0.4f];
		[powerup setNumMissiles:RandInRange(1, 3)];
		break;

	default:
		OSSG_ERROR(@"rand() is broken!!!");
		break;
	} // switch (sel)

	RenderEntity * re = [powerup getRenderEntity];

	// Move slightly slower then the enemy that spawned it:
	re.acceleration = ([self getRenderEntity].acceleration * 0.8f);
	re.entityType = GE_ITEM_POWERUP;

	// Make the object blink continuously:
	CCActionRepeatForever * blingBling = [CCActionRepeatForever actionWithAction:[CCActionBlink actionWithDuration:1 blinks:1]];
	[re runAction:blingBling];

	// Set other states and link:
	float r,g,b;
	[re getDamageColorRed:&r green:&g blue:&b];
	re.color = [CCColor colorWithRed:r green:g blue:b];

	[powerup setPosition:[self getPosition]];
	[powerup setSortOrderForType];

	[Stage linkEntity:powerup];
}

- (void) spawnShotHitParticleSystem: (CGPoint) pos
{
	float r,g,b;
	[renderEntity getDamageColorRed:&r green:&g blue:&b];

	CCParticleExplosion * prt = [[CCParticleExplosion alloc] initWithTotalParticles:500];
	prt.autoRemoveOnFinish = YES; // Remove self from scene when effect is done.
	prt.position      = pos;
	prt.startColor    = [CCColor colorWithRed:r green:g blue:b];
	prt.endColor      = [CCColor colorWithRed:r green:g blue:b];
	prt.startColorVar = [CCColor blackColor];
	prt.endColorVar   = [CCColor blackColor];
	prt.startSize     = 4;
	prt.startSizeVar  = 1;
	prt.speed         = 70;
	prt.life          = 0.3f;
	prt.lifeVar       = 0.005f;
	prt.duration      = 0.03f;

	[Stage addChild:prt z:ESORT_PARTICLE];
}

- (BOOL) receiveHit: (GameEntity *) projectile
{
	OSSG_ASSERT(projectile != nil  && [projectile getType] == GE_PROJECTILE);
	OSSG_ASSERT(projectile != self && [projectile uid] != [self uid]);

	// My own shots don't hit me:
	if ([projectile getParent] == self)
	{
		return NO;
	}

	// Shots fired by enemies will ignore other enemies an continue flying.
	// Remove this to enable "friendly fire".
	if ((type == GE_ENEMY) && ([[projectile getParent] getType] == GE_ENEMY))
	{
		return NO;
	}

	// This will avoid the entity's own shots to damage its shield:
	if ([[projectile getParent] hasShield] && ([[projectile getParent] getShield] == self))
	{
		return NO;
	}

	// Elements like UI don't take hits:
	if (!takeHits)
	{
		return NO;
	}

	// Projectile gets wasted instantly:
	[projectile setHealth:0];

	// Fly some particles from the impact location:
	[self spawnShotHitParticleSystem:[projectile getPosition]];

	// God mode? If so, no-target:
	if ((type == GE_PLAYER) && Session.godMode)
	{
		return NO;
	}

	// Kill entity if health drops to <= 0:
	health -= [projectile getDamageInflicted];
	if (health <= 0)
	{
		// Player gets awarded some credits for the kill:
		if ([projectile getParent] == Session.player)
		{
			[Session addPlayerCredits:[self getCreditsForDeath]];
		}

		// Drop powerup?
		if ([self shouldDropItemPowerup])
		{
			[self dropItemPowerup];
		}

		// Remove:
		[Stage unlinkEntity:self];
	}

	return YES;
}

- (BOOL) intersects: (GameEntity *) ent
{
	OSSG_ASSERT(ent != nil);
	const CGRect myRect    = [renderEntity boundingBox];
	const CGRect otherRect = [[ent getRenderEntity] boundingBox];
	return CGRectIntersectsRect(myRect, otherRect);
}

- (BOOL) pointIntersects: (CGPoint) pt
{
	return CGRectContainsPoint([renderEntity boundingBox], pt);
}

- (void) setTakesHits: (BOOL) enable
{
	takeHits = enable;
}

- (void) setSortOrderForType
{
	switch (type)
	{
	case GE_PLAYER :
	case GE_ENEMY  :
	case GE_ITEM_POWERUP :
		[renderEntity setEntitySort:ESORT_ENTITY];
		break;

	case GE_PROJECTILE :
		[renderEntity setEntitySort:ESORT_PROJECTILE];
		break;

	case GE_SHIELD :
		[renderEntity setEntitySort:ESORT_SHIELD];
		break;

	default :
		OSSG_ERROR(@"Invalid GameEntityType!");
		break;
	} // switch (type)
}

- (void) setAI: (EntityAI *) ai
{
	AI = ai;
	AI.owner = self;
}

- (void) fireWeapon: (WeaponId) which
{
	OSSG_ASSERT((int)which < NUM_WEAPONS);
	ProjectileDescriptor * pd = projectileDesc[(int)which];

	if (pd == nil)
	{
		return; // Don't have this weapon
	}

	if ((Time.currentTime - pd.lastTimeFired) < pd.fireCoolDownTime)
	{
		return; // Weapon not ready to fire again wet
	}

	if (((numBatteries - pd.batteriesCost) < 0) || ((numMissiles - pd.missileCost) < 0))
	{
		if (self == Session.player)
		{
			[SoundSys playSoundAsync:SND_ALERT];
		}
		return; // Not enough ammo
	}

	// Set up the projectile:
	GameEntity * shot = [GameEntity spawn:GE_PROJECTILE];
	[shot setGraphics:pd.spriteName withScale:pd.spriteScale];
	[shot setSortOrderForType];

	float r,g,b;
	[renderEntity getDamageColorRed:&r green:&g blue:&b];

	RenderEntity * sre = [shot getRenderEntity];
	sre.acceleration   = pd.acceleration;
	sre.turnSpeed      = pd.turnSpeed;
	sre.allowMovement  = NO;

	if (pd.trailSpriteName != nil)
	{
		TrailRenderParams trail = pd.trailParams;
		trail.r = r;
		trail.g = g;
		trail.b = b;
		[sre enableTrailRender:&trail];
	}
	else
	{
		sre.color = [CCColor colorWithRed:r green:g blue:b];
	}

	// Arbitrary rotation of the projectile:
	const float radians = CC_DEGREES_TO_RADIANS(renderEntity.rotation);
	[sre setTargetAngle:renderEntity.rotation];
	sre.position = renderEntity.position;
	sre.rotation = renderEntity.rotation;
	[shot setFreeFireVectorX:sin(radians) andY:cos(radians)];

	if (pd.aiType == AI_HOMING_MISSILE)
	{
		// "smart' projectiles like homing missiles
		// will use acquireTarget to select a target when fired.
		// The rest will be a no-op.
		EntityAI * ai = [EntityAI createAIOfType:pd.aiType];
		[shot setAI:ai];
		[ai acquireTarget];
	}

	[shot setDamageInflicted:pd.damageInflicted];
	[shot setParent:self];
	[Stage linkEntity:shot];

	// Update weapon fire-rate time track:
	pd.lastTimeFired = Time.currentTime;

	// Decrement shot owner's ammo:
	numBatteries -= pd.batteriesCost;
	numMissiles  -= pd.missileCost;

	// Fire sound effect:
	[SoundSys playSoundAsync:pd.sound];
}

- (void) setShieldBatteryCost: (int) cost
{
	shieldCost = cost;
}

- (int) getShieldBatteryCost
{
	return shieldCost;
}

- (void) setShieldHealth: (int) amount
{
	shieldHealth = amount;
}

- (int) getShieldHealth
{
	return shieldHealth;
}

- (void) setShieldScale: (float) scale
{
	shieldScale = scale;
}

- (float) getShieldScale
{
	return shieldScale;
}

- (BOOL) hasShield
{
	return shield != nil;
}

- (GameEntity *) getShield
{
	return shield;
}

- (BOOL) activateShield
{
	if ([self hasShield])
	{
		// Already has a shield!
		return NO;
	}

	if (numBatteries < shieldCost)
	{
		// Not enough batteries to start a shield
		if (self == Session.player)
		{
			[SoundSys playSoundAsync:SND_ALERT];
		}
		return NO;
	}

	numBatteries -= shieldCost;

	// Spawn shield entity:
	shield = [GameEntity spawn:GE_SHIELD];
	[shield setParent:self];
	[shield setHealth:[self getShieldHealth]];
	[shield setMaxHealth:[self getShieldHealth]];
	[shield setShieldScale:[self getShieldScale]];

	[shield setGraphics:@"shield" withScale:0.0f];
	RenderEntity * re = [shield getRenderEntity];

	// This is actually the owner's type...
	// Feels like a hack :P
	re.entityType = type;

	// Make the object rotate continuously:
	CCActionRepeatForever * actionRotate = [CCActionRepeatForever actionWithAction:[CCActionRotateBy actionWithDuration:0.01f angle:1]];
	[re runAction:actionRotate];
	re.allowRotation = NO; // Will be rotated by the CCAction
	re.allowMovement = NO; // Will always follow the owner

	// Set other states and link:
	float r,g,b;
	[re getDamageColorRed:&r green:&g blue:&b];
	re.color = [CCColor colorWithRed:r green:g blue:b];

	const int shieldDmg = ([self getDamageInflicted] > 0) ? ([self getDamageInflicted] / 2) : 0;
	[shield setDamageInflicted:shieldDmg];
	[shield setPosition:[self getPosition]];
	[shield setSortOrderForType];
	[shield setAlive:YES];

	[Stage linkEntity:shield];
	[SoundSys playSoundAsync:SND_ACTIVATE_SHIELD];
	return YES;
}

@end
