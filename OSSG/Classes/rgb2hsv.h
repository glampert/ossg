//
//  rgb2hsv.h
//   RGB to HSV color conversion. Adapted from code found on Stack Overflow thread:
//   http://stackoverflow.com/questions/15095909/from-rgb-to-hsv-in-opengl-glsl/17897228#17897228
//
//  Created by Guilherme R. Lampert on 29/04/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#ifndef RGB2HSV_H_
#define RGB2HSV_H_

// ======================================================
// RGB color vector to Hue(huehue) Saturation Value.
// ======================================================

typedef struct {
	float x,y,z,w;
} vec4_t;

static vec4_t mix(vec4_t a, vec4_t b, float t)
{
	vec4_t c;
	c.x = ((b.x - a.x) * t + a.x);
	c.y = ((b.y - a.y) * t + a.y);
	c.z = ((b.z - a.z) * t + a.z);
	c.w = ((b.w - a.w) * t + a.w);
	return c;
}

static float step(float edge, float x)
{
	if (x < edge)
	{
		return 0.0f;
	}
	return 1.0f;
}

static void rgb2hsv(float r, float g, float b, float hsv[3])
{
	const vec4_t K = { 0.0f, -1.0f / 3.0f, 2.0f / 3.0f, -1.0f };
	const vec4_t C = { b, g, K.w, K.z };
	const vec4_t F = { g, b, K.x, K.y };
	const vec4_t P = mix(C, F, step(b, g));

	const vec4_t T = { P.x, P.y, P.w, r   };
	const vec4_t W = { r,   P.y, P.z, P.x };
	const vec4_t Q = mix(T, W, step(P.x, r));

	const float d = Q.x - ((Q.w < Q.y) ? Q.w : Q.y);
	const float e = 1.0e-10f;

	hsv[0] = fabsf(Q.z + (Q.w - Q.y) / (6.0f * d + e));
	hsv[1] = d / (Q.x + e);
	hsv[2] = Q.x;
}

#endif // RGB2HSV_H_
