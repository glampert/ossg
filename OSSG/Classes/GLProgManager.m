//
//  GLProgManager.m
//  OSSG
//
//  Created by Guilherme R. Lampert on 29/04/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "GLProgManager.h"

// ======================================================
// Helper macros:
// ======================================================

#define GLPROG_LIST_BEGIN()\
static struct {\
	GLuint glid;\
	const char * name;\
	const char * vs;\
	const char * fs;\
	const VertexAttrib * const * vtxAttribs;\
} customPrograms[] = {
#define DECLARE_GLPROG(progId) { 0, #progId, progId##_vs, progId##_fs, progId##_vtxAttribs },
#define GLPROG_LIST_END() };

// Accessors:
#define GLPROG_GLID(progId)              customPrograms[(int)(progId)].glid
#define GLPROG_NAME(progId)              customPrograms[(int)(progId)].name
#define GLPROG_VS_SOURCE(progId)         customPrograms[(int)(progId)].vs
#define GLPROG_FS_SOURCE(progId)         customPrograms[(int)(progId)].fs
#define GLPROG_VERTEX_ATTRIBUTES(progId) customPrograms[(int)(progId)].vtxAttribs

// ======================================================
// List of custom program matching the GLProgId enum:
// ======================================================

// Every program id declared in the GLProgId enum
// must be defined here:

extern const char GLPROG_COLOR_REPLACE_vs[];
extern const char GLPROG_COLOR_REPLACE_fs[];
extern const VertexAttrib * GLPROG_COLOR_REPLACE_vtxAttribs[];

extern const char GLPROG_FADE_SCREEN_vs[];
extern const char GLPROG_FADE_SCREEN_fs[];
extern const VertexAttrib * GLPROG_FADE_SCREEN_vtxAttribs[];

extern const char GLPROG_BACKGROUND_OBJECTS_vs[];
extern const char GLPROG_BACKGROUND_OBJECTS_fs[];
extern const VertexAttrib * GLPROG_BACKGROUND_OBJECTS_vtxAttribs[];

extern const char GLPROG_BACKGROUND_IMAGE_vs[];
extern const char GLPROG_BACKGROUND_IMAGE_fs[];
extern const VertexAttrib * GLPROG_BACKGROUND_IMAGE_vtxAttribs[];

GLPROG_LIST_BEGIN()
	DECLARE_GLPROG(GLPROG_COLOR_REPLACE)
	DECLARE_GLPROG(GLPROG_FADE_SCREEN)
	DECLARE_GLPROG(GLPROG_BACKGROUND_OBJECTS)
	DECLARE_GLPROG(GLPROG_BACKGROUND_IMAGE)
GLPROG_LIST_END()

// ======================================================
// GLProgManager implementation:
// ======================================================

@implementation GLProgManager
{
	// Save and restore Cocos2d GL prog when drawing with our custom programs.
	GLint oldProgram;
}

// Keep track of accidental multiple initializations.
static BOOL glProgMgrCreated = NO;

//
// Methods:
//

- (void) printInfoLogs: (GLuint) progId vs: (GLuint) vsId fs: (GLuint) fsId
{
	GLsizei charsWritten;
	char infoLogBuf[4096];

	charsWritten = 0;
	glGetShaderInfoLog(vsId, OSSG_ARRAY_LEN(infoLogBuf) - 1, &charsWritten, infoLogBuf);
	if (charsWritten > 0)
	{
		infoLogBuf[OSSG_ARRAY_LEN(infoLogBuf) - 1] = '\0';
		OSSG_LOG(@"------ VS INFO LOG ------ \n%s\n", infoLogBuf);
	}

	charsWritten = 0;
	glGetShaderInfoLog(fsId, OSSG_ARRAY_LEN(infoLogBuf) - 1, &charsWritten, infoLogBuf);
	if (charsWritten > 0)
	{
		infoLogBuf[OSSG_ARRAY_LEN(infoLogBuf) - 1] = '\0';
		OSSG_LOG(@"------ FS INFO LOG ------ \n%s\n", infoLogBuf);
	}

	charsWritten = 0;
	glGetProgramInfoLog(progId, OSSG_ARRAY_LEN(infoLogBuf) - 1, &charsWritten, infoLogBuf);
	if (charsWritten > 0)
	{
		infoLogBuf[OSSG_ARRAY_LEN(infoLogBuf) - 1] = '\0';
		OSSG_LOG(@"------ PROGRAM INFO LOG ------ \n%s\n", infoLogBuf);
	}
}

- (GLuint) createSingleProgram: (const char *) name vs: (const char *)vsSrc fs: (const char *) fsSrc vertexAttribs: (const VertexAttrib * const *) vtxAttribs
{
	OSSG_ASSERT(name  != NULL);
	OSSG_ASSERT(vsSrc != NULL);
	OSSG_ASSERT(fsSrc != NULL);
	OSSG_ASSERT(vtxAttribs != NULL);

	// Allocate ids:
	GLuint progId = glCreateProgram();
	GLuint vsId   = glCreateShader(GL_VERTEX_SHADER);
	GLuint fsId   = glCreateShader(GL_FRAGMENT_SHADER);

	if ((progId <= 0) || (vsId <= 0) || (fsId <= 0))
	{
		OSSG_LOG(@"Problems when creating shader program '%s'. Failed to alloc GL ids!", name);
		if (glIsProgram(progId)) { glDeleteProgram(progId); }
		if (glIsShader(vsId))    { glDeleteShader(vsId);    }
		if (glIsShader(fsId))    { glDeleteShader(fsId);    }
		return 0;
	}

	// Compile & attach shaders:
	glShaderSource(vsId, 1, (const GLchar **)&vsSrc, NULL);
	glCompileShader(vsId);
	glAttachShader(progId, vsId);
	glShaderSource(fsId, 1, (const GLchar **)&fsSrc, NULL);
	glCompileShader(fsId);
	glAttachShader(progId, fsId);

	// Set vertex attributes:
	for (int i = 0; vtxAttribs[i] != NULL; ++i)
	{
		glBindAttribLocation(progId, vtxAttribs[i]->index, vtxAttribs[i]->name);
	}

	// Link the shader program:
	glLinkProgram(progId);

	// Print errors/warnings for the just compiled shaders and program:
	[self printInfoLogs:progId vs:vsId fs:fsId];

	// After attached to a program the shader objects can be deleted.
	glDeleteShader(vsId);
	glDeleteShader(fsId);

	return progId;
}

- (void) loadCustomPrograms
{
	OSSG_ASSERT(OSSG_ARRAY_LEN(customPrograms) == NUM_GLPROGS);
	OSSG_LOG(@"Loading custom GL shader programs...");

	for (int i = 0; i < NUM_GLPROGS; ++i)
	{
		OSSG_LOG(@"Creating custom shader program '%s'...", GLPROG_NAME(i));
		GLPROG_GLID(i) = [self createSingleProgram:GLPROG_NAME(i)
		                                        vs:GLPROG_VS_SOURCE(i)
		                                        fs:GLPROG_FS_SOURCE(i)
		                             vertexAttribs:GLPROG_VERTEX_ATTRIBUTES(i)];
		if (GLPROG_GLID(i) != 0)
		{
			OSSG_LOG(@"Custom GL prog '%s' created.", GLPROG_NAME(i));
		}
	}
}

- (id) init
{
	OSSG_ASSERT(glProgMgrCreated == NO);

	self = [super init];
	if (!self)
	{
		return nil;
	}

	oldProgram = 0;
	[self loadCustomPrograms];

	glProgMgrCreated = YES;
	OSSG_LOG(@"GLProgManager initialized...");
	return self;
}

- (void) dealloc
{
	// Cleanup. Not strictly necessary for singletons.
	for (int i = 0; i < NUM_GLPROGS; ++i)
	{
		if (GLPROG_GLID(i))
		{
			glDeleteProgram(GLPROG_GLID(i));
			GLPROG_GLID(i) = 0;
		}
	}
}

- (void) enableProgram: (GLProgId) prog
{
	OSSG_ASSERT((int)prog < NUM_GLPROGS);

	// Save current so that we ca [restore] it afterwards
	// to allows further Cocos2d drawings.
	oldProgram = 0;
	glGetIntegerv(GL_CURRENT_PROGRAM, &oldProgram);

	if (GLPROG_GLID(prog) == 0)
	{
		OSSG_LOG(@"Attention! Custom shader program '%s' (%d) is null!",
			GLPROG_NAME(prog), (int)prog);
		return;
	}

	// Set custom shader program:
	glUseProgram(GLPROG_GLID(prog));
}

- (void) restore
{
	// Restore Cocos2d shader:
	glUseProgram(oldProgram);
}

- (GLint) getProgramParamHandle: (GLProgId) prog paramName: (NSString *) name
{
	OSSG_ASSERT(name != nil);
	OSSG_ASSERT((int)prog < NUM_GLPROGS);

	const char * nameCStr = [name cStringUsingEncoding:NSUTF8StringEncoding];
	GLint loc = glGetUniformLocation(GLPROG_GLID(prog), nameCStr);

	if (loc == -1)
	{
		OSSG_LOG(@"Failed to get location for shader uniform '%@'. ProgId: '%s' (%d)",
			name, GLPROG_NAME(prog), (int)prog);
	}

	return loc;
}

- (BOOL) setProgramParam: (GLint) paramId intValue: (int) val
{
	if (paramId != -1)
	{
		glUniform1i(paramId, val);
		return YES;
	}
	return NO;
}

- (BOOL) setProgramParam: (GLint) paramId floatValue: (float) val
{
	if (paramId != -1)
	{
		glUniform1f(paramId, val);
		return YES;
	}
	return NO;
}

- (BOOL) setProgramParam: (GLint) paramId floatVec4: (const float *) val
{
	OSSG_ASSERT(val != nil);
	if (paramId != -1)
	{
		glUniform4f(paramId, val[0], val[1], val[2], val[3]);
		return YES;
	}
	return NO;
}

- (BOOL) setProgramParam: (GLint) paramId floatVec3: (const float *) val
{
	OSSG_ASSERT(val != nil);
	if (paramId != -1)
	{
		glUniform3f(paramId, val[0], val[1], val[2]);
		return YES;
	}
	return NO;
}

- (BOOL) setProgramParam: (GLint) paramId floatMat4: (const float *) val
{
	OSSG_ASSERT(val != nil);
	if (paramId != -1)
	{
		glUniformMatrix4fv(paramId, 1, GL_FALSE, val);
		return YES;
	}
	return NO;
}

@end
