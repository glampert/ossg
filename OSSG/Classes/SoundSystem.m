//
//  SoundSystem.m
//  OSSG
//
//  Created by Guilherme R. Lampert on 22/04/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "SoundSystem.h"

//
// TODO:
// - Should add a sound queue do remove duplicated entries.
// - Sounds need some volume adjusting I think...
//

#define STD_SOUND_FILE_EXT @".mp3"

// ======================================================
// Our list of sound effects and musics:
// ======================================================

static NSString * soundNames[NUM_SOUNDS] = {
	@"snd_fire_main_weap"  STD_SOUND_FILE_EXT,
	@"snd_fire_missile"    STD_SOUND_FILE_EXT,
	@"snd_menu_change"     STD_SOUND_FILE_EXT,
	@"snd_btn_click"       STD_SOUND_FILE_EXT,
	@"snd_alert"           STD_SOUND_FILE_EXT,
	@"snd_upgrade"         STD_SOUND_FILE_EXT,
	@"snd_activate_shield" STD_SOUND_FILE_EXT,
	@"snd_got_powerup"     STD_SOUND_FILE_EXT
};

static NSString * bgMusicNames[NUM_BG_MUSICS] = {
	@"bgm_menu"    STD_SOUND_FILE_EXT,
	@"bgm_in_game" STD_SOUND_FILE_EXT
};

// ======================================================
// SoundSystem implementation:
// ======================================================

@implementation SoundSystem

// Keep track of accidental redundant calls.
static BOOL soundsPreloaded = NO;

//
// Methods:
//

+ (SoundSystem *) createInstance: (BOOL) nullSystem
{
	if (nullSystem)
	{
		return nil;
	}
	else
	{
		return [SoundSystem new];
	}
}

- (id) init
{
	self = [super init];
	if (!self)
	{
		return nil;
	}

	OSSG_LOG(@"SoundSystem initialized...");
	return self;
}

- (BOOL) areSoundsPreloaded
{
	return soundsPreloaded;
}

- (void) preloadSounds
{
	if (soundsPreloaded)
	{
		return; // Already done.
	}

	OSSG_LOG(@"Pre-loading all sounds and musics...");

	OALSimpleAudio * oal = [OALSimpleAudio sharedInstance];
	oal.preloadCacheEnabled = YES;

	for (size_t i = 0; i < OSSG_ARRAY_LEN(soundNames); ++i)
	{
		[oal preloadEffect:soundNames[i]];
	}

	// We can only preload one background sound at a time.
	// Start with the menu tune:
	[oal preloadBg:bgMusicNames[BGM_MENU]];

	soundsPreloaded = YES;
	OSSG_LOG(@"Sound pre-load completed.");
}

- (void) playSoundAsync: (SoundId) which
{
	const int soundIndex = (int)which;
	OSSG_ASSERT(soundIndex >= 0 && soundIndex < NUM_SOUNDS);

	OALSimpleAudio * oal = [OALSimpleAudio sharedInstance];
	[oal playEffect:soundNames[soundIndex]];
}

- (void) playSoundAsync: (SoundId) which withVolume: (float) volume looping: (BOOL) loop
{
	const int soundIndex = (int)which;
	OSSG_ASSERT(soundIndex >= 0 && soundIndex < NUM_SOUNDS);

	OALSimpleAudio * oal = [OALSimpleAudio sharedInstance];
	[oal playEffect:soundNames[soundIndex] volume:volume pitch:1 pan:0 loop:loop];
}

- (void) playBgMusic: (BgMusicId) which withVolume: (float) volume looping: (BOOL) loop
{
	const int musicIndex = (int)which;
	OSSG_ASSERT(musicIndex >= 0 && musicIndex < NUM_BG_MUSICS);

	OALSimpleAudio * oal = [OALSimpleAudio sharedInstance];
	[oal playBg:bgMusicNames[musicIndex] volume:volume pan:0 loop:loop];
}

- (void) stopBgMusic
{
	[[OALSimpleAudio sharedInstance] stopBg];
}

- (void) stopSounds
{
	[[OALSimpleAudio sharedInstance] stopAllEffects];
}

- (void) stopEverything
{
	[[OALSimpleAudio sharedInstance] stopEverything];
}

@end
