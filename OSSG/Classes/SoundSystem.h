//
//  SoundSystem.h
//  OSSG
//
//  Created by Guilherme R. Lampert on 22/04/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "OSSG.h"

// ======================================================
// Sound indexes:
// ======================================================

// Sound effects:
typedef enum eSoundId {
	SND_FIRE_MAIN_WEAP,
	SND_FIRE_MISSILE,
	SND_MENU_CHANGE,
	SND_BTN_CLICK,
	SND_ALERT,
	SND_UPGRADE,
	SND_ACTIVATE_SHIELD,
	SND_GOT_POWERUP,
	NUM_SOUNDS
} SoundId;

// Background music:
typedef enum eBgMusicId {
	BGM_MENU,
	BGM_IN_GAME,
	NUM_BG_MUSICS
} BgMusicId;

// ======================================================
// SoundSystem interface:
// ======================================================

//
// Sound effect and music manager.
// All game sounds are played by the Sound System.
//
// This class is a singleton. A global instance 'SoundSys'
// is provided for accessing the Sound System.
//
@interface SoundSystem : NSObject

// Factory method. Can create a functional Sound System
// or a null stub that disables sound playing for good.
+ (SoundSystem *) createInstance: (BOOL) nullSystem;

// Instance methods:
- (id)   init;
- (BOOL) areSoundsPreloaded;
- (void) preloadSounds;
- (void) playSoundAsync: (SoundId)   which;
- (void) playSoundAsync: (SoundId)   which withVolume: (float) volume looping: (BOOL) loop; // Volume is in [0,1] range
- (void) playBgMusic:    (BgMusicId) which withVolume: (float) volume looping: (BOOL) loop; // This is always asynchronous.
- (void) stopBgMusic;    // Stop background music and rewind.
- (void) stopSounds;     // Stop sound effects only.
- (void) stopEverything; // Stop all sounds + music.

@end
