//
//  GameSession.m
//  OSSG
//
//  Created by Guilherme R. Lampert on 27/03/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "GameSession.h"
#import "StageBackground.h"
#import "GameGUI_Menus.h"

// ======================================================
// Globals / constants:
// ======================================================

TimeVars        Time        = { (1.0 / 60.0), 0.0 };
CGRect          WorldBounds = { {0, 0}, {1024, 768} };
SoundSystem   * SoundSys    = nil;
GameSession   * Session     = nil;
GameGUI       * Gui         = nil;
GameStage     * Stage       = nil;
GLProgManager * GLProgMgr   = nil;

// Maximum stage number.
// Once reached, the player stops leveling up.
#define MAX_STAGE_NUM 999

// Every this many stages a "boss" is spawned at the end.
#define STAGES_BETWEEN_BOSSES 3

// Default player ship:
#define DEFAULT_SHIP_NAME @"light_fighter"

// Default weapon damages for a new game:
#define DEFAULT_MAIN_WEAP_DMG 30
#define DEFAULT_MISSILE_DMG   50

// Default player & shield health for new games:
#define DEFAULT_PLAYER_HEALTH 100
#define DEFAULT_SHIELD_HEALTH 70

// Player's secondary weapon is always a missile.
#define WEAP_MISSILE WEAP_SECONDARY1

// ======================================================
// PlayerSaveState helper structure:
// ======================================================

// This struct is loaded an saved with a single fread/fwrite.
// It is packed and fields use small data types to speed reading/writing.
// We write it to storage on every successful end of a game stage to update
// the player states. See also savePlayerProgress/loadPlayerState.
#pragma pack(push, 1)
typedef struct sPlayerSaveState {

	// Id of this structure.
	uint32_t magic;

	// Player related states:
	uint32_t playerCredits;
	uint32_t playerBatteries;
	uint32_t playerMissiles;
	uint16_t playerHealth;
	uint16_t shieldHealth;
	uint16_t mainWeapDmg;
	uint16_t missileDmg;

	// Game/stage related states:
	uint16_t stagesToNextBoss;
	uint16_t stageNum;

	// Strings:
	unichar playerName[MAX_PLAYER_NAME_LEN];          // Printable player name string.
	unichar playerShipName[MAX_PLAYER_SHIP_NAME_LEN]; // Ship name, used to build the filename as: "player_cfg_<shipName>".

} PlayerSaveState;
#pragma pack(pop)

// 'magic' filed of PlayerSaveState.
// Helps a bit on detecting corrupted files.
#define SAVE_FILE_MAGIC OSSG_4CC('O', 'S', 'S', 'G')

// Added to player save file name:
#define SAVE_FILE_PREFIX  @"ossg_"
#define SAVE_FILE_POSTFIX @"_gm.sav"

// ======================================================
// GameSession implementation:
// ======================================================

@implementation GameSession
{
	//
	// Private data:
	//
	ProjectileDescriptor * pdMainWeap;         // Player's main weapon
	ProjectileDescriptor * pdMissileWeap;      // Player's secondary weapon slot 1
	StageBackground      * background;         // Background is loaded once the game starts and remains in memory till the end.
	NSString             * playerName;         // Name also used for save game filename.
	NSString             * playerNameFilename; // playerName adjusted to be used as a filename, for save files.
	NSString             * playerShipName;     // "player_cfg_<playerShipName>"
	BOOL                   stageEnded;
}

// Keep track of accidental multiple initializations.
static BOOL gameSessionCreated = NO;

//
// Methods:
//

- (id) init
{
	OSSG_ASSERT(gameSessionCreated == NO);

	self = [super init];
	if (!self)
	{
		return nil;
	}

	// Init instance variables with defaults:
	pdMainWeap           = nil;
	pdMissileWeap        = nil;
	background           = nil;
	playerName           = @"Player_1";
	playerNameFilename   = @"player_1";
	playerShipName       = DEFAULT_SHIP_NAME;
	stageEnded           = YES;
	_player              = nil;
	_playerCredits       = 0;
	_stageNum            = 1;
	_stagesBetweenBosses = STAGES_BETWEEN_BOSSES;
	_stagesToNextBoss    = STAGES_BETWEEN_BOSSES;
	_godMode             = NO;
	_enableSound         = YES;
	_showPerfStats       = NO;

	gameSessionCreated = YES;
	OSSG_LOG(@"GameSession initialized...");
	return self;
}

- (void) setGodMode: (BOOL) state
{
	if (state != _godMode)
	{
		_godMode = state;

		// Save property:
		[[NSUserDefaults standardUserDefaults]
			setBool:_godMode forKey:@"god-mode"];
		OSSG_LOG(@"Update config 'god-mode'...");
	}
}

- (void) setEnableSound: (BOOL) state
{
	if (state != _enableSound)
	{
		_enableSound = state;

		// Reset the sound system:
		SoundSys = [SoundSystem createInstance:(_enableSound ? NO : YES)];

		// Save property:
		[[NSUserDefaults standardUserDefaults]
			setBool:_enableSound forKey:@"enable-sound"];
		OSSG_LOG(@"Update config 'enable-sound'...");
	}
}

- (void) setShowPerfStats: (BOOL) state
{
	if (state != _showPerfStats)
	{
		_showPerfStats = state;

		// Save property:
		[[NSUserDefaults standardUserDefaults]
			setBool:_showPerfStats forKey:@"show-geeky-stats"];
		OSSG_LOG(@"Update config 'show-geeky-stats'...");
	}
}

- (void) initGameConfigs
{
	// Load local user configurations:
	id godModeProp = [[NSUserDefaults standardUserDefaults] objectForKey:@"god-mode"];
	id enableSoundProp = [[NSUserDefaults standardUserDefaults] objectForKey:@"enable-sound"];
	id showGeekyStatsProp = [[NSUserDefaults standardUserDefaults] objectForKey:@"show-geeky-stats"];

	if (godModeProp)
	{
		_godMode = [godModeProp boolValue];
	}
	else
	{
		// Write it for the first time.
		[[NSUserDefaults standardUserDefaults]
			setBool:_godMode forKey:@"god-mode"];
	}

	if (enableSoundProp)
	{
		_enableSound = [enableSoundProp boolValue];
	}
	else
	{
		// Write it for the first time.
		[[NSUserDefaults standardUserDefaults]
			setBool:_enableSound forKey:@"enable-sound"];
	}

	if (showGeekyStatsProp)
	{
		_showPerfStats = [showGeekyStatsProp boolValue];
	}
	else
	{
		// Write it for the first time.
		[[NSUserDefaults standardUserDefaults]
			setBool:_showPerfStats forKey:@"show-geeky-stats"];
	}
}

- (NSString *) getPlayerSaveFileWithPath
{
	// Path to "Documents/" dir:
	NSString * filename = [NSString stringWithFormat:@"%@%@%@", SAVE_FILE_PREFIX, playerNameFilename, SAVE_FILE_POSTFIX];
	return [GetUserHomePath() stringByAppendingPathComponent:filename];
}

- (BOOL) savePlayerProgress
{
	//
	// Write a PlayerSaveState instance to the save file.
	// Overwrites current.
	//
	PlayerSaveState save;
	memset(&save, 0, sizeof(PlayerSaveState));

	save.magic            = SAVE_FILE_MAGIC;
	save.playerCredits    = _playerCredits;
	save.playerBatteries  = (_player != nil) ? [_player getNumBatteries] : 0;
	save.playerMissiles   = (_player != nil) ? [_player getNumMissiles]  : 0;
	save.playerHealth     = (_player != nil) ? [_player getHealth]       : DEFAULT_PLAYER_HEALTH;
	save.shieldHealth     = (_player != nil) ? [_player getShieldHealth] : DEFAULT_SHIELD_HEALTH;
	save.mainWeapDmg      = (_player != nil) ? [_player getProjectileDescriptor:WEAP_MAIN].damageInflicted    : DEFAULT_MAIN_WEAP_DMG;
	save.missileDmg       = (_player != nil) ? [_player getProjectileDescriptor:WEAP_MISSILE].damageInflicted : DEFAULT_MISSILE_DMG;
	save.stagesToNextBoss = _stagesToNextBoss;
	save.stageNum         = _stageNum;

	// Copy names:
	NSRange r;
	r = NSMakeRange(0, MIN(playerName.length, MAX_PLAYER_NAME_LEN-1));
	[playerName getCharacters:save.playerName range:r];
	r = NSMakeRange(0, MIN(playerShipName.length, MAX_PLAYER_SHIP_NAME_LEN-1));
	[playerShipName getCharacters:save.playerShipName range:r];

	// Get full file+path name:
	NSString * saveFilePath = [self getPlayerSaveFileWithPath];
	OSSG_LOG(@"Saving player progress to \"%@\"...", saveFilePath);

	// File writing:
	FILE * fp = fopen([saveFilePath cStringUsingEncoding:NSUTF8StringEncoding], "wb");
	if (!fp)
	{
		OSSG_ERROR(@"Failed to save user progress to file \"%@\"!", saveFilePath);
		return NO;
	}

	const size_t itemsWritten = fwrite(&save, sizeof(PlayerSaveState), 1, fp);
	fclose(fp); // Done with the file.

	// Error checking:
	if (itemsWritten == 1)
	{
		OSSG_LOG(@"Player save file \"%@\" written successfully!", saveFilePath);
		return YES;
	}
	else
	{
		OSSG_ERROR(@"File IO error while trying to save user progress!\nSys err: '%s'", strerror(errno));
		return NO;
	}
}

- (BOOL) loadPlayerState: (PlayerSaveState *) save fileName: (NSString *) fName
{
	//
	// Loads the contents of the save game file for the current player.
	//
	OSSG_ASSERT(save != NULL);

	// Either use explicit filename or build it from user name:
	NSString * saveFilePath;
	if (fName == nil)
	{
		saveFilePath = [self getPlayerSaveFileWithPath];
	}
	else
	{
		saveFilePath = [GetUserHomePath() stringByAppendingPathComponent:fName];
	}

	OSSG_LOG(@"Reading player progress from \"%@\"...", saveFilePath);

	FILE * fp = fopen([saveFilePath cStringUsingEncoding:NSUTF8StringEncoding], "rb");
	if (!fp)
	{
		// This is not an error message because the fist time
		// the save file might not yet exist.
		OSSG_LOG(@"Failed to open user save file \"%@\"...", saveFilePath);
		return NO;
	}

	const size_t itemsRead = fread(save, sizeof(PlayerSaveState), 1, fp);

	// Error checking:
	if ((itemsRead != 1) && ferror(fp))
	{
		fclose(fp);
		OSSG_ERROR(@"File IO error while trying to load user progress!\nSys err: '%s'", strerror(errno));
		return NO;
	}

	fclose(fp); // Done with the file.

	if (save->magic == SAVE_FILE_MAGIC)
	{
		OSSG_LOG(@"Player save file \"%@\" loaded successfully!", saveFilePath);
		return YES;
	}
	else
	{
		OSSG_ERROR(@"Save file \"%d\" appears to be corrupted! Bad magic...", saveFilePath);
		return NO;
	}
}

- (NSString *) getPlayerNameFromSaveFile: (NSString *) filename
{
	PlayerSaveState save;
	memset(&save, 0, sizeof(PlayerSaveState));

	FILE * fp = fopen([filename cStringUsingEncoding:NSUTF8StringEncoding], "rb");
	if (!fp)
	{
		OSSG_LOG(@"Failed to open user save file \"%@\"...", filename);
		return @"";
	}

	fread(&save, sizeof(PlayerSaveState), 1, fp);
	fclose(fp);

	unsigned int len = 0;
	while ((save.playerName[len] != 0) && (len < MAX_PLAYER_NAME_LEN-1))
	{
		len++;
	}

	return [NSString stringWithCharacters:save.playerName length:len];
}

- (NSString *) playerNameToFileName: (NSString *) name
{
	OSSG_ASSERT(name != nil);

	// Remove non-allowed chars:
	NSCharacterSet * doNotWant = [NSCharacterSet characterSetWithCharactersInString:@" \\/:.-+=*%$#@!~^&|(){};,?<>\"\'\t\n"];
	NSString * filename = [[name componentsSeparatedByCharactersInSet:doNotWant] componentsJoinedByString:@""];

	// Make lowercase and return:
	filename = [filename lowercaseString];
	return filename;
}

- (NSDictionary *) getSaveFileList
{
	NSMutableDictionary * dict = [NSMutableDictionary dictionary];

	// List contents of the save path (Documents/ dir):
	NSString * home = GetUserHomePath();
	NSArray * directoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:home error:nil];

	if (directoryContents)
	{
		for (NSString * filename in directoryContents)
		{
			// If the file name has both the save file prefix and postfix,
			// there are good chances this is an OSSG save file.
			if (([filename rangeOfString:SAVE_FILE_PREFIX].location  != NSNotFound) &&
				([filename rangeOfString:SAVE_FILE_POSTFIX].location != NSNotFound))
			{
				[dict setObject:[self getPlayerNameFromSaveFile:[home stringByAppendingPathComponent:filename]] forKey:filename];
			}
		}
	}
	else
	{
		OSSG_LOG(@"[NSFileManager contentsOfDirectoryAtPath] failed!");
	}

	return dict;
}

- (void) setPlayerMainWeaponDamage: (int)mainDmg missileDamage: (int)missileDmg
{
	//
	// Player's main weapon:
	// Energy cannon (requires no ammo)
	//
	if (pdMainWeap == nil)
	{
		// Constant parameters:
		pdMainWeap = [ProjectileDescriptor new];
		pdMainWeap.spriteName       = @"shot";
		pdMainWeap.trailSpriteName  = nil;
		pdMainWeap.aiType           = AI_NONE;
		pdMainWeap.sound            = SND_FIRE_MAIN_WEAP;
		pdMainWeap.spriteScale      = 1;
		pdMainWeap.batteriesCost    = 0;
		pdMainWeap.missileCost      = 0;
		pdMainWeap.fireCoolDownTime = 0.2f;
		pdMainWeap.lastTimeFired    = 0;
		pdMainWeap.acceleration     = 1000;
		pdMainWeap.turnSpeed        = 10;
	}
	pdMainWeap.damageInflicted = mainDmg;

	//
	// Player's secondary weapon:
	// Missile launcher (requires missile)
	//
	if (pdMissileWeap == nil)
	{
		// Constant parameters:
		pdMissileWeap = [ProjectileDescriptor new];
		pdMissileWeap.spriteName       = @"missile";
		pdMissileWeap.trailSpriteName  = @"streak";
		pdMissileWeap.aiType           = AI_NONE;
		pdMissileWeap.sound            = SND_FIRE_MISSILE;
		pdMissileWeap.spriteScale      = 0.7f;
		pdMissileWeap.batteriesCost    = 0;
		pdMissileWeap.missileCost      = 1;
		pdMissileWeap.fireCoolDownTime = 1.0f;
		pdMissileWeap.lastTimeFired    = 0;
		pdMissileWeap.acceleration     = 1000;
		pdMissileWeap.turnSpeed        = 10;
		pdMissileWeap.trailParams      = (TrailRenderParams) {
			/* baseTexture = */ "streak",
			/* rgba        = */ 0.0f,0.0f,0.0f,0.5f,
			/* fadeTimeSec = */ 0.5f,
			/* minSegments = */ 2,
			/* width       = */ 4,
			/* fastMode    = */ YES
		};
	}
	pdMissileWeap.damageInflicted = missileDmg;
}

- (BOOL) playerInit: (NSString *) saveFile
{
	OSSG_ASSERT(_player == nil);

	PlayerSaveState save;
	memset(&save, 0, sizeof(PlayerSaveState));

	// Try to load the save file:
	if (![self loadPlayerState:&save fileName:saveFile])
	{
		return NO;
	}

	// Get ship name from save:
	unsigned int len = 0;
	while ((save.playerShipName[len] != 0) && (len < MAX_PLAYER_SHIP_NAME_LEN-1))
	{
		len++;
	}
	playerShipName = [NSString stringWithCharacters:save.playerShipName length:len];
	OSSG_ASSERT((playerShipName != nil) && ![playerShipName isEqualToString:@""]);

	// Load player ship sprite & configurations:
	_player = [GameEntity spawnWithFile:[NSString stringWithFormat:@"player_cfg_%@", playerShipName]];
	[_player setHealth:save.playerHealth];

	// Default values for a new game:
	[self setPlayerMainWeaponDamage:save.mainWeapDmg missileDamage:save.missileDmg];

	// Main cannon and missile:
	[_player setProjectileDescriptor:pdMainWeap    forWeapon:WEAP_MAIN];
	[_player setProjectileDescriptor:pdMissileWeap forWeapon:WEAP_MISSILE];

	// Third weapon is the shield.
	[_player setShieldBatteryCost:2];
	[_player setShieldScale:0.8f];

	_playerCredits    = save.playerCredits;
	_stagesToNextBoss = save.stagesToNextBoss;
	_stageNum         = save.stageNum;

	[_player setNumBatteries:save.playerBatteries];
	[_player setNumMissiles:save.playerMissiles];
	[_player setShieldHealth:save.shieldHealth];

	OSSG_LOG(@"Player initialized!");
	return YES;
}

- (void) perStageSetup: (GameStage *) gs
{
	OSSG_ASSERT(gs != nil);

	// Background layer:
	[gs addChild:background z:ESORT_BACKGROUND];

	// Position player in the middle of the screen,
	// back against the left side of the world.
	const CGRect playerBox = [[_player getRenderEntity] boundingBox];
	[_player setPosition:ccp((playerBox.size.width - playerBox.size.width / 2), gs.contentSize.height / 2)];

	// Reset player health to max and make it visible:
	[_player setHealth:[_player getMaxHealth]];
	[_player getRenderEntity].visible = YES;
	[_player setAlive:YES];

	// Animate:
	[[_player getRenderEntity] playAnimation:@"player_anim" withMode:ANIM_PLAY_LOOP];

	// Link player to game stage:
	[gs linkEntity:_player];

	stageEnded = NO;
}

- (void) gameReset
{
	[RenderEntity unloadAllSpriteSheets];
	[background reset];

	[_player setDefaults];
	_player           = nil;
	_playerCredits    = 0;
	_stageNum         = 1;
	_stagesToNextBoss = self.stagesBetweenBosses;
	playerShipName    = DEFAULT_SHIP_NAME;
}

- (BOOL) startNewGameWithShip: (NSString *) shipName
{
	OSSG_LOG(@"Creating new game with ship \"%@\"...", shipName);

	// Reset everything.
	// Next playerInit will recreate the player instance.
	[self gameReset];

	// Set the ship name:
	playerShipName = shipName;

	// Create the save file for this game.
	// playerInit will load it and use the correct ship.
	[self savePlayerProgress];

	// And now create the player with defaults:
	return [self playerInit:nil];
}

- (BOOL) startNewGameWithSaveFile: (NSString  *) filename
{
	OSSG_LOG(@"Creating new game with save file \"%@\"...", filename);

	// Full reset, in case this is not the first game of this session.
	[self gameReset];

	// Load the save game:
	return [self playerInit:filename];
}

- (void) deleteAllSaveGames
{
	OSSG_LOG(@"Attention! Deleting all saved games...");

	NSFileManager * fileMgr = [NSFileManager defaultManager];
	NSDictionary * dict = [self getSaveFileList];

	for (NSString * filename in dict.allKeys)
	{
		[fileMgr removeItemAtPath:[GetUserHomePath() stringByAppendingPathComponent:filename] error:nil];
	}

	OSSG_LOG(@"Save games delete!");
}

- (void) endStage
{
	[Gui endStage];
	Stage = nil;
}

- (void) frameUpdate
{
	// Check end game condition:
	if (!stageEnded && (![_player isAlive] || [Stage stageCleared]))
	{
		stageEnded = YES;

		// Darken the screen if the player died:
		if (![_player isAlive])
		{
			[Gui fadeScreen];
		}

		// Wait 2 seconds before showing the popup.
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)),
		dispatch_get_main_queue(), ^{
			if ([_player isAlive]) // Victory!
			{
				[Gui showEndGameMenu:YES];

				// Increment states:
				_stagesToNextBoss--;
				_stageNum++;

				// Cap to max value:
				if (_stageNum > MAX_STAGE_NUM)
				{
					_stageNum = MAX_STAGE_NUM;
				}

				if (Stage.hasBoss)
				{
					_stagesToNextBoss = _stagesBetweenBosses;
				}

				[Session savePlayerProgress];
			}
			else // Failed
			{
				[Gui showEndGameMenu:NO];
				// Will have to retry current stage.
			}
		});
	}
}

- (void) setPlayerName: (NSString *) name
{
	OSSG_ASSERT(name.length < MAX_PLAYER_NAME_LEN);
	playerName = name;

	// Remove possible unwanted chars from playerName and make it lowercase:
	playerNameFilename = [self playerNameToFileName:playerName];
}

- (void) addPlayerCredits: (int) amount
{
	_playerCredits += amount;
}

- (void) subtractPlayerCredits: (int) amount
{
	_playerCredits -= amount;
}

- (int) getPlayerMissileUpgradeCost
{
	return (40 + (_stageNum * _stageNum) + ((int)log((double)_stageNum) * 4));
}

- (int) getPlayerMissileUpgradeIncrement
{
	return (1 + (int)log((double)_stageNum));
}

- (int) getPlayerMissileUpgradeLevel
{
	return [_player getProjectileDescriptor:WEAP_MISSILE].damageInflicted;
}

- (int) getPlayerShieldUpgradeCost
{
	return (50 + (_stageNum * _stageNum) + ((int)log((double)_stageNum) * 6));
}

- (int) getPlayerShieldUpgradeIncrement
{
	return (1 + (int)log((double)_stageNum));
}

- (int) getPlayerShieldUpgradeLevel
{
	return [_player getShieldHealth];
}

- (BOOL) tryUpgradePlayerMissiles
{
	const int cost = [self getPlayerMissileUpgradeCost];
	if (cost > _playerCredits)
	{
		return NO;
	}
	_playerCredits -= cost;

	const int increment = [self getPlayerMissileUpgradeIncrement];
	[_player getProjectileDescriptor:WEAP_MISSILE].damageInflicted += increment;
	pdMissileWeap.damageInflicted += increment;

	// Will have to trigger a game save:
	[Session savePlayerProgress];
	return YES;
}

- (BOOL) tryUpgradePlayerShield
{
	const int cost = [self getPlayerShieldUpgradeCost];
	if (cost > _playerCredits)
	{
		return NO;
	}
	_playerCredits -= cost;

	const int increment = [self getPlayerShieldUpgradeIncrement];
	const int current = [_player getShieldHealth];
	[_player setShieldHealth:(current + increment)];

	// Will have to trigger a game save:
	[Session savePlayerProgress];
	return YES;
}

- (void) commonInit
{
	//
	// Low-level one-time systems initialization.
	// Should always happen BEFORE gameInit!
	//

	SystemCommonInit();
	[self initGameConfigs];
	[GameEntity commonInit];

	// Create other helper singletons:
	SoundSys  = [SoundSystem createInstance:(_enableSound ? NO : YES)];
	Gui       = [GameGUI new];
	GLProgMgr = [GLProgManager new];

	// This one will reference GLProgMgr, so should be done last.
	[RenderEntity commonInit];

	// Done!
	OSSG_LOG(@"[GameSession commonInit] completed!");
}

- (void) gameInit
{
	//
	// Game related one-time initialization.
	// Must happen AFTER commonInit.
	//

	// Pre-load sounds:
	[SoundSys preloadSounds];

	// Background layer, created once and reused for every stage:
	background = [StageBackground new];

	// Done!
	OSSG_LOG(@"[GameSession gameInit] completed!");
}

+ (void) createSharedGameSession
{
	// First thing to call when the app starts.
	Session = [GameSession new];
}

@end
