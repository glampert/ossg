//
//  GameSession.h
//  OSSG
//
//  Created by Guilherme R. Lampert on 27/03/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "OSSG.h"

// ======================================================
// Helpers / game globals:
// ======================================================

// This includes a '\0' at the end.
#define MAX_PLAYER_NAME_LEN      32
#define MAX_PLAYER_SHIP_NAME_LEN 16

// Forward declarations:
@class SoundSystem;
@class GameSession;
@class GameStage;
@class GameGUI;
@class GLProgManager;

// Times are in seconds
typedef struct {
	double deltaTime;
	double currentTime;
} TimeVars;

// Game Globals:
extern TimeVars        Time;        // Always up-to-date after a [Stage update].
extern CGRect          WorldBounds; // World bounding box of the "playable" area of the screen.
extern SoundSystem   * SoundSys;    // Global sound-effect/music manager. Created once and constant through the application.
extern GameSession   * Session;     // Game glue code. Created once and constant through the application.
extern GameGUI       * Gui;         // User Interface manager. Created once and constant through the application.
extern GameStage     * Stage;       // Changes when a stage change. Don't keep references to it!
extern GLProgManager * GLProgMgr;   // OpenGL shader program manager for our custom shaders.

// ======================================================
// GameSession interface:
// ======================================================

//
// Global game state manager.
// A Game Session is created at app startup and remains active
// until shutdown.
//
// GameSession is a singleton. The unique global instance
// must be accessed via the 'Session' global variable.
// If you try to create an instance of GameSession yourself,
// you'll get and assertion.
//
@interface GameSession : NSObject

// Player / stage:
@property(nonatomic, readonly, strong) GameEntity * player;
@property(nonatomic, readonly) int playerCredits;       // Current credits. Saved to file.
@property(nonatomic, readonly) int stageNum;            // 0 to MAX_STAGE_NUM
@property(nonatomic, readonly) int stagesToNextBoss;    // When == 1, a boss will be at the end of the current stage.
@property(nonatomic, readonly) int stagesBetweenBosses; // Every so stages there is a boss at the end.

// Miscellaneous game configs:
@property(nonatomic) BOOL godMode;
@property(nonatomic) BOOL enableSound;
@property(nonatomic) BOOL showPerfStats;

// Creates the singleton instance.
+ (void) createSharedGameSession;

// Game-related methods:
- (void) commonInit;
- (void) gameInit;
- (void) perStageSetup:            (GameStage *) gs;
- (BOOL) startNewGameWithShip:     (NSString  *) shipName;
- (BOOL) startNewGameWithSaveFile: (NSString  *) filename;
- (void) deleteAllSaveGames;
- (void) endStage;
- (void) frameUpdate;

// Misc player helpers:
- (void) setPlayerName:         (NSString *) name;
- (void) addPlayerCredits:      (int) amount;
- (void) subtractPlayerCredits: (int) amount;
- (int)  getPlayerMissileUpgradeCost;
- (int)  getPlayerMissileUpgradeIncrement;
- (int)  getPlayerMissileUpgradeLevel;
- (int)  getPlayerShieldUpgradeCost;
- (int)  getPlayerShieldUpgradeIncrement;
- (int)  getPlayerShieldUpgradeLevel;
- (BOOL) tryUpgradePlayerMissiles;
- (BOOL) tryUpgradePlayerShield;

// Returns a list with all save files.
// Dictionary key is the actual undecorated filename.
// The item pointed by the filename is the player name as found in the file.
- (NSDictionary *) getSaveFileList;

@end
