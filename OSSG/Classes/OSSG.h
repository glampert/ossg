//
//  OSSG.h
//  Master include for the OSSG project. Exposes most of the useful stuff.
//
//  Created by Guilherme R. Lampert on 27/03/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#ifndef OSSG_H_
#define OSSG_H_

// ======================================================
// Apple/system imports:
// ======================================================

#import <stdint.h>
#import <assert.h>
#import <Foundation/Foundation.h>

// ======================================================
// Cocos2d library:
// ======================================================

#import "cocos2d.h"
#import "cocos2d-ui.h"

// ======================================================
// Local imports:
// ======================================================

#import "RenderEntity.h"
#import "GameEntity.h"
#import "GameGUI.h"
#import "GameStage.h"
#import "GameSession.h"
#import "SoundSystem.h"
#import "EntityAI.h"
#import "GLProgManager.h"

// ======================================================
// Miscellaneous utility functions:
// ======================================================

// Init system & common utilities:
void SystemCommonInit(void);

// Math helpers:
float ClampF(float x, float minimum, float maximum);
float LerpF(float a, float b, float t);
float MapRangeF(float x, float inMin, float inMax, float outMin, float outMax);
BOOL  EqualsWithinTol(float val1, float val2);
void  PointLerp(CGPoint * result, const CGPoint * a, const CGPoint * b, float t);
BOOL  PointEq(const CGPoint * a, const CGPoint * b);

// Random numbers:
void   SeedRandGen(void);
double RandInRange(double lowerBound, double upperBound);

// Timers:
int64_t AbsoluteTimeMillisecs(void);
double  AbsoluteTimeSeconds(void);

// Miscellaneous:
BOOL ResourceExists(NSString * filePath);
NSString * GetUserHomePath(void);
void DrawFullScreenQuad(void); // With OpenGL

// System dialog windows/popups:
void SystemAlert(NSString * title, NSString * message, ...);
void ShowCancelContinueSystemDlg(NSString * title, NSString * message, void(^callback)(BOOL));

// ======================================================
// Macros / global constants:
// ======================================================

// OSSG_ASSERT(): Fatal assertion error.
#define OSSG_ASSERT assert

// OSSG_ERROR(): Display error message to user.
#define OSSG_ERROR(...) SystemAlert(@"Error!", __VA_ARGS__)

// OSSG_LOG(): Print to standard log. Can be disabled.
#define OSSG_LOG(...) NSLog(__VA_ARGS__)

// OSSG_ARRAY_LEN(): Length in elements of static C arrays.
#define OSSG_ARRAY_LEN(x) (sizeof(x) / sizeof(x[0]))

// Four character code (4CC):
#define OSSG_4CC(c0, c1, c2, c3)\
	((uint32_t)(uint8_t)(c0)        |\
	((uint32_t)(uint8_t)(c1) << 8)  |\
	((uint32_t)(uint8_t)(c2) << 16) |\
	((uint32_t)(uint8_t)(c3) << 24))

#endif // OSSG_H_
