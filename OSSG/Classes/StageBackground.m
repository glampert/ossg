//
//  StageBackground.m
//  OSSG
//
//  Created by Guilherme R. Lampert on 29/05/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "StageBackground.h"
#import "CCTexture_Private.h" // for [CCTexture name]

// ======================================================
// Generic 3D object:
// ======================================================

typedef struct sObjVertex {
	float px,py,pz; // Position
	float nx,ny,nz; // Normal
	float u,v;      // Texture coords
} ObjVertex;

typedef struct sObj3d {
	GLsizei numVertexes;
	GLsizei numIndexes;
	GLuint vb,ib;
} Obj3d;

// ======================================================

Obj3d * Create3dObject(const float vertexes[][3], size_t numVertexes,
                       const float normals[][3],  size_t numNormals,
                       const float uvs[][2],      size_t numTexCoords,
                       const uint16_t indexes[],  size_t numIndexes,
                       GLint vbStorage)
{
	OSSG_ASSERT(numVertexes != 0);

	// Right now, these must be of the same size:
	OSSG_ASSERT(numVertexes == numNormals);
	OSSG_ASSERT(numVertexes == numTexCoords);

	// Allocate 3D object struct:
	Obj3d * obj = (Obj3d *)calloc(1, sizeof(Obj3d));
	OSSG_ASSERT(obj != NULL);
	obj->numVertexes = (GLsizei)numVertexes;
	obj->numIndexes  = (GLsizei)numIndexes;

	// Set up an interleaved vertex array
	// (much more efficient for iPhone and friend):
	ObjVertex * interleavedVerts = (ObjVertex *)malloc(numVertexes * sizeof(ObjVertex));
	OSSG_ASSERT(interleavedVerts != NULL);

	for (size_t i = 0; i < numVertexes; ++i)
	{
		ObjVertex * vert = &interleavedVerts[i];
		vert->px = vertexes[i][0];
		vert->py = vertexes[i][1];
		vert->pz = vertexes[i][2];
		vert->nx = normals[i][0];
		vert->ny = normals[i][1];
		vert->nz = normals[i][2];
		vert->u  = uvs[i][0];
		vert->v  = uvs[i][1];
	}

	// Crete GL buffers:
	glGenBuffers(1, &obj->vb);
	glBindBuffer(GL_ARRAY_BUFFER, obj->vb);
	glBufferData(GL_ARRAY_BUFFER, (numVertexes * sizeof(ObjVertex)), interleavedVerts, vbStorage);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &obj->ib);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, obj->ib);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, (numIndexes * sizeof(uint16_t)), indexes, vbStorage);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	free(interleavedVerts);
	return obj;
}

void Destroy3dObject(Obj3d * obj)
{
	if (obj == NULL)
	{
		return;
	}

	if (obj->vb)
	{
		glDeleteBuffers(1, &obj->vb);
	}

	if (obj->ib)
	{
		glDeleteBuffers(1, &obj->ib);
	}

	free(obj);
}

void Draw3dObject(const Obj3d * obj)
{
	//
	// This function can be used with any shader program.
	//
	OSSG_ASSERT(obj != NULL);

	// Bind vertex buffer:
	glBindBuffer(GL_ARRAY_BUFFER, obj->vb);

	// Set "vertex format":
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, /* pos.xyz */ 3, GL_FLOAT, GL_FALSE, sizeof(ObjVertex), (GLvoid *)0);

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, /* norm.xyz */ 3, GL_FLOAT, GL_FALSE, sizeof(ObjVertex), (GLvoid *)(sizeof(float) * 3));

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, /* uv */ 2, GL_FLOAT, GL_FALSE, sizeof(ObjVertex), (GLvoid *)((sizeof(float) * 3) + (sizeof(float) * 3)));

	// Bind index buffer:
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, obj->ib);

	// Draw all triangles of the object:
	glDrawElements(GL_TRIANGLES, obj->numIndexes, GL_UNSIGNED_SHORT, NULL);

	// Cleanup:
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Increment so that Cocos2d stats overlay gets updated:
	CC_INCREMENT_GL_DRAWS(1);
}

// ======================================================
// Planets / asteroids:
// ======================================================

typedef struct sAsteroid {
	Obj3d * obj;
	float   specularConcentration; // Value from 0 to 1 (wider, concentrated)
	float   specularIntensity;     // Value from 0 to 1
	float   scale;
	float   rotationDegrees;
	kmVec3  rotationAxis;
	kmVec3  translation;
	GLuint  surfaceTexture;
} Asteroid;

typedef struct sPlanet {
	Obj3d *    obj;
	GLuint     surfaceTexture;
	GLuint     atmosphereTexture;
	float      specularConcentration; // Value from 0 to 1 (wider, concentrated)
	float      specularIntensity;     // Value from 0 to 1
	float      scale;
	float      rotationYDegrees;
	kmVec3     translation;
	BOOL       renderAtmosphere;
	Asteroid * asteroids; // Optional asteroids orbiting the planet.
	int        numAsteroids;
} Planet;

typedef struct sLightSource {
	kmVec3 positionWorldSpace;
	kmVec3 ambientColor;
} LightSource;

// ======================================================

// Constants shared by all planets / asteroids:
static const float atmosphereExtraScale  = 0.03f; // Added to planet scale to simulate clouds
static const float planetRotationSpeed   = 0.5f;  // Multiplied by Time.deltaTime
static const float asteroidRotationSpeed = 10.0f; // Multiplied by Time.deltaTime

// Handles to the uniforms used by the background rendering shader:
static GLint u_MVPMatrix;
static GLint u_lightPos;
static GLint u_ambientContrib;
static GLint u_specularConcentration;
static GLint u_materialScale;

// ======================================================

kmVec3 RandAsteroidRotationAxis()
{
	kmVec3 v = { RandInRange(0, 10), RandInRange(0, 10), RandInRange(0, 10) };
	kmVec3Normalize(&v, &v);
	return v;
}

void DrawAsteroid(Asteroid * asteroid, const kmMat4 * vpMatrix, const kmMat4 * parentTransform, const LightSource * light)
{
	OSSG_ASSERT(asteroid != NULL);
	OSSG_ASSERT(vpMatrix != NULL);
	OSSG_ASSERT(light    != NULL);
	OSSG_ASSERT(asteroid->obj != NULL);

	kmMat4 matTranslation, matRotation, matScale;
	kmMat4 modelMatrix, mvpMatrix;

	if (parentTransform != NULL)
	{
		modelMatrix = *parentTransform;
	}
	else
	{
		kmMat4Identity(&modelMatrix);
	}

	// Position it:
	kmMat4Translation(&matTranslation,
		asteroid->translation.x,
		asteroid->translation.y,
		asteroid->translation.z);
	kmMat4Multiply(&modelMatrix, &modelMatrix, &matTranslation);

	// Rotate it:
	kmMat4RotationAxisAngle(&matRotation, &asteroid->rotationAxis,
		CC_DEGREES_TO_RADIANS(asteroid->rotationDegrees));
	kmMat4Multiply(&modelMatrix, &modelMatrix, &matRotation);

	// Advance planet rotation:
	asteroid->rotationDegrees += (Time.deltaTime * asteroidRotationSpeed);
	if (asteroid->rotationDegrees > 360.0f)
	{
		asteroid->rotationDegrees = 0.0f;
	}

	// Optional scale:
	if (asteroid->scale != 1.0f)
	{
		// Uniform (xyz) scale:
		kmMat4Scaling(&matScale,
			asteroid->scale,
			asteroid->scale,
			asteroid->scale);
		kmMat4Multiply(&modelMatrix, &modelMatrix, &matScale);
	}

	// Set MVP matrix:
	kmMat4Multiply(&mvpMatrix, vpMatrix, &modelMatrix);
	[GLProgMgr setProgramParam:u_MVPMatrix floatMat4:mvpMatrix.mat];

	// Set light position in model/object space:
	kmVec3 lightPosObjectSpace;
	kmVec3InverseTransform(&lightPosObjectSpace, &light->positionWorldSpace, &modelMatrix);
	[GLProgMgr setProgramParam:u_lightPos floatVec3:(float *)&lightPosObjectSpace];
	[GLProgMgr setProgramParam:u_ambientContrib floatVec3:(float *)&light->ambientColor];

	// Surface params:
	[GLProgMgr setProgramParam:u_specularConcentration floatValue:asteroid->specularConcentration];
	[GLProgMgr setProgramParam:u_materialScale floatValue:(asteroid->specularIntensity / (1.0f - asteroid->specularConcentration))];

	// Draw call:
	ccGLBindTexture2D(asteroid->surfaceTexture);
	Draw3dObject(asteroid->obj);
}

void DrawPlanet(Planet * planet, const kmMat4 * vpMatrix, const LightSource * light)
{
	OSSG_ASSERT(planet   != NULL);
	OSSG_ASSERT(vpMatrix != NULL);
	OSSG_ASSERT(light    != NULL);
	OSSG_ASSERT(planet->obj != NULL);

	kmVec3 lightPosObjectSpace;
	kmMat4 matRotation, matTranslation, matScale;
	kmMat4 modelMatrix, mvpMatrix;

	// Position it:
	kmMat4Translation(&matTranslation,
		planet->translation.x,
		planet->translation.y,
		planet->translation.z);

	// Rotate planet about its Y axis:
	kmMat4RotationY(&matRotation, CC_DEGREES_TO_RADIANS(planet->rotationYDegrees));

	// Advance planet rotation:
	planet->rotationYDegrees += (Time.deltaTime * planetRotationSpeed);
	if (planet->rotationYDegrees > 360.0f)
	{
		planet->rotationYDegrees = 0.0f;
	}

	// Combine transforms:
	kmMat4Multiply(&modelMatrix, &matTranslation, &matRotation);

	// Optional scale:
	if (planet->scale != 1.0f)
	{
		// Uniform (xyz) scale:
		kmMat4Scaling(&matScale,
			planet->scale,
			planet->scale,
			planet->scale);

		kmMat4Multiply(&modelMatrix, &modelMatrix, &matScale);
	}

	// Set light position in model/object space:
	kmVec3InverseTransform(&lightPosObjectSpace, &light->positionWorldSpace, &modelMatrix);
	[GLProgMgr setProgramParam:u_lightPos floatVec3:(float *)&lightPosObjectSpace];
	[GLProgMgr setProgramParam:u_ambientContrib floatVec3:(float *)&light->ambientColor];

	// Set MVP matrix:
	kmMat4Multiply(&mvpMatrix, vpMatrix, &modelMatrix);
	[GLProgMgr setProgramParam:u_MVPMatrix floatMat4:mvpMatrix.mat];

	// Surface params:
	[GLProgMgr setProgramParam:u_specularConcentration floatValue:planet->specularConcentration];
	[GLProgMgr setProgramParam:u_materialScale floatValue:(planet->specularIntensity / (1.0f - planet->specularConcentration))];

	// Draw the sphere object with texture:
	ccGLBindTexture2D(planet->surfaceTexture);
	Draw3dObject(planet->obj);

	// Optional orbiting asteroids:
	if (planet->numAsteroids > 0)
	{
		OSSG_ASSERT(planet->asteroids != NULL);
		for (int a = 0; a < planet->numAsteroids; ++a)
		{
			DrawAsteroid(&planet->asteroids[a], vpMatrix, &modelMatrix, light);
		}
	}

	// Optional atmosphere simulation:
	if (planet->renderAtmosphere)
	{
		// Rotate the clouds slightly faster:
		kmMat4RotationY(&matRotation, CC_DEGREES_TO_RADIANS(planet->rotationYDegrees * 1.8f));

		// Render a second planet with some scale and a texture with
		// transparency to simulate some clouds hovering the surface.
		// This is a lame effect, but should be fast enough and have a descent result.
		const float atmosphereScale = planet->scale + atmosphereExtraScale;
		kmMat4Scaling(&matScale, atmosphereScale, atmosphereScale, atmosphereScale);

		kmMat4Multiply(&modelMatrix, &matTranslation, &matRotation);
		kmMat4Multiply(&modelMatrix, &modelMatrix,    &matScale);
		kmMat4Multiply(&mvpMatrix,   vpMatrix,        &modelMatrix);

		// Set light position in model/object space:
		kmVec3InverseTransform(&lightPosObjectSpace, &light->positionWorldSpace, &modelMatrix);
		[GLProgMgr setProgramParam:u_lightPos floatVec3:(float *)&lightPosObjectSpace];
		[GLProgMgr setProgramParam:u_ambientContrib floatVec3:(float *)&light->ambientColor];

		// Set MVP:
		[GLProgMgr setProgramParam:u_MVPMatrix floatMat4:mvpMatrix.mat];

		// Atmosphere texture is assumed to have transparency:
		glEnable(GL_BLEND);

		ccGLBindTexture2D(planet->atmosphereTexture);
		Draw3dObject(planet->obj);
	}
}

// ======================================================
// StageBackground implementation:
// ======================================================

// Planet meshes as C arrays:
#import "sphereLow.h"
#import "sphereHigh.h"

// Asteroid meshes as C arrays:
#import "asteroid0.h"
#import "asteroid1.h"
#import "asteroid2.h"

#define NUM_ASTEROIDS 10

@implementation StageBackground
{
	//
	// Awesome planet textures from:
	// http://freebitmaps.blogspot.com.br/2010/10/srgb-planet-reststop.html
	//
	CCTexture * planet1DiffuseMap;
	CCTexture * planet2DiffuseMap;
	CCTexture * atmosphereMap; // Common atmosphere map (just few clouds with alpha)
	CCTexture * backgroundImage;

	// Three different asteroid textures to add variety:
	CCTexture * asteroidDiffuseMaps[3];

	// And three different asteroid meshes:
	Obj3d * asteroidMeshes[3];

	Planet planet1; // Planet closer to the camera
	Planet planet2; // More distant planet

	// Asteroid instances:
	Asteroid asteroids[NUM_ASTEROIDS];

	// Two light sources in the scene to simulate sun
	// and indirect lighting.
	LightSource light1;
	LightSource light2;
}

- (void) initPlanets
{
	// planet1:
	{
		// Planet 1 has atmosphere and some asteroids orbiting it:
		planet1.specularConcentration = 0.6f;
		planet1.specularIntensity     = 0.3f;
		planet1.scale                 = 1.2f;
		planet1.rotationYDegrees      = 90.0f;
		planet1.translation           = (kmVec3){ 2.0f, 0.0f, -3.0f };
		planet1.renderAtmosphere      = YES;
		planet1.surfaceTexture        = [planet1DiffuseMap name];
		planet1.atmosphereTexture     = [atmosphereMap name];
		planet1.asteroids             = asteroids;
		planet1.numAsteroids          = NUM_ASTEROIDS;

		// Planet 1 gets a higher poly-count mesh:
		planet1.obj = Create3dObject(
			sphereHighVertexes,  sphereHighVertexesCount,
			sphereHighNormals,   sphereHighNormalsCount,
			sphereHighTexCoords, sphereHighTexCoordsCount,
			sphereHighIndexes,   sphereHighIndexesCount,
			GL_STATIC_DRAW);
	}

	// planet2:
	{
		// Planet 1 has atmosphere and some asteroids orbiting it:
		planet2.specularConcentration = 0.2f;
		planet2.specularIntensity     = 0.2f;
		planet2.scale                 = 1.0f;
		planet2.rotationYDegrees      = 0.0f;
		planet2.translation           = (kmVec3){ -7.0f, 0.0f, -12.0f };
		planet2.renderAtmosphere      = NO;
		planet2.surfaceTexture        = [planet2DiffuseMap name];
		planet2.atmosphereTexture     = [atmosphereMap name];
		planet2.asteroids             = NULL;
		planet2.numAsteroids          = 0;

		// Planet 2 uses a low poly sphere as its mesh, since it is further away from the viewer:
		planet2.obj = Create3dObject(
			sphereLowVertexes,  sphereLowVertexesCount,
			sphereLowNormals,   sphereLowNormalsCount,
			sphereLowTexCoords, sphereLowTexCoordsCount,
			sphereLowIndexes,   sphereLowIndexesCount,
			GL_STATIC_DRAW);
	}
}

- (void) resetAsteroids
{
	//
	// Position the asteroids:
	//
	for (int i = 0; i < NUM_ASTEROIDS; ++i)
	{
		asteroids[i].specularConcentration = 0.6f;
		asteroids[i].specularIntensity     = 0.3f;
		asteroids[i].rotationDegrees       = 0.0f;
		asteroids[i].scale                 = 0.1f;
		asteroids[i].rotationAxis          = RandAsteroidRotationAxis();
		asteroids[i].surfaceTexture        = [asteroidDiffuseMaps[rand() % 3] name];

		// Randomize initial position within planet1's neighborhood:
		asteroids[i].translation = (kmVec3){
			planet1.translation.x + RandInRange( 3, 6),
			planet1.translation.y + RandInRange(-4, 4),
			planet1.translation.z + RandInRange(-2, 4)
		};
	}
}

- (void) initAsteroids
{
	[self resetAsteroids];

	//
	// Load asteroid meshes:
	//
	asteroidMeshes[0] = Create3dObject(
		asteroid0Vertexes,  asteroid0VertexesCount,
		asteroid0Normals,   asteroid0NormalsCount,
		asteroid0TexCoords, asteroid0TexCoordsCount,
		asteroid0Indexes,   asteroid0IndexesCount,
		GL_STATIC_DRAW);

	asteroidMeshes[1] = Create3dObject(
		asteroid1Vertexes,  asteroid1VertexesCount,
		asteroid1Normals,   asteroid1NormalsCount,
		asteroid1TexCoords, asteroid1TexCoordsCount,
		asteroid1Indexes,   asteroid1IndexesCount,
		GL_STATIC_DRAW);

	asteroidMeshes[2] = Create3dObject(
		asteroid2Vertexes,  asteroid2VertexesCount,
		asteroid2Normals,   asteroid2NormalsCount,
		asteroid2TexCoords, asteroid2TexCoordsCount,
		asteroid2Indexes,   asteroid2IndexesCount,
		GL_STATIC_DRAW);

	for (int i = 0; i < NUM_ASTEROIDS; ++i)
	{
		asteroids[i].obj = asteroidMeshes[rand() % 3];
	}
}

- (id) init
{
	self = [super init];
	if (!self)
	{
		return nil;
	}

	// Load texture maps:
	planet1DiffuseMap      = [CCTexture textureWithFile:@"planet1."    STD_SPRITE_SHEET_FILE_EXT];
	planet2DiffuseMap      = [CCTexture textureWithFile:@"planet2."    STD_SPRITE_SHEET_FILE_EXT];
	atmosphereMap          = [CCTexture textureWithFile:@"atmosphere." STD_SPRITE_SHEET_FILE_EXT];
	backgroundImage        = [CCTexture textureWithFile:@"background." STD_SPRITE_SHEET_FILE_EXT];
	asteroidDiffuseMaps[0] = [CCTexture textureWithFile:@"asteroid0."  STD_SPRITE_SHEET_FILE_EXT];
	asteroidDiffuseMaps[1] = [CCTexture textureWithFile:@"asteroid1."  STD_SPRITE_SHEET_FILE_EXT];
	asteroidDiffuseMaps[2] = [CCTexture textureWithFile:@"asteroid2."  STD_SPRITE_SHEET_FILE_EXT];

	[self initPlanets];
	[self initAsteroids];

	//
	// Set-up the light sources:
	//
	light1.positionWorldSpace = (kmVec3){ 0.0f,  1.5f, -4.0f  };
	light1.ambientColor       = (kmVec3){ 0.35f, 0.35f, 0.35f };
	light2.positionWorldSpace = (kmVec3){ 0.0f,  1.5f, -24.0f };
	light2.ambientColor       = (kmVec3){ 0.1f,  0.1f,  0.1f  };

	//
	// Cache uniform var handles:
	//
	GLint s_baseMap;

	u_MVPMatrix = [GLProgMgr getProgramParamHandle:GLPROG_BACKGROUND_OBJECTS paramName:@"u_MVPMatrix"];
	OSSG_ASSERT(u_MVPMatrix != -1);

	u_lightPos = [GLProgMgr getProgramParamHandle:GLPROG_BACKGROUND_OBJECTS paramName:@"u_lightPos"];
	OSSG_ASSERT(u_lightPos != -1);

	u_ambientContrib = [GLProgMgr getProgramParamHandle:GLPROG_BACKGROUND_OBJECTS paramName:@"u_ambientContrib"];
	OSSG_ASSERT(u_ambientContrib != -1);

	u_specularConcentration = [GLProgMgr getProgramParamHandle:GLPROG_BACKGROUND_OBJECTS paramName:@"u_specularConcentration"];
	OSSG_ASSERT(u_specularConcentration != -1);

	u_materialScale = [GLProgMgr getProgramParamHandle:GLPROG_BACKGROUND_OBJECTS paramName:@"u_materialScale"];
	OSSG_ASSERT(u_materialScale != -1);

	// Set one time initialized variables for GLPROG_BACKGROUND_OBJECTS:
	s_baseMap = [GLProgMgr getProgramParamHandle:GLPROG_BACKGROUND_OBJECTS paramName:@"s_baseMap"];
	OSSG_ASSERT(s_baseMap != -1);
	[GLProgMgr enableProgram:GLPROG_BACKGROUND_OBJECTS];
		[GLProgMgr setProgramParam:s_baseMap intValue:0];
	[GLProgMgr restore];

	// Set one time initialized variables for GLPROG_BACKGROUND_IMAGE:
	s_baseMap = [GLProgMgr getProgramParamHandle:GLPROG_BACKGROUND_IMAGE paramName:@"s_baseMap"];
	OSSG_ASSERT(s_baseMap != -1);
	[GLProgMgr enableProgram:GLPROG_BACKGROUND_IMAGE];
		[GLProgMgr setProgramParam:s_baseMap intValue:0];
	[GLProgMgr restore];

	return self;
}

- (void) dealloc
{
	Destroy3dObject(planet1.obj);
	Destroy3dObject(planet2.obj);

	for (int i = 0; i < 3; ++i)
	{
		Destroy3dObject(asteroidMeshes[i]);
		asteroidMeshes[i] = NULL;
	}
}

- (void) reset
{
	[self resetAsteroids];
	planet1.rotationYDegrees = 90.0f;
	planet2.rotationYDegrees = 0.0f;
}

- (void) draw
{
	// Draw the background image plane first.
	// We'll clear the depth buffer before rendering the planets,
	// so this will always be behind them.
	[GLProgMgr enableProgram:GLPROG_BACKGROUND_IMAGE];
	ccGLBindTexture2D([backgroundImage name]);
	DrawFullScreenQuad();
	[GLProgMgr restore];

	const CGSize viewSize = [CCDirector sharedDirector].viewSizeInPixels;
	const float aspect = (viewSize.width / viewSize.height);

	const GLboolean wasDepthEnabled    = glIsEnabled(GL_DEPTH_TEST);
	const GLboolean wasCullFaceEnabled = glIsEnabled(GL_CULL_FACE);
	const GLboolean wasBlendEnabled    = glIsEnabled(GL_BLEND);

	// Set proper states:
	if (wasDepthEnabled == GL_FALSE)
	{
		glEnable(GL_DEPTH_TEST);
	}
	if (wasCullFaceEnabled == GL_FALSE)
	{
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK); // Cull back facing polygons
	}
	if (wasBlendEnabled == GL_TRUE)
	{
		glDisable(GL_BLEND);
	}

	// Clear depth so the planets will be drawn in front
	// of the background image plane.
	glClear(GL_DEPTH_BUFFER_BIT);

	[GLProgMgr enableProgram:GLPROG_BACKGROUND_OBJECTS];

	kmMat4 vpMatrix;
	kmMat4PerspectiveProjection(&vpMatrix, 60.0f, aspect, 0.5f, 100.0f);

	DrawPlanet(&planet1, &vpMatrix, &light1);
	DrawPlanet(&planet2, &vpMatrix, &light2);

	// Cleanup:
	[GLProgMgr restore];

	if (wasBlendEnabled == GL_TRUE)
	{
		glEnable(GL_BLEND);
	}
	if (wasCullFaceEnabled == GL_FALSE)
	{
		glDisable(GL_CULL_FACE);
	}
	if (wasDepthEnabled == GL_FALSE)
	{
		glDisable(GL_DEPTH_TEST);
	}
}

@end
