//
//  RenderEntity.h
//  OSSG
//
//  Created by Guilherme R. Lampert on 27/03/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "OSSG.h"

// ======================================================
// Enums / constants / helpers:
// ======================================================

@class GameStage;

// Z-depth sorting of entities.
// Cocos2d Z decreases when moving into the screen
// and increases when moving out of the screen towards the viewer.
// Trails and projectiles should always go behind entities, so first in the enum!
typedef enum eEntitySortOrder {
	ESORT_BACKGROUND,        // Stage background image.
	ESORT_TRAIL,             // Smoke trail / ship trail render.
	ESORT_PROJECTILE,        // Projectile of any kind.
	ESORT_ENTITY,            // Generic game entity.
	ESORT_SHIELD,            // Shield hovering over the entity sprite.
	ESORT_PARTICLE,          // Particle systems.
	ESORT_UI_ELEMENT,        // In-game UI sprites.
	ESORT_FADE_SCREEN,       // Fade screen overlay for game pause / special events.
	ESORT_POPUP_MENU,        // In-game popup menu panel. Must be above the fade-screen overlay.
	ESORT_POPUP_MENU_BUTTON, // Buttons on top of ESORT_POPUP_MENU.
	ESORT_TOPMOST = ESORT_POPUP_MENU_BUTTON
} EntitySortOrder;

// Play modes for [RenderEntity playAnimation]
typedef enum eAnimPlayMode {
	ANIM_PLAY_ONCE,
	ANIM_PLAY_LOOP
} AnimPlayMode;

// Configuration parameters for a trail renderer.
typedef struct sTrailRenderParams {
	const char * textureFilename;
	float        r,g,b,a;
	float        fadeTimeSec;
	int          minSegments;
	int          width;
	BOOL         fastMode; // Lower accuracy but faster rendering.
} TrailRenderParams;

// File extension expected for sprite sheets and images:
#define STD_SPRITE_SHEET_FILE_EXT @"png"

// ======================================================
// RenderEntity interface:
// ======================================================

//
// Rendering component of a GameEntity.
// This class inherits from CCSprite (Cocos2d)
// and adds some game specific functionalities to it.
//
// Most rendering and animation code in encapsulated inside.
// Direct calls to Cocos2d should be avoided outside RenderEntity.
//
@interface RenderEntity : CCSprite

// Properties:
@property(nonatomic) int   entityType;
@property(nonatomic) float acceleration;        // Acceleration for [moveToPosition].
@property(nonatomic) float turnSpeed;           // Turn speed for auto rotation [setTargetAngle].
@property(nonatomic) float damageAmount;        // Amount of damage for the damage effect. Inverse of the entity health.
@property(nonatomic) BOOL  displayDamageEffect; // Displays the damage effect for the player and enemies. By default: NO.
@property(nonatomic) BOOL  allowMovement;       // Allows auto movement [moveToPosition]. By default: YES.
@property(nonatomic) BOOL  constraintToWorld;   // Constraint [moveToPosition] movement to world bounds. By default: NO.
@property(nonatomic) BOOL  allowRotation;       // Allows auto rotation [setTargetAngle]. By default YES.

// Class methods:
+ (void)            commonInit; // To be called once at startup
+ (void)            perStageSetup:          (GameStage *) gs;
+ (id)              createEmpty;
+ (id)              createWithSpriteName:   (NSString *) spriteName;
+ (BOOL)            loadSpriteSheet:        (NSString *) sheetName;
+ (void)            unloadAllSpriteSheets;

// Instance methods:
- (id)              init;
- (RenderEntity *)  clone; // Deep copy self into a new instance.
- (BOOL)            loadAnimation:          (NSArray *) frameNames spriteSheet: (NSString *) sheet animName: (NSString *) anim frameDelay: (float) seconds bindPose: (BOOL) yesNo;
- (BOOL)            playAnimation:          (NSString *) animName withMode: (AnimPlayMode) mode;
- (void)            stopAnimation;
- (void)            enableTrailRender:      (const TrailRenderParams *) params;
- (void)            disableTrailRender:     (BOOL) fullCleanup;
- (BOOL)            isTrailRenderEnabled;
- (void)            frameUpdate;
- (void)            moveToPosition:         (CGPoint) pos setTargetAngle: (BOOL) yesNo;
- (void)            stopMoving;
- (void)            setTargetAngle:         (float) degrees;
- (void)            setEntitySort:          (EntitySortOrder) sort;
- (EntitySortOrder) getEntitySort;
- (void)            getDamageColorRed:      (float *)r green: (float *)g blue: (float *)b;

// Only called internally by GameStage:
- (void)            stageLink:              (GameStage *) gs;
- (void)            stageUnlink:            (GameStage *) gs;

@end
