//
//  EnemyAIs.m
//  OSSG - All enemy AI implementations.
//
//  Created by Guilherme R. Lampert on 22/04/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "EnemyAIs.h"

// ======================================================
// AIStraightMover implementation:
// ======================================================

@implementation AIStraightMover

//
// Methods:
//

- (id) initWithType: (AIType) tp
{
	self = [super initWithType:tp];
	if (!self)
	{
		return nil;
	}

	self.firesHomingMissiles = NO;

	return self;
}

- (void) think
{
	// Move right to left:
	CGPoint p = [self.owner getPosition];
	p.x -= ([self.owner getRenderEntity].acceleration * Time.deltaTime);
	[self.owner setPosition:p];

	// Tag owner for removal if outside the screen.
	if (p.x < -([[self.owner getRenderEntity] boundingBox].size.width + 20))
	{
		[self.owner setAlive:NO];
		return;
	}

	// Only try to fire when inside the world bound.
	// (enemies usually start a bit off in the right side of the screen)
	if (p.x <= WorldBounds.size.width)
	{
		const int n = rand() % NUM_WEAPONS; // [0, NUM_WEAPONS-1]
		ProjectileDescriptor * pd = [self.owner getProjectileDescriptor:(WeaponId)n];
		if ((pd.aiType == AI_HOMING_MISSILE) && (self.firesHomingMissiles == NO))
		{
			return; // Skip
		}

		[self.owner fireWeapon:(WeaponId)n];
	}
}

- (EntityAI *) clone
{
	AIStraightMover * ai = [AIStraightMover new];

	ai.owner = self.owner;
	ai.type  = self.type;

	ai.firesHomingMissiles = self.firesHomingMissiles;
	return ai;
}

@end

// ======================================================
// AIWaveMover implementation:
// ======================================================

typedef double (* WaveFunc_t)(double);

@implementation AIWaveMover
{
	WaveFunc_t waveFunc;
	double currentValue;
}

//
// Methods:
//

- (id) initWithType: (AIType) tp
{
	self = [super initWithType:tp];
	if (!self)
	{
		return nil;
	}

	self.waveLength = RandInRange(1, 2);
	self.waveScale  = RandInRange(5, 10);
	currentValue    = 0;

	const int sel = rand() % 2 + 1; // [1,2]
	switch (sel)
	{
	case 1 :
		waveFunc = &sin;
		break;
	case 2 :
		waveFunc = &cos;
		break;
	default:
		OSSG_ERROR(@"rand() is broken!!!");
		break;
	} // switch (sel)

	return self;
}

- (void) think
{
	[super think];

	currentValue += (Time.deltaTime * self.waveLength);
	if (currentValue >= (2 * M_PI))
	{
		currentValue = 0;
	}

	CGPoint p = [self.owner getPosition];
	p.y += waveFunc(currentValue) * self.waveScale;
	if (p.y > (Stage.contentSize.height - 20))
	{
		return;
	}

	[self.owner setPosition:p];
}

- (EntityAI *) clone
{
	AIWaveMover * ai = [AIWaveMover new];

	ai.owner = self.owner;
	ai.type  = self.type;

	ai.firesHomingMissiles = self.firesHomingMissiles;

	ai.waveLength    = self.waveLength;
	ai.waveScale     = self.waveScale;
	ai->waveFunc     = waveFunc;
	ai->currentValue = currentValue;

	return ai;
}

@end

// ======================================================
// AIDroneMine implementation:
// ======================================================

@implementation AIDroneMine
{
	CCActionRepeatForever * actionRotate;
}

//
// Methods:
//

- (id) initWithType: (AIType) tp
{
	self = [super initWithType:tp];
	if (!self)
	{
		return nil;
	}

	actionRotate = nil;
	return self;
}

- (void) think
{
	if (actionRotate == nil)
	{
		actionRotate = [CCActionRepeatForever actionWithAction:[CCActionRotateBy actionWithDuration:0.01f angle:1]];
		[[self.owner getRenderEntity] runAction:actionRotate];
		[self.owner getRenderEntity].allowRotation = NO; // Will be rotated by the CCAction
	}

	[super think];

	// Move right to left:
	CGPoint p = [self.owner getPosition];
	p.x -= ([self.owner getRenderEntity].acceleration * Time.deltaTime);
	[self.owner setPosition:p];

	// Tag owner for removal if outside the screen.
	if (p.x < -([[self.owner getRenderEntity] boundingBox].size.width + 20))
	{
		[self.owner setAlive:NO];
	}
}

- (EntityAI *) clone
{
	AIDroneMine * ai = [AIDroneMine new];

	ai.owner = self.owner;
	ai.type  = self.type;

	ai->actionRotate = nil;

	return ai;
}

@end

// ======================================================
// AIBoss implementation:
// ======================================================

@implementation AIBoss
{
	double waveScale;
	double waveLength;
	double sineParam;
	int    xDir;
}

//
// Methods:
//

- (id) initWithType: (AIType) tp
{
	OSSG_ASSERT(tp == AI_BOSS);

	self = [super initWithType:tp];
	if (!self)
	{
		return nil;
	}

	waveScale  = 15.0;
	waveLength = 1.0;
	sineParam  = 0.0;
	xDir       = 1;

	return self;
}

- (void) think
{
	const CGRect bbox = [[self.owner getRenderEntity] boundingBox];
	CGPoint p = [self.owner getPosition];

	sineParam += (Time.deltaTime * waveLength);
	if (sineParam >= (2 * M_PI))
	{
		sineParam = 0;
	}

	// Move in the X axis til in firing range:
	if (p.x > (WorldBounds.size.width - (bbox.size.width / 2) - 5))
	{
		p.x -= ([self.owner getRenderEntity].acceleration * Time.deltaTime);
		xDir = 1;
	}
	else
	{
		double newX = p.x, newY = p.y;

		// Move in the X:
		if (xDir)
		{
			newX -= ([self.owner getRenderEntity].acceleration * Time.deltaTime);
		}
		else
		{
			newX += ([self.owner getRenderEntity].acceleration * Time.deltaTime);
		}

		if (newX < WorldBounds.origin.x + bbox.size.width + 20)
		{
			xDir = !xDir;
			newX = p.x;
		}

		// Move in the Y:
		newY += (sin(sineParam) * waveScale);
		if ((newY > WorldBounds.size.height) || (newY < WorldBounds.origin.y))
		{
			newY = p.y;
		}

		p.x = newX;
		p.y = newY;
	}

	[self.owner setPosition:p];

	// Only try to fire when inside the world bound.
	// (enemies usually start a bit off in the right side of the screen)
	if (p.x <= WorldBounds.size.width)
	{
		const int n = rand() % 2; // First and secondary weapon only
		[self.owner fireWeapon:(WeaponId)n];
	}

	// Tag owner for removal if outside the screen.
	if (p.x < -([[self.owner getRenderEntity] boundingBox].size.width + 20))
	{
		[self.owner setAlive:NO];
	}
}

- (EntityAI *) clone
{
	AIBoss * ai = [AIBoss new];

	ai.owner = self.owner;
	ai.type  = self.type;

	ai->waveScale  = waveScale;
	ai->waveLength = waveLength;
	ai->sineParam  = sineParam;
	ai->xDir       = xDir;

	return ai;
}

@end

// ======================================================
// AIHomingMissile implementation:
// ======================================================

// Define this to 1 or 0 to visualize the curve control points.
#ifndef DEBUG_DRAW_HMISSILE_CURVE_CTRL_PTS
	#define DEBUG_DRAW_HMISSILE_CURVE_CTRL_PTS 0
#endif // DEBUG_DRAW_HMISSILE_CURVE_CTRL_PTS

// How long the missile takes to hit the target.
// Smaller values = faster missile.
#define MISSILE_TIME_TO_TARGET 5

// Midpoint of the curve the missile follows to target.
// Start point is the ship that fired, endpoint is the target.
// Adjust this to tweak the curve's visual.
#define CURVE_MIDPOINT 100

// For how long the missile flies a strait line before
// it starts seeking the target. This is for aesthetics only.
// This value has to change together with MISSILE_TIME_TO_TARGET to look good.
#define TIME_TO_START_SEEKING 2

@implementation AIHomingMissile
{
	//
    // Private data:
	//
	#if DEBUG_DRAW_HMISSILE_CURVE_CTRL_PTS
	CCDrawNode * debugDraw;
	#endif // DEBUG_DRAW_HMISSILE_CURVE_CTRL_PTS
	GameEntity * target;
	double       timeCount;
}

//
// Methods:
//

- (id) initWithType: (AIType) tp
{
	OSSG_ASSERT(tp == AI_HOMING_MISSILE);

	self = [super initWithType:tp];
	if (!self)
	{
		return nil;
	}

	// Init instance variables:
	#if DEBUG_DRAW_HMISSILE_CURVE_CTRL_PTS
	debugDraw = nil;
	#endif // DEBUG_DRAW_HMISSILE_CURVE_CTRL_PTS
	target    = nil;
	timeCount = 0;

	return self;
}

- (EntityAI *) clone
{
	AIHomingMissile * ai = [AIHomingMissile new];

	ai.owner = self.owner;
	ai.type  = self.type;

	#if DEBUG_DRAW_HMISSILE_CURVE_CTRL_PTS
	ai->debugDraw = debugDraw;
	#endif // DEBUG_DRAW_HMISSILE_CURVE_CTRL_PTS
	ai->target    = target;
	ai->timeCount = timeCount;

	return ai;
}

#if DEBUG_DRAW_HMISSILE_CURVE_CTRL_PTS
- (void) dealloc
{
    if (debugDraw != nil)
	{
		[Stage removeChild:debugDraw cleanup:YES];
		debugDraw = nil;
	}
}
#endif // DEBUG_DRAW_HMISSILE_CURVE_CTRL_PTS

- (BOOL) acquireTarget
{
	if ((target == nil) || (self.owner == nil))
	{
		return NO;
	}

	const CGPoint freeFireVector = [self.owner getFreeFireVector];
	const CGPoint currentPos = [self.owner getPosition];
	const CGPoint targetPos  = [target getPosition];
	CGPoint mid;

	mid.x = currentPos.x + (freeFireVector.x * CURVE_MIDPOINT);
	mid.y = currentPos.y + (freeFireVector.y * CURVE_MIDPOINT);

	// Curve that the missile will follow to its target:
	CCPointArray * pts = [CCPointArray arrayWithCapacity:3];
	[pts addControlPoint:currentPos];
	[pts addControlPoint:mid];
	[pts addControlPoint:targetPos];
	CCActionCatmullRomTo * action = [CCActionCatmullRomTo actionWithDuration:MISSILE_TIME_TO_TARGET points:pts];

	// DEBUG DRAW CURVE CONTROL POINTS:
	#if DEBUG_DRAW_HMISSILE_CURVE_CTRL_PTS
	debugDraw = [CCDrawNode new];
	[Stage addChild:debugDraw z:ESORT_TOPMOST + 1]; // A high Z ensures the dots are always on top
	[debugDraw drawDot:currentPos radius:5 color:[CCColor redColor]];
	[debugDraw drawDot:mid        radius:5 color:[CCColor blueColor]];
	[debugDraw drawDot:targetPos  radius:5 color:[CCColor greenColor]];
	#endif // DEBUG_DRAW_HMISSILE_CURVE_CTRL_PTS

	// Fire action:
	[[self.owner getRenderEntity] runAction:action];
	return YES;
}

- (void) think
{
	// TODO: handle the case when the target is lost while
	// in flight / target was never acquired.

	if (target != nil)
	{
		if (timeCount >= TIME_TO_START_SEEKING)
		{
			[self.owner moveToPosition:[target getPosition] setTargetAngle:YES];
		}
		else
		{
			timeCount += Time.deltaTime;
		}
	}
}

@end
