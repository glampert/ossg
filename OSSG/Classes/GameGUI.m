//
//  GameGUI.m
//  OSSG
//
//  Created by Guilherme R. Lampert on 22/04/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "GameGUI.h"
#import "GameGUI_Menus.h"

/*
 * A note for the braves editing this file in the future (this includes me!):
 * There is a LOT of hardcoded constants used for UI positioning in this file.
 * It is certainly not portable and would most likely screw every position up
 * if we were to switch to smaller size screens.
 * iOS and Cocos helps us out by using points instead of pixels, but still
 * hadcoded constants are a pretty bad solution...
 *
 * FIXME: Find better ways to handle UI element positioning.
 */

// ======================================================
// Constants / macros:
// ======================================================

// UI font face names & colors:
#define UI_FONT_FILENAME          @"coalition.ttf"
#define UI_FONT_FAMILY            @"Coalition"
#define UI_TEXT_COLOR             [CCColor colorWithRed:0.7f green:0.7f blue:0.7f]
#define UI_TEXT_COLOR_HIGHLIGHTED [CCColor whiteColor]

// SBar sprite scales:
#define SBAR_HEALTH_BAR_SCALE_X 8.0f
#define SBAR_HEALTH_BAR_SCALE_Y 0.4f
#define SBAR_ICON_SCALE         0.4f

// Small offset for buttons close to screen borders.
#define SCREEN_BORDER_BUTTON_OFFSET 5

// Virtual size of every menu screen:
#define MENU_SCREEN_WIDTH  1024
#define MENU_SCREEN_HEIGHT 768

// Offsets for the sides
#define MENU_BORDER_OFFSET_X 200
#define MENU_BORDER_OFFSET_Y 150

// Scale of button sprites:
#define MENU_BUTTON_SCALE_X 2.3f
#define MENU_BUTTON_SCALE_Y 1.8f

// Default CCTransition used by all menus.
#define DEFAULT_MENU_TRANSITION(direction)\
	[CCTransition transitionPushWithDirection:direction duration:1]

// Validates a sender passed to a block as an 'id'.
#define VALIDATE_CTRL_SENDER(sender)\
{\
	OSSG_ASSERT((sender) != nil);\
	OSSG_ASSERT([(sender) isKindOfClass:[CCControl class]]);\
	CCControl * __ctrl = (CCControl *)(sender);\
	OSSG_ASSERT(__ctrl.scene != nil);\
	OSSG_ASSERT([__ctrl.scene isKindOfClass:[MenuScreen class]]);\
}

// Cast an 'id' to a MenuScreen:
#define SENDER_GET_MENU(sender) ((MenuScreen *)((CCControl *)(sender)).scene)

// ======================================================
// LoadSprite(): Local helper to load CCSprites from named files.
// ======================================================

static CCSprite * LoadSprite(NSString * spriteName)
{
	OSSG_ASSERT(spriteName != nil);

	// Append default extension if not present:
	NSString * fileExt = [spriteName pathExtension];
	if ([fileExt caseInsensitiveCompare:STD_SPRITE_SHEET_FILE_EXT] != NSOrderedSame)
	{
		spriteName = [spriteName stringByAppendingFormat:@".%@", STD_SPRITE_SHEET_FILE_EXT];
	}

	// Load a sprite with extra error checking:
	if (!ResourceExists(spriteName))
	{
		OSSG_ERROR(@"File '%@' is not present / does not name a file!", spriteName);
		return [CCSprite emptySprite]; // Safe default
	}

	CCSprite * sprite = [CCSprite spriteWithImageNamed:spriteName];
	if (!sprite)
	{
		OSSG_ERROR(@"Failed to load graphics from file '%@'!", spriteName);
		return [CCSprite emptySprite]; // Safe default
	}

	return sprite;
}

// ======================================================
// PopupMenuFSOverlay local helper class:
// ======================================================

@interface PopupMenuFSOverlay : CCNode
- (void) draw;
@end

@implementation PopupMenuFSOverlay
- (void) draw
{
	// This method only actually draws the full-screen
	// overlay that comes with the pause/end-game popup menus.
	// The menu buttons and panel are drawn with CCSprites.
	[GLProgMgr enableProgram:GLPROG_FADE_SCREEN];
	DrawFullScreenQuad();
	[GLProgMgr restore];
}
@end

// ======================================================
// CustomControl implementation:
// ======================================================

@implementation CustomControl

//
// Methods:
//

- (id) initWithParams: (NSString *)title frontName: (NSString *)fName fontSize: (float) fSize spriteName: (NSString *)sName
{
	self = [super init];
	if (!self)
	{
		return nil;
	}

	_onToggle              = nil;
	_tooltip               = nil;
	_buttonIcon            = nil;
	_button                = [CCButton buttonWithTitle:title fontName:fName fontSize:fSize];
	_backgroundNormal      = LoadSprite(sName);
	_backgroundHighlighted = LoadSprite([NSString stringWithFormat:@"%@_highlighted", sName]);
	_isSwitchButton        = NO;
	_switchButtonState     = NO;
	_hidden                = NO;
	_alwaysShowIcon        = NO;

	[self resetStates];
	return self;
}

- (id) initWithBgOnly: (NSString *)spriteName
{
	self = [super init];
	if (!self)
	{
		return nil;
	}

	_onToggle              = nil;
	_tooltip               = nil;
	_buttonIcon            = nil;
	_button                = [CCButton buttonWithTitle:@"" fontName:UI_FONT_FILENAME fontSize:20]; // No text
	_backgroundNormal      = LoadSprite(spriteName);
	_backgroundHighlighted = LoadSprite([NSString stringWithFormat:@"%@_highlighted", spriteName]);
	_isSwitchButton        = NO;
	_switchButtonState     = NO;
	_hidden                = NO;
	_alwaysShowIcon        = NO;

	[self resetStates];
	return self;
}

+ (CustomControl *) createButton: (NSString *)title frontName: (NSString *)fName fontSize: (float) fSize spriteName: (NSString *)sName
{
	return [[CustomControl alloc] initWithParams:title frontName:fName fontSize:fSize spriteName:sName];
}

+ (CustomControl *) createButtonWithBgOnly: (NSString *)spriteName scaleX: (float)sx scaleY: (float)sy
{
	CustomControl * ctrl = [[CustomControl alloc] initWithBgOnly:spriteName];
	[ctrl setBackgroundScaleX:sx andY:sy];
	return ctrl;
}

- (void) resetStates
{
	if (_hidden)
	{
		return;
	}

	_button.state                  = CCControlStateNormal;
	_button.preferredSize          = [_backgroundNormal boundingBox].size;
	_button.enabled                = YES;
	_button.zoomWhenHighlighted    = NO;
	_button.color                  = UI_TEXT_COLOR;
	_buttonIcon.color              = UI_TEXT_COLOR;
	_buttonIcon.visible            = (_isSwitchButton && !_alwaysShowIcon) ? _switchButtonState : YES;
	_backgroundNormal.visible      = YES;
	_backgroundHighlighted.visible = NO;
}

- (void) setHidden: (BOOL) state
{
	_hidden = state;

	if (_hidden)
	{
		_button.enabled                = NO;
		_button.visible                = NO;
		_backgroundNormal.visible      = NO;
		_backgroundHighlighted.visible = NO;
		_buttonIcon.visible            = NO;
		_tooltip.visible               = NO;
	}
	else
	{
		_button.enabled                = YES;
		_button.visible                = YES;
		_backgroundNormal.visible      = YES;
		_backgroundHighlighted.visible = NO;
		_buttonIcon.visible            = (_isSwitchButton && !_alwaysShowIcon) ? _switchButtonState : YES;
		_tooltip.visible               = YES;
	}
}

- (void) addButtonIcon: (NSString *)iconSprite scaleX: (float)sx scaleY: (float)sy
{
	_buttonIcon = LoadSprite(iconSprite);
	_buttonIcon.scaleX   = sx;
	_buttonIcon.scaleY   = sy;
	_buttonIcon.position = _backgroundNormal.position;
	_buttonIcon.color    = UI_TEXT_COLOR;
}

- (void) addTooltip: (NSString *) text fontSize: (float) size
{
	_tooltip = [CCLabelTTF labelWithString:text fontName:UI_FONT_FILENAME fontSize:size];
	_tooltip.color = UI_TEXT_COLOR;

	_tooltip.position = ccp(
		_backgroundNormal.position.x + ([_backgroundNormal boundingBox].size.width / 2) + ([_tooltip boundingBox].size.width / 2) + 7,
		_backgroundNormal.position.y);
}

- (BOOL) pointIntersects: (CGPoint) pt
{
	return CGRectContainsPoint([_backgroundNormal boundingBox], pt);
}

- (CGRect) getBoundingBox
{
	return [_backgroundNormal boundingBox];
}

- (void) setIsSwitchButton: (BOOL) yesNo
{
	_isSwitchButton = yesNo;

	// Switch buttons have a custom block:
	if (_isSwitchButton)
	{
		__block CustomControl * me = self;
		_button.block = ^void(id sender) {
			VALIDATE_CTRL_SENDER(sender);
			[SoundSys playSoundAsync:SND_BTN_CLICK];
			me.switchButtonState = !me.switchButtonState;
			if (me.onToggle)
			{
				me.onToggle(me, me.switchButtonState);
			}
		};
	}
}

- (void) frameUpdate
{
	if (_hidden)
	{
		return;
	}

	if (_button.highlighted || _button.selected || (_switchButtonState == YES))
	{
		_backgroundNormal.visible      = NO;
		_backgroundHighlighted.visible = YES;
		_buttonIcon.color              = UI_TEXT_COLOR_HIGHLIGHTED;
		_button.color                  = UI_TEXT_COLOR_HIGHLIGHTED;

		if (_isSwitchButton || _alwaysShowIcon)
		{
			_buttonIcon.visible = YES;
		}
	}
	else
	{
		_backgroundNormal.visible      = YES;
		_backgroundHighlighted.visible = NO;
		_buttonIcon.color              = UI_TEXT_COLOR;
		_button.color                  = UI_TEXT_COLOR;

		if (_isSwitchButton && !_alwaysShowIcon)
		{
			_buttonIcon.visible = NO;
		}
	}
}

- (void) setPosition: (CGPoint) pos
{
	_button.position                = pos;
	_backgroundNormal.position      = pos;
	_backgroundHighlighted.position = pos;
	_buttonIcon.position            = _backgroundNormal.position;

	_tooltip.position = ccp(
		_backgroundNormal.position.x + ([_backgroundNormal boundingBox].size.width / 2) + ([_tooltip boundingBox].size.width / 2) + 7,
		_backgroundNormal.position.y);
}

- (void) setBackgroundScaleX: (float) sx andY: (float) sy
{
	_backgroundNormal.scaleX = sx;
	_backgroundNormal.scaleY = sy;

	_backgroundHighlighted.scaleX = sx;
	_backgroundHighlighted.scaleY = sy;

	_button.preferredSize = [_backgroundNormal boundingBox].size;
}

- (void) setButtonIconScaleX: (float) sx andY: (float) sy
{
	if (_buttonIcon)
	{
		_buttonIcon.scaleX = sx;
		_buttonIcon.scaleY = sy;
	}
}

- (void) setTooltipScaleX: (float) sx andY: (float) sy
{
	if (_tooltip)
	{
		_tooltip.scaleX = sx;
		_tooltip.scaleY = sy;
	}
}

- (void) setTextOffsetX: (float) x andY: (float) y
{
	_button.position = ccp(_button.position.x + x, _button.position.y + y);
}

- (void) setTextColor: (CCColor *) c
{
	_button.color = c;
}

- (void) setTooltipText: (NSString *) text
{
	if (_tooltip)
	{
		_tooltip.string = text;
	}
}

- (void) linkToScene: (CCScene *) gs
{
	OSSG_ASSERT(gs != nil);

	[gs addChild:_backgroundNormal      z:ESORT_POPUP_MENU_BUTTON];
	[gs addChild:_backgroundHighlighted z:ESORT_POPUP_MENU_BUTTON];
	[gs addChild:_button                z:ESORT_POPUP_MENU_BUTTON + 1];

	if (_buttonIcon)
	{
		[gs addChild:_buttonIcon z:ESORT_POPUP_MENU_BUTTON + 1];
	}
	if (_tooltip)
	{
		[gs addChild:_tooltip z:ESORT_POPUP_MENU_BUTTON + 1];
	}
}

- (void) removeFromScene: (CCScene *) gs
{
	OSSG_ASSERT(gs != nil);

	if (_tooltip)
	{
		[gs removeChild:_tooltip cleanup:YES];
	}
	if (_buttonIcon)
	{
		[gs removeChild:_buttonIcon cleanup:YES];
	}

	[gs removeChild:_button                cleanup:YES];
	[gs removeChild:_backgroundHighlighted cleanup:YES];
	[gs removeChild:_backgroundNormal      cleanup:YES];

	[self resetStates];
}

@end

// ======================================================
// CustomTextField implementation:
// ======================================================

#define TEXT_FIELD_FONT_SIZE 18
#define TEXT_FIELD_SIZE CGSizeMake(270, 60)

@implementation CustomTextField

+ (CustomTextField *) createTextFieldWithInitialText: (NSString *)text
{
	CCSpriteFrame * textFieldBg = [CCSpriteFrame frameWithImageNamed:@"common_button_highlighted." STD_SPRITE_SHEET_FILE_EXT];
	CustomTextField * textField = [CustomTextField textFieldWithSpriteFrame:textFieldBg];

	// Position / set size:
	textField.preferredSize       = TEXT_FIELD_SIZE;
	textField.position            = ccp(0,0);
	textField.padding             = 20;
	textField.fontSize            = TEXT_FIELD_FONT_SIZE;
	textField.color               = UI_TEXT_COLOR;
	textField.textField.textColor = UI_TEXT_COLOR.UIColor;
	textField.textField.font      = [UIFont fontWithName:UI_FONT_FAMILY size:TEXT_FIELD_FONT_SIZE];
	textField.string              = text;

	return textField;
}

@end

// ======================================================
// ListMenu implementation:
// ======================================================

@implementation ListMenu
{
	// Each entry is actually a button control.
	CustomControl * entries[MAX_LIST_MENU_ENTRIES];
}

//
// Methods:
//

- (id) init
{
	self = [super init];
	if (!self)
	{
		return nil;
	}

	_background = LoadSprite(@"popup_menu_bg");

	// Sprite image is assumed to need a 90 degree flip.
	// (This is the same image used by the in-game pause menu,
	// which is rendered in a different orientation, the default).
	_background.rotation = 90;

	// Add the four buttons, but make then hidden by default.
	for (int i = 0; i < MAX_LIST_MENU_ENTRIES; ++i)
	{
		entries[i] = [CustomControl createButtonWithBgOnly:@"common_button"
													scaleX:(MENU_BUTTON_SCALE_X + 0.4f)
													scaleY:(MENU_BUTTON_SCALE_Y - 0.2f)];
		entries[i].isSwitchButton = YES;
		entries[i].hidden = YES; // Not visible by default
	}

	return self;
}

- (void) frameUpdate
{
	// Forward the update to each individual control:
	for (int i = 0; i < MAX_LIST_MENU_ENTRIES; ++i)
	{
		[entries[i] frameUpdate];
	}
}

- (CGRect) getBoundingBox
{
	return [_background boundingBox];
}

- (void) setPosition: (CGPoint) pos
{
	_background.position = pos;

	for (int i = 0; i < MAX_LIST_MENU_ENTRIES; ++i)
	{
		CustomControl * ctrl = entries[i];
		[ctrl setPosition:ccp(pos.x,
			(pos.y + ([_background boundingBox].size.height / 2) - 90) -
				(([ctrl getBoundingBox].size.height + 20) * i))];
	}
}

- (void) setBackgroundScaleX: (float) sx andY: (float) sy
{
	// NOTE: Since the image is flipped, we swizzle x and y when scaling.
	_background.scaleX = sy;
	_background.scaleY = sx;
}

- (void) resetAllControls
{
	for (int i = 0; i < MAX_LIST_MENU_ENTRIES; ++i)
	{
		[entries[i] resetStates];
		entries[i].switchButtonState = NO;
	}
}

- (void) linkToScene: (CCScene *) gs
{
	OSSG_ASSERT(gs != nil);
	[gs addChild:_background z:ESORT_POPUP_MENU];

	// Link controls to the scene:
	for (int i = 0; i < MAX_LIST_MENU_ENTRIES; ++i)
	{
		[entries[i] linkToScene:gs];
	}
}

- (void) removeFromScene: (CCScene *) gs
{
	OSSG_ASSERT(gs != nil);
	[gs removeChild:_background cleanup:YES];

	// Remove controls from scene:
	for (int i = 0; i < MAX_LIST_MENU_ENTRIES; ++i)
	{
		[entries[i] removeFromScene:gs];
	}

	[self resetAllControls];
}

- (CustomControl *) getEntry: (int) index
{
	OSSG_ASSERT(index >= 0 && index < MAX_LIST_MENU_ENTRIES);
	return entries[index];
}

- (int) getEntryIndex: (const CustomControl *) entry
{
	for (int i = 0; i < MAX_LIST_MENU_ENTRIES; ++i)
	{
		if (entries[i] == entry)
		{
			return i;
		}
	}
	return -1;
}

- (CustomControl *) getFirstSelectedEntry
{
	for (int i = 0; i < MAX_LIST_MENU_ENTRIES; ++i)
	{
		if (entries[i].switchButtonState == YES)
		{
			return entries[i];
		}
	}
	return nil;
}

- (void) setEntry: (int) index withText: (NSString *) text andCallback: (void(^)(CustomControl *, BOOL)) onToggle
{
	OSSG_ASSERT(index >= 0 && index < MAX_LIST_MENU_ENTRIES);

	// Set text and make visible:
	CustomControl * ctrl = entries[index];
	ctrl.hidden       = NO;
	ctrl.onToggle     = onToggle;
	ctrl.button.title = [ListMenu truncateTextToFitListEntry:text];
}

+ (NSString *) truncateTextToFitListEntry: (NSString *) text
{
	// Truncate text if too long:
	NSRange stringRange = { 0, MIN([text length], MAX_LIST_MENU_ENTRY_TEXT_LEN) };

	// Adjust the range to include dependent (unicode) chars:
	stringRange = [text rangeOfComposedCharacterSequencesForRange:stringRange];

	// Truncate:
	return [text substringWithRange:stringRange];
}

@end

// ======================================================
// MenuScreen implementation:
// ======================================================

@implementation MenuScreen
{
	CCSprite       * background;
	ListMenu       * listMenu; // Optional list menu
	NSMutableArray * controls; // Array of CustomControl
	float            prevNextButtonsX;

	// Labels:
	CCLabelTTF * topSideLabel;
	CCLabelTTF * rightSideLabel;

	// Previous and next menus. Null if no predecessor/successors.
	// Weak references to avoid a retain cycle.
	__unsafe_unretained MenuScreen * prevMenu;
	__unsafe_unretained MenuScreen * nextMenu[MAX_NEXT_MENUS];
}

//
// Methods:
//

- (id) init
{
	self = [super init];
	if (!self)
	{
		return nil;
	}

	// Enable touch handling on this scene node:
	self.userInteractionEnabled = YES;
	self.multipleTouchEnabled   = YES;

	// Init instance vars:
	_onNext          = nil;
	_onPrev          = nil;
	_onMenuEnter     = nil;
	_onMenuExit      = nil;
	background       = nil;
	listMenu         = nil;
	controls         = [NSMutableArray array];
	prevNextButtonsX = 0;
	topSideLabel     = nil;
	rightSideLabel   = nil;

	prevMenu = nil;
	for (int i = 0; i < MAX_NEXT_MENUS; ++i)
	{
		nextMenu[i] = nil;
	}

	// Always load the background:
	background = LoadSprite(@"big_menu_bg");
	background.anchorPoint = ccp(0,0); // Origin: Lower left corner of image (same as OpenGL)
	background.position    = ccp(0,0);
	[self addChild:background z:ESORT_POPUP_MENU];

	return self;
}

- (void) onEnter // Overwrites CCNode
{
	[super onEnter];

	if (_onMenuEnter)
	{
		_onMenuEnter(self);
	}
}

- (void) onExit // Overwrites CCNode
{
	[super onExit];

	if (_onMenuExit)
	{
		_onMenuExit(self);
	}
}

- (void) update: (CCTime) deltaTime // Overwrites CCNode
{
	// Update global timer:
	Time.deltaTime   = deltaTime;
	Time.currentTime = AbsoluteTimeSeconds();

	// Update the buttons / controls:
	for (unsigned int i = 0; i < [controls count]; ++i)
	{
		CustomControl * ctrl = [controls objectAtIndex:i];
		OSSG_ASSERT(ctrl != nil);
		[ctrl frameUpdate];
	}

	[listMenu frameUpdate];
}

- (void) setPrevMenu: (MenuScreen *) prev
{
	if (prev != nil)
	{
		CustomControl * btn = [self newButton:@"<<<" smallButton:YES];
		prevNextButtonsX += ([MenuScreen borderX] + ([btn getBoundingBox].size.width / 2) - 50);
		[btn setPosition:ccp(prevNextButtonsX, [MenuScreen borderY])];

		[self setPrevMenu:prev withButton:btn];
	}
}

- (void) setNextMenu: (MenuScreen *) next
{
	if (next != nil)
	{
		CustomControl * btn = [self newButton:@">>>" smallButton:YES];
		prevNextButtonsX += ([MenuScreen borderX] + ([btn getBoundingBox].size.width / 2) - 50);
		[btn setPosition:ccp(prevNextButtonsX, [MenuScreen borderY])];

		// With the default next/prev buttons nextMenu[0] uses the default button.
		[self setNextMenu:next index:0 withButton:btn];
	}
}

- (void) setPrevMenu: (MenuScreen *) prev withButton: (CustomControl *) btn
{
	prevMenu = prev;

	if (prevMenu)
	{
		OSSG_ASSERT(btn != nil);
		btn.button.block = ^void(id sender) {
			VALIDATE_CTRL_SENDER(sender);
			[SoundSys playSoundAsync:SND_BTN_CLICK];
			// Return to previous menu:
			MenuScreen * menu = SENDER_GET_MENU(sender);
			[menu swithToPrevMenu];
		};
	}
}

- (void) setNextMenu: (MenuScreen *) next index: (int) i withButton: (CustomControl *) btn
{
	OSSG_ASSERT(i >= 0 && i < MAX_NEXT_MENUS);
	nextMenu[i] = next;

	if (nextMenu[i])
	{
		OSSG_ASSERT(btn != nil);
		btn.button.block = ^void(id sender) {
			VALIDATE_CTRL_SENDER(sender);
			[SoundSys playSoundAsync:SND_BTN_CLICK];
			// Go to next menu:
			MenuScreen * menu = SENDER_GET_MENU(sender);
			[menu swithToNextMenu:i];
		};
	}
}

- (BOOL) swithToPrevMenu
{
	if (!prevMenu)
	{
		return NO;
	}

	OSSG_LOG(@"Switching back to previows menu...");

	if (_onPrev)
	{
		const BOOL shouldSwitch = _onPrev(self);
		if (!shouldSwitch)
		{
			OSSG_LOG(@"Block canceled menu switch...");
			return NO;
		}
	}

	[prevMenu resetAllControls];
	[[CCDirector sharedDirector] replaceScene:prevMenu withTransition:DEFAULT_MENU_TRANSITION(CCTransitionDirectionRight)];

	[SoundSys playSoundAsync:SND_MENU_CHANGE];
	return YES;
}

- (BOOL) swithToNextMenu: (int) index
{
	OSSG_ASSERT(index >= 0 && index < MAX_NEXT_MENUS);

	if (!nextMenu[index])
	{
		return NO;
	}

	OSSG_LOG(@"Switching to next menu %d...", index);

	if (_onNext)
	{
		const BOOL shouldSwitch = _onNext(self, index);
		if (!shouldSwitch)
		{
			OSSG_LOG(@"Block canceled menu switch...");
			return NO;
		}
	}

	[nextMenu[index] resetAllControls];
	[[CCDirector sharedDirector] replaceScene:nextMenu[index] withTransition:DEFAULT_MENU_TRANSITION(CCTransitionDirectionLeft)];

	[SoundSys playSoundAsync:SND_MENU_CHANGE];
	return YES;
}

- (void) resetAllControls
{
	for (unsigned int i = 0; i < [controls count]; ++i)
	{
		CustomControl * ctrl = [controls objectAtIndex:i];
		OSSG_ASSERT(ctrl != nil);
		[ctrl resetStates];
	}

	[listMenu resetAllControls];
}

- (void) setRightSideLabel: (NSString *) text fontSize: (float) size
{
	[self setRightSideLabel:text fontSize:size position:ccp(
		[MenuScreen width]  - [MenuScreen borderX] - 20,
		[MenuScreen height] - 380)];
}

- (void) setRightSideLabel: (NSString *) text fontSize: (float) size position: (CGPoint) pos
{
	// This little trick will draw the text vertically (each char in a line).
	// E.g.: "OSSG" would be drawn:
	//
	// O
	// S
	// S
	// G
	//
	NSMutableString * str = [NSMutableString string];
	for (unsigned int i = 0; i < [text length]; ++i)
	{
		[str appendFormat:@"%c\n", [text characterAtIndex:i]];
	}

	rightSideLabel = [CCLabelTTF labelWithString:str fontName:UI_FONT_FILENAME fontSize:size];
	rightSideLabel.position = pos;
	rightSideLabel.color    = UI_TEXT_COLOR;

	[self addChild:rightSideLabel z:ESORT_POPUP_MENU_BUTTON];
}

- (CCLabelTTF *) getRightSideLabel
{
	return rightSideLabel;
}

- (void) setTopSideLabel: (NSString *) text fontSize: (float) size
{
	[self setTopSideLabel:text fontSize:size position:ccp(
		[MenuScreen width] / 2, [MenuScreen height] - [MenuScreen borderY] + 50)];
}

- (void) setTopSideLabel: (NSString *) text fontSize: (float) size position: (CGPoint) pos
{
	topSideLabel = [CCLabelTTF labelWithString:text fontName:UI_FONT_FILENAME fontSize:size];
	topSideLabel.position = pos;
	topSideLabel.color    = UI_TEXT_COLOR;

	[self addChild:topSideLabel z:ESORT_POPUP_MENU_BUTTON];
}

- (CCLabelTTF *) getTopSideLabel
{
	return topSideLabel;
}

- (CustomControl *) newButton: (NSString *) title smallButton: (BOOL) small
{
	CustomControl * btn = [CustomControl createButton:title frontName:UI_FONT_FILENAME fontSize:20 spriteName:@"common_button"];

	if (small)
	{
		[btn setBackgroundScaleX:(MENU_BUTTON_SCALE_X * 0.7f) andY:(MENU_BUTTON_SCALE_Y * 0.7f)];
	}
	else
	{
		[btn setBackgroundScaleX:MENU_BUTTON_SCALE_X andY:MENU_BUTTON_SCALE_Y];
	}

	[btn linkToScene:self];
	[controls addObject:btn];
	return btn;
}

- (CustomControl *) newSwitchButton: (NSString *) tooltip;
{
	return [self newSwitchButton:tooltip withIcon:@"icon_check"];
}

- (CustomControl *) newSwitchButton: (NSString *) tooltip withIcon: (NSString *) iconName
{
	CustomControl * btn = [CustomControl createButtonWithBgOnly:@"big_button" scaleX:0.7f scaleY:0.7f];
	[btn addButtonIcon:iconName scaleX:1.0f scaleY:1.0f];
	btn.isSwitchButton = YES;

	if (tooltip)
	{
		[btn addTooltip:tooltip fontSize:20];
	}

	[btn linkToScene:self];
	[controls addObject:btn];
	return btn;
}

- (ListMenu *) newListMenu
{
	// NOTE: Only one list menu per menu screen!
	listMenu = [ListMenu new];
	[listMenu setBackgroundScaleX:1.7f andY:0.8f];

	// Position to the left side of the screen:
	const float x = ([MenuScreen borderX] + ([listMenu getBoundingBox].size.width  / 2) - 75);
	const float y = ([MenuScreen height]  - ([listMenu getBoundingBox].size.height / 2) - 90);
	[listMenu setPosition:ccp(x, y)];

	[listMenu linkToScene:self];
	return listMenu;
}

- (CustomTextField *) newTextFieldAt: (CGPoint) pos withText: (NSString *) text andTitle: (NSString *) title
{
	CustomTextField * textField = [CustomTextField createTextFieldWithInitialText:text];

	if (title)
	{
		CCLabelTTF * textFieldTitle = [CCLabelTTF labelWithString:title fontName:UI_FONT_FILENAME fontSize:24];
		textFieldTitle.position = ccp(pos.x + ([textField boundingBox].size.width / 2) - 10, pos.y);
		textFieldTitle.color = UI_TEXT_COLOR;
		[self addChild:textFieldTitle z:ESORT_POPUP_MENU_BUTTON];

		textField.position = ccp(pos.x, pos.y - 100);
	}
	else
	{
		textField.position = pos;
	}

	[self addChild:textField z:ESORT_POPUP_MENU_BUTTON];

	return textField;
}

+ (float) width
{
	return MENU_SCREEN_WIDTH;
}

+ (float) height
{
	return MENU_SCREEN_HEIGHT;
}

+ (float) borderX
{
	return MENU_BORDER_OFFSET_X;
}

+ (float) borderY
{
	return MENU_BORDER_OFFSET_Y;
}

@end

// ======================================================
// GameGUI implementation:
// ======================================================

@implementation GameGUI
{
	// SBar sprites:
	CCSprite           * batteryIcon;
	CCSprite           * missileIcon;
	CCSprite           * healthBarFrame;
	CCSprite           * healthBar;

	// Popup menus buttons & labels:
	CustomControl      * pauseMenuEndStageBtn;
	CustomControl      * pauseMenuContinueBtn;
	CustomControl      * endGameMenuContinueBtn;
	CCLabelTTF         * pauseMenuLabel;
	CCLabelTTF         * endGameMenuLabel;
	BOOL                 pauseMenuOpen;
	BOOL                 endGameMenuOpen;
	CCSprite           * popupMenuPanel;
	PopupMenuFSOverlay * fadeScreenOverlay;

	// CCLabelTTF list:
	NSMutableArray     * uiLabels;
}

// Keep track of accidental multiple initializations.
static BOOL gameGUICreated = NO;

// SBar states. We avoid refreshing text labels
// as much as possible. CCLabelTTF is not very efficient at it.
static struct {
	int playerHealth;
	int playerCredits;
	int stageNum;
	int numMissiles;
	int numBatteries;
} sbarState;

// Set all sbarState fields to invalid values
// so that they get refreshed in the next update.
#define SBAR_RESET()\
{\
	sbarState.playerHealth  = -1;\
	sbarState.playerCredits = -1;\
	sbarState.stageNum      = -1;\
	sbarState.numMissiles   = -1;\
	sbarState.numBatteries  = -1;\
}

//
// Methods:
//

- (void) initInGameButtons
{
	// Create in-game buttons:
	_mainWeapBtn    = [CustomControl createButtonWithBgOnly:@"big_button" scaleX:1.0f scaleY:1.0f];
	_secondWeapBtn1 = [CustomControl createButtonWithBgOnly:@"big_button" scaleX:0.6f scaleY:0.8f];
	_secondWeapBtn2 = [CustomControl createButtonWithBgOnly:@"big_button" scaleX:0.6f scaleY:0.8f];
	_pauseBtn       = [CustomControl createButtonWithBgOnly:@"big_button" scaleX:0.5f scaleY:0.5f];

	// Also add decorative icons to the buttons:
	[_mainWeapBtn    addButtonIcon:@"icon_blast"   scaleX:1.1f scaleY:1.1f];
	[_secondWeapBtn1 addButtonIcon:@"icon_missile" scaleX:0.6f scaleY:0.6f];
	[_secondWeapBtn2 addButtonIcon:@"icon_battery" scaleX:0.6f scaleY:0.6f];
	[_pauseBtn       addButtonIcon:@"icon_pause"   scaleX:0.6f scaleY:0.6f];

	// Size of the whole window/screen:
	const CGSize winSize = [CCDirector sharedDirector].viewSize;

	// In-game UI buttons:
	{
		CGRect mainBox, btnBox;

		// Main button:
		mainBox = [_mainWeapBtn getBoundingBox];
		[_mainWeapBtn setPosition:ccp(
			(winSize.width - (mainBox.size.width / 2) - SCREEN_BORDER_BUTTON_OFFSET),
			(mainBox.size.height / 2) + SCREEN_BORDER_BUTTON_OFFSET)];

		// 1st secondary weapon:
		btnBox = [_secondWeapBtn1 getBoundingBox];
		[_secondWeapBtn1 setPosition:ccp(
			(winSize.width - mainBox.size.width - 80),
			(btnBox.size.height / 2) + SCREEN_BORDER_BUTTON_OFFSET)];

		// 2nd secondary weapon:
		btnBox = [_secondWeapBtn2 getBoundingBox];
		[_secondWeapBtn2 setPosition:ccp(
			(winSize.width - mainBox.size.width - 200),
			(btnBox.size.height / 2) + SCREEN_BORDER_BUTTON_OFFSET)];

		// Pause/home button:
		btnBox = [_pauseBtn getBoundingBox];
		[_pauseBtn setPosition:ccp(
			(btnBox.size.width / 2) + SCREEN_BORDER_BUTTON_OFFSET,
			(winSize.height - (btnBox.size.height / 2) - SCREEN_BORDER_BUTTON_OFFSET))];
	}

	// Pause menu panel and common buttons:
	{
		// Background panel:
		popupMenuPanel = LoadSprite(@"popup_menu_bg");
		popupMenuPanel.anchorPoint = ccp(0,1);
		popupMenuPanel.scale       = 1.0f;
		popupMenuPanel.position    = ccp(
			(winSize.width  / 2) - ([popupMenuPanel boundingBox].size.width  / 2),
			(winSize.height / 2) + ([popupMenuPanel boundingBox].size.height / 2));

		// End game/stage button:
		pauseMenuEndStageBtn = [CustomControl createButton:@"End Stage" frontName:UI_FONT_FILENAME fontSize:20 spriteName:@"common_button"];
		[pauseMenuEndStageBtn setBackgroundScaleX:1.7f andY:1.3f];
		[pauseMenuEndStageBtn setPosition:ccp(
			(popupMenuPanel.position.x + [pauseMenuEndStageBtn getBoundingBox].size.width)  - 50,
			(popupMenuPanel.position.y - [pauseMenuEndStageBtn getBoundingBox].size.height) - 90)];
		[pauseMenuEndStageBtn setTextOffsetX:1.0f andY:-2.0f];

		// Resume game/stage button:
		pauseMenuContinueBtn = [CustomControl createButton:@"Resume" frontName:UI_FONT_FILENAME fontSize:20 spriteName:@"common_button"];
		[pauseMenuContinueBtn setBackgroundScaleX:1.7f andY:1.3f];
		[pauseMenuContinueBtn setPosition:ccp(
			(popupMenuPanel.position.x + [pauseMenuContinueBtn getBoundingBox].size.width)  + 220,
			(popupMenuPanel.position.y - [pauseMenuContinueBtn getBoundingBox].size.height) - 90)];
		[pauseMenuContinueBtn setTextOffsetX:1.0f andY:-2.0f];

		// End game/stage button for when the player wins/looses:
		endGameMenuContinueBtn = [CustomControl createButton:@"Continue" frontName:UI_FONT_FILENAME fontSize:20 spriteName:@"common_button"];
		[endGameMenuContinueBtn setBackgroundScaleX:1.7f andY:1.3f];
		[endGameMenuContinueBtn setPosition:ccp(
			(popupMenuPanel.position.x + [popupMenuPanel boundingBox].size.width/2),
			(popupMenuPanel.position.y - [endGameMenuContinueBtn getBoundingBox].size.height) - 110)];
		[endGameMenuContinueBtn setTextOffsetX:1.0f andY:-2.0f];
	}

	// Associate actions to the in-game buttons:
	{
		//
		// In-game popup menu buttons:
		//
		_pauseBtn.button.block = ^void(id sender) {
			if (![Gui isPauseMenuOpen] && ![Gui isEndGameMenuOpen])
			{
				[SoundSys playSoundAsync:SND_BTN_CLICK];
				[Gui showPauseMenu]; // Pause game
			}
		};

		pauseMenuContinueBtn.button.block = ^void(id sender) {
			if ([Gui isPauseMenuOpen] == YES)
			{
				[SoundSys playSoundAsync:SND_BTN_CLICK];
				[Gui hidePauseMenu]; // Resume game
			}
		};

		pauseMenuEndStageBtn.button.block = ^void(id sender) {
			OSSG_ASSERT([Gui isPauseMenuOpen] == YES);
			[SoundSys playSoundAsync:SND_BTN_CLICK];
			[Session endStage];
		};

		endGameMenuContinueBtn.button.block = ^void(id sender) {
			OSSG_ASSERT([Gui isEndGameMenuOpen] == YES);
			[SoundSys playSoundAsync:SND_BTN_CLICK];
			[Session endStage];
		};

		// This will avoid the button from taking over the touch inputs.
		_mainWeapBtn.button.exclusiveTouch    = NO;
		_secondWeapBtn1.button.exclusiveTouch = NO;
		_secondWeapBtn2.button.exclusiveTouch = NO;

		//
		// In-game action buttons:
		//
		_mainWeapBtn.button.block = ^void(id sender) {
			if (![Gui isPauseMenuOpen] && ![Gui isEndGameMenuOpen])
			{
				[Session.player fireWeapon:WEAP_MAIN];
			}
		};

		_secondWeapBtn1.button.block = ^void(id sender) {
			if (![Gui isPauseMenuOpen] && ![Gui isEndGameMenuOpen])
			{
				[Session.player fireWeapon:WEAP_SECONDARY1];
			}
		};

		_secondWeapBtn2.button.block = ^void(id sender) {
			if (![Gui isPauseMenuOpen] && ![Gui isEndGameMenuOpen])
			{
				// Second weapon button will be fixed to shield for now.
				// But it could be any weapon really...
				[Session.player activateShield];
			}
		};
	}
}

- (void) initSBar
{
	const CGSize winSize = [[CCDirector sharedDirector] viewSize];

	// Load sprites:
	batteryIcon    = LoadSprite(@"icon_battery");
	missileIcon    = LoadSprite(@"icon_missile");
	healthBarFrame = LoadSprite(@"health_bar_frame");
	healthBar      = LoadSprite(@"health_bar");

	// Make anchor points the upper left corner.
	// I prefer to work with these measures in "old school" mode :P
	batteryIcon.anchorPoint    = ccp(0,1);
	missileIcon.anchorPoint    = ccp(0,1);
	healthBarFrame.anchorPoint = ccp(0,1);
	healthBar.anchorPoint      = ccp(0,1);

	// Set sprite positions / scales:
	CGRect bbox, healthBarBox;

	healthBar.scaleX        = SBAR_HEALTH_BAR_SCALE_X;
	healthBar.scaleY        = SBAR_HEALTH_BAR_SCALE_Y;
	healthBarBox            = [healthBar boundingBox];
	healthBar.position      = ccp(20, healthBarBox.size.height + 40);
	healthBarFrame.scaleY   = 0.57f;
	healthBarFrame.scaleX   = 0.64f;
	healthBarFrame.position = ccp(healthBar.position.x, healthBar.position.y + healthBarBox.size.height);
	healthBarBox            = [healthBarFrame boundingBox];

	batteryIcon.colorRGBA   = UI_TEXT_COLOR;
	batteryIcon.scale       = SBAR_ICON_SCALE;
	bbox                    = [batteryIcon boundingBox];
	batteryIcon.position    = ccp(
		winSize.width - bbox.size.width - (healthBarBox.size.width - 20),
		(winSize.height - bbox.size.height) + 10);

	missileIcon.colorRGBA   = UI_TEXT_COLOR;
	missileIcon.scale       = SBAR_ICON_SCALE;
	bbox                    = [missileIcon boundingBox];
	missileIcon.position    = ccp(
		winSize.width - bbox.size.width - (healthBarBox.size.width - 160),
		(winSize.height - bbox.size.height) + 10);

	SBAR_RESET();

	OSSG_LOG(@"SBar successfully initialized.");
}

- (void) initTextLabels
{
	OSSG_ASSERT(uiLabels != nil);

	CGRect bbox;
	const CGSize winSize = [[CCDirector sharedDirector] viewSize];

	// UI_LBL_STAGE_NUM:
	CCLabelTTF * labelStageNum = [CCLabelTTF labelWithString:@"Stage: 0" fontName:UI_FONT_FILENAME fontSize:28];
	labelStageNum.position  = ccp((winSize.width / 2) + 20, winSize.height - 30);
	labelStageNum.colorRGBA = UI_TEXT_COLOR;
	[uiLabels addObject:labelStageNum];

	// UI_LBL_PLAYER_CREDITS:
	CCLabelTTF * labelMoney = [CCLabelTTF labelWithString:@"Credits: 0" fontName:UI_FONT_FILENAME fontSize:20];
	labelMoney.position  = ccp(labelStageNum.position.x - 320, labelStageNum.position.y);
	labelMoney.colorRGBA = UI_TEXT_COLOR;
	[uiLabels addObject:labelMoney];

	// UI_LBL_SBAR_BATTERIES:
	CCLabelTTF * labelBatteries = [CCLabelTTF labelWithString:@":0" fontName:UI_FONT_FILENAME fontSize:20];
	bbox = [batteryIcon boundingBox];
	const int sbarTextY = batteryIcon.position.y - 15;
	labelBatteries.position  = ccp(batteryIcon.position.x + bbox.size.width + 20, sbarTextY);
	labelBatteries.colorRGBA = UI_TEXT_COLOR;
	[uiLabels addObject:labelBatteries];

	// UI_LBL_SBAR_MISSILES:
	CCLabelTTF * labelMissiles = [CCLabelTTF labelWithString:@":0" fontName:UI_FONT_FILENAME fontSize:20];
	bbox = [missileIcon boundingBox];
	labelMissiles.position  = ccp(missileIcon.position.x + bbox.size.width + 25, sbarTextY);
	labelMissiles.colorRGBA = UI_TEXT_COLOR;
	[uiLabels addObject:labelMissiles];

	// UI_LBL_PERF_STATS:
	CCLabelTTF * labelPerfStats = [CCLabelTTF labelWithString:
			@"FPS: 00.0\nSPF: 0.000\nDRAWS: 000\nENTITIES: 000\nWAVES LEFT: 000"
			fontName:UI_FONT_FILENAME fontSize:14];
	labelPerfStats.position = ccp(
		([healthBarFrame boundingBox].origin.x + [healthBarFrame boundingBox].size.width + 150),
		[labelPerfStats boundingBox].size.height + 10);
	labelPerfStats.colorRGBA = UI_TEXT_COLOR;
	[uiLabels addObject:labelPerfStats];

	// Special label only drawn in the pause menu:
	pauseMenuLabel = [CCLabelTTF labelWithString:@"OSSG" fontName:UI_FONT_FILENAME fontSize:48];
	pauseMenuLabel.position  = ccp(popupMenuPanel.position.x + ([popupMenuPanel boundingBox].size.width / 2), popupMenuPanel.position.y - 50);
	pauseMenuLabel.colorRGBA = UI_TEXT_COLOR;

	// Special label only drawn in the end-game menu:
	endGameMenuLabel = [CCLabelTTF labelWithString:@"" fontName:UI_FONT_FILENAME fontSize:32];
	endGameMenuLabel.position  = ccp(popupMenuPanel.position.x + ([popupMenuPanel boundingBox].size.width / 2), popupMenuPanel.position.y - 90);
	endGameMenuLabel.colorRGBA = UI_TEXT_COLOR;

	OSSG_LOG(@"In-game UI labels successfully initialized.");
}

- (void) labelPrint: (UILabelId) labelId format: (NSString *) text, ...
{
	const int labelIndex = (int)labelId;
	OSSG_ASSERT(labelIndex >= 0 && labelIndex < [uiLabels count]);
	OSSG_ASSERT(uiLabels != nil);

	va_list args;
	va_start(args, text);
    NSString * contents = [[NSString alloc] initWithFormat:text arguments:args];
	va_end(args);

	CCLabelTTF * label = [uiLabels objectAtIndex:labelIndex];
	OSSG_ASSERT(label != nil);

	// Set label text and we are done:
	label.string = contents;
}

- (void) refreshSBar
{
	OSSG_ASSERT(Stage   != nil);
	OSSG_ASSERT(Session != nil);

	if (endGameMenuOpen || pauseMenuOpen)
	{
		return;
	}

	// Only refresh the sates that have changed since last check:

	if (Session.playerCredits != sbarState.playerCredits)
	{
		sbarState.playerCredits = Session.playerCredits;
		[self labelPrint:UI_LBL_PLAYER_CREDITS format:@"Credits: %d", sbarState.playerCredits];
	}

	if (Session.stageNum != sbarState.stageNum)
	{
		sbarState.stageNum = Session.stageNum;
		[self labelPrint:UI_LBL_STAGE_NUM format:@"Stage: %d", sbarState.stageNum];
	}

	if ([Session.player getNumBatteries] != sbarState.numBatteries)
	{
		sbarState.numBatteries = [Session.player getNumBatteries];
		[self labelPrint:UI_LBL_SBAR_BATTERIES format:@":%d", sbarState.numBatteries];
	}

	if ([Session.player getNumMissiles] != sbarState.numMissiles)
	{
		sbarState.numMissiles = [Session.player getNumMissiles];
		[self labelPrint:UI_LBL_SBAR_MISSILES format:@":%d", sbarState.numMissiles];
	}

	if (([Session.player getHealth] != sbarState.playerHealth) && ([Session.player getHealth] >= 0))
	{
		sbarState.playerHealth = [Session.player getHealth];
		float scaleX = MapRangeF(
				/* value        */ (float)sbarState.playerHealth,
				/* input range  */ 0.0f, 100.0f,
				/* output range */ 0.0f, SBAR_HEALTH_BAR_SCALE_X);

		// Don't let it quite reach zero, it might look ugly
		if (scaleX < 0.001f)
		{
			scaleX = 0.001f;
		}
		healthBar.scaleX = scaleX;

		// Set color according to damage amount:
		float r,g,b;
		[[Session.player getRenderEntity] getDamageColorRed:&r green:&g blue:&b];
		healthBar.color = [CCColor colorWithRed:r green:g blue:b];
	}
}

- (id) init
{
	OSSG_ASSERT(gameGUICreated == NO);

	self = [super init];
	if (!self)
	{
		return nil;
	}

	// Init everything to safe defaults:
	batteryIcon            = nil;
	missileIcon            = nil;
	healthBarFrame         = nil;
	healthBar              = nil;
	popupMenuPanel         = nil;
	pauseMenuEndStageBtn   = nil;
	pauseMenuContinueBtn   = nil;
	endGameMenuContinueBtn = nil;
	pauseMenuLabel         = nil;
	endGameMenuLabel       = nil;
	pauseMenuOpen          = NO;
	endGameMenuOpen        = NO;
	fadeScreenOverlay      = [PopupMenuFSOverlay node];
	uiLabels               = [NSMutableArray arrayWithCapacity:UI_NUM_LABELS];

	// Pre-load our custom font:
	[CCLabelTTF registerCustomTTF:UI_FONT_FILENAME];

	[self initInGameButtons];
	[self initSBar];
	[self initTextLabels];
	[self initMenus]; // GameGUI+Menus.m

	//
	// The world bounding box is defined by the UI overlays.
	// We don't want the player and other things walking over the UI elements
	//
	const CGSize winSize = [[CCDirector sharedDirector] viewSize];
	WorldBounds.origin.x    = 0;
	WorldBounds.origin.y    = [_mainWeapBtn getBoundingBox].size.height;
	WorldBounds.size.width  = winSize.width;
	WorldBounds.size.height = winSize.height - [_pauseBtn getBoundingBox].size.height;

	gameGUICreated = YES;
	OSSG_LOG(@"GameGUI initialized...");
	return self;
}

- (void) perStageSetup: (GameStage *) gs
{
	OSSG_ASSERT(gs != nil);

	SBAR_RESET();

	// Register buttons for this scene/stage:
	[_mainWeapBtn    linkToScene:gs];
	[_secondWeapBtn1 linkToScene:gs];
	[_secondWeapBtn2 linkToScene:gs];
	[_pauseBtn       linkToScene:gs];

	// Register SBar sprites with this scene/stage:
	[gs addChild:batteryIcon    z:ESORT_UI_ELEMENT];
	[gs addChild:missileIcon    z:ESORT_UI_ELEMENT];
	[gs addChild:healthBarFrame z:ESORT_UI_ELEMENT];
	[gs addChild:healthBar      z:ESORT_UI_ELEMENT];

	// Register text labels with this scene/stage:
	for (CCLabelTTF * label in uiLabels)
	{
		OSSG_ASSERT(label != nil);
		[gs addChild:label z:ESORT_UI_ELEMENT];
	}

	// Make label visible/invisible according to config param:
	CCLabelTTF * label = [uiLabels objectAtIndex:UI_LBL_PERF_STATS];
	label.visible = Session.showPerfStats;

	// Make sure popup menus are hidden:
	[self hidePauseMenu];
	[self hideEndGameMenu];

	// Add popup pause menu to scene:
	[gs addChild:fadeScreenOverlay z:ESORT_FADE_SCREEN];
	[gs addChild:popupMenuPanel    z:ESORT_POPUP_MENU];
	[gs addChild:pauseMenuLabel    z:ESORT_POPUP_MENU_BUTTON];
	[gs addChild:endGameMenuLabel  z:ESORT_POPUP_MENU_BUTTON];

	[pauseMenuEndStageBtn   linkToScene:gs];
	[pauseMenuContinueBtn   linkToScene:gs];
	[endGameMenuContinueBtn linkToScene:gs];
}

- (void) drawCocosPerfStats
{
	// Perf counters:
	static int frames       = 0;
	static double accumDT   = 0;
	static double frameRate = 0;

	frames++;
	accumDT += Time.deltaTime;

	if (accumDT > CC_DIRECTOR_STATS_INTERVAL)
	{
		[self labelPrint:UI_LBL_PERF_STATS format:
			@"FPS: %.1f\nSPF: %.3f\nDRAWS: %d\nENTITIES: %d\nWAVES LEFT: %d",
			frameRate, Time.deltaTime, (int)__ccNumberOfDraws, [Stage entityCount], [Stage wavesLeft]];

		frameRate = (frames / accumDT);
		frames    = 0;
		accumDT   = 0;
	}

	// This variable is Cocos2d internal, so its use here
	// is likely to break compatibility with future library versions.
	// We don't care about that, since the project will never be upgraded.
	__ccNumberOfDraws = 0;
}

- (void) frameUpdate
{
	// Update buttons:
	[_mainWeapBtn           frameUpdate];
	[_secondWeapBtn1        frameUpdate];
	[_secondWeapBtn2        frameUpdate];
	[_pauseBtn              frameUpdate];
	[pauseMenuEndStageBtn   frameUpdate];
	[pauseMenuContinueBtn   frameUpdate];
	[endGameMenuContinueBtn frameUpdate];

	// Update player status bar if needed:
	[self refreshSBar];

	// Draw Cocos2d rendering stats:
	if (Session.showPerfStats)
	{
		[self drawCocosPerfStats];
	}
}

- (void) fadeScreen
{
	fadeScreenOverlay.visible = YES;
}

- (void) showPauseMenu
{
	pauseMenuOpen = YES;
	Stage.paused  = YES;

	pauseMenuContinueBtn.hidden = NO;
	pauseMenuEndStageBtn.hidden = NO;
	pauseMenuLabel.visible      = YES;
	fadeScreenOverlay.visible   = YES;
	popupMenuPanel.visible      = YES;
}

- (void) hidePauseMenu
{
	Stage.paused  = NO;
	pauseMenuOpen = NO;

	pauseMenuContinueBtn.hidden = YES;
	pauseMenuEndStageBtn.hidden = YES;
	pauseMenuLabel.visible      = NO;
	fadeScreenOverlay.visible   = NO;
	popupMenuPanel.visible      = NO;
}

- (BOOL) isPauseMenuOpen
{
	return pauseMenuOpen;
}

- (void) showEndGameMenu: (BOOL) successfulStage
{
	endGameMenuOpen = YES;
	Stage.paused    = YES;

	if (successfulStage)
	{
		endGameMenuLabel.string = @"MISSION SUCCESSFUL!";
	}
	else
	{
		endGameMenuLabel.string = @"MISSION FAILED!";
	}

	endGameMenuContinueBtn.hidden = NO;
	endGameMenuLabel.visible      = YES;
	fadeScreenOverlay.visible     = YES;
	popupMenuPanel.visible        = YES;
}

- (void) hideEndGameMenu
{
	endGameMenuOpen = NO;
	Stage.paused    = NO;

	endGameMenuContinueBtn.hidden = YES;
	endGameMenuLabel.visible      = NO;
	fadeScreenOverlay.visible     = NO;
	popupMenuPanel.visible        = NO;
}

- (BOOL) isEndGameMenuOpen
{
	return endGameMenuOpen;
}

@end
