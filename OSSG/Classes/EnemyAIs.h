//
//  EnemyAIs.h
//  OSSG - All enemy AI implementations.
//
//  Created by Guilherme R. Lampert on 22/04/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "EntityAI.h"

// ======================================================
// AIStraightMover interface:
// ======================================================

//
// The straight mover just travels in a straight line,
// from right to left, firing its weapons in a random interval.
// It will fire all weapons, including specials, if they have ammo.
//
@interface AIStraightMover : EntityAI

// By default = NO
@property(nonatomic) BOOL firesHomingMissiles;

// Methods implemented from EntityAI:
- (id)         initWithType: (AIType) tp;
- (void)       think;
- (EntityAI *) clone;

@end

// ======================================================
// AIWaveMover interface:
// ======================================================

//
// Moves in a sine wave. Dancing up and down.
// This AI type builds on top of AIStraightMover, thus
// also moving its owner from right to left.
// It will fire all weapons, including specials, if they have ammo.
//
@interface AIWaveMover : AIStraightMover

// Sine wave length and scale.
// Scale controls how hight the entity goes before descending.
// Length controls the interval between the waves.
@property(nonatomic) double waveLength;
@property(nonatomic) double waveScale;

// Methods overwritten from AIStraightMover:
- (id)         initWithType: (AIType) tp;
- (void)       think;
- (EntityAI *) clone;

@end

// ======================================================
// AIDroneMine interface:
// ======================================================

//
// A Drone Mine rotates in its own axis (just a decorative effect)
// and moves from the right of the screen to the left.
// Does not fires any weapons.
//
@interface AIDroneMine : EntityAI

// Methods implemented from EntityAI:
- (id)         initWithType: (AIType) tp;
- (void)       think;
- (EntityAI *) clone;

@end

// ======================================================
// AIBoss interface:
// ======================================================

//
// "Boss" or just a very tough enemy type.
//
@interface AIBoss : EntityAI

// Methods implemented from EntityAI:
- (id)         initWithType: (AIType) tp;
- (void)       think;
- (EntityAI *) clone;

@end

// ======================================================
// AIHomingMissile interface:
// ======================================================

//
// Behavior for a homing missile/projectile.
// Has the ability to follow and hit a target entity.
//
@interface AIHomingMissile : EntityAI

- (id)   initWithType: (AIType) tp;
- (void) think;
- (BOOL) acquireTarget;
- (EntityAI *) clone;

@end
