//
//  GameGUI_Menus.m
//  OSSG - All menus used outside game stages.
//
//  Created by Guilherme R. Lampert on 22/05/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "GameGUI_Menus.h"

// ======================================================
// Game Menus:
// ======================================================

// Categories don't have an easy support for instance variables.
// Luckily for us, there are no issues in making these menu
// instances static locals. They remain in memory for the lifetime of
// the app and GameGUI is a also singleton.

static MenuScreen * homeMenu;
static MenuScreen * newGameMenu;
static MenuScreen * continueGameMenu;
static MenuScreen * preferencesMenu;
static MenuScreen * selectShipMenu;
static MenuScreen * upgradesMenu;
static ListMenu   * saveGameList;
static NSString   * saveFiles[MAX_LIST_MENU_ENTRIES];

#define TOP_LABEL_SIZE        24
#define SIDE_LABEL_SIZE_SMALL 48
#define SIDE_LABEL_SIZE_BIG   128
#define MENU_BACKGROUND_MUSIC_VOLUME  0.5f
#define STAGE_BACKGROUND_MUSIC_VOLUME 0.5f

// ======================================================
// GameGUI+Menus Category Implementation:
// ======================================================

@implementation GameGUI (Menus)

//
// Methods:
//

- (void) initHomeMenu
{
	const float buttonSpacing = 120;
	float x, y;

	[homeMenu setRightSideLabel:@"OSSG" fontSize:SIDE_LABEL_SIZE_BIG];

	CustomControl * newGameBtn = [homeMenu newButton:@"New Game" smallButton:NO];
	x = ([MenuScreen borderX] + ([newGameBtn getBoundingBox].size.width / 2) - 50);
	y = [MenuScreen height] - [MenuScreen borderY];
	[newGameBtn setPosition:ccp(x, y)];

	CustomControl * continueGameBtn = [homeMenu newButton:@"Continue Game" smallButton:NO];
	y -= ([continueGameBtn getBoundingBox].size.height + buttonSpacing);
	[continueGameBtn setPosition:ccp(x, y)];

	CustomControl * preferencesBtn = [homeMenu newButton:@"Preferences" smallButton:NO];
	y -= ([preferencesBtn getBoundingBox].size.height + buttonSpacing);
	[preferencesBtn setPosition:ccp(x, y)];

	[homeMenu setNextMenu:newGameMenu      index:0 withButton:newGameBtn];
	[homeMenu setNextMenu:continueGameMenu index:1 withButton:continueGameBtn];
	[homeMenu setNextMenu:preferencesMenu  index:2 withButton:preferencesBtn];
}

- (void) initNewGameMenu
{
	float x, y;

	[newGameMenu setRightSideLabel:@"NEW GAME" fontSize:SIDE_LABEL_SIZE_SMALL];
	[newGameMenu setPrevMenu:homeMenu];
	[newGameMenu setNextMenu:selectShipMenu];

	x = [MenuScreen borderX] + 50;
	y = [MenuScreen height] - [MenuScreen borderY] - 100;
	__block CustomTextField * textField = [newGameMenu newTextFieldAt:ccp(x, y) withText:@"Player_1" andTitle:@"PLAYER NAME"];

	// Menu actions:

	newGameMenu.onMenuEnter = ^void(MenuScreen * menu) {
		textField.string = @"Player_1"; // Set default text.
	};

	newGameMenu.onNext = ^BOOL(MenuScreen * menu, int nextIndex) {
		// Do not allow player to proceed with and invalid name:
		if ((textField.string.length == 0) || (textField.string.length >= MAX_PLAYER_NAME_LEN))
		{
			[SoundSys playSoundAsync:SND_ALERT];
			return NO;
		}
		[Session setPlayerName:textField.string];
		return YES;
	};
}

- (void) initContinueGameMenu
{
#define EMPTY_SAVE_SLOT_TEXT @"Empty Slot"
#define ADD_SAVE_ENTRY(name, index)\
	[saveGameList setEntry:(index) withText:(name) andCallback:\
		^void(CustomControl * button, BOOL state) {\
			[saveGameList resetAllControls];\
			button.switchButtonState = state;\
	}]

	saveGameList = [continueGameMenu newListMenu];
	[continueGameMenu setRightSideLabel:@"CONTINUE GAME" fontSize:SIDE_LABEL_SIZE_SMALL];
	[continueGameMenu setPrevMenu:homeMenu];
	[continueGameMenu setNextMenu:upgradesMenu];

	// Once we enter the menu, refresh the save-game list:
	continueGameMenu.onMenuEnter = ^void(MenuScreen * menu) {
		int savesFound = 0;
		NSDictionary * dict = [Session getSaveFileList];

		// Add entries for existent files:
		for (NSString * filename in dict.allKeys)
		{
			saveFiles[savesFound] = filename;
			NSString * playerName = [dict objectForKey:filename];
			ADD_SAVE_ENTRY(playerName, savesFound);
			savesFound++;
		}

		// Fill the rest with empty entries:
		while (savesFound < MAX_LIST_MENU_ENTRIES)
		{
			saveFiles[savesFound] = EMPTY_SAVE_SLOT_TEXT;
			ADD_SAVE_ENTRY(EMPTY_SAVE_SLOT_TEXT, savesFound);
			savesFound++;
		}
	};

	// Make sure the selected save slot is not empty:
	continueGameMenu.onNext = ^BOOL(MenuScreen * menu, int nextIndex) {
		const CustomControl * selectedEntry = [saveGameList getFirstSelectedEntry];
		if (selectedEntry == nil)
		{
			[SoundSys playSoundAsync:SND_ALERT];
			return NO;
		}

		const int entryIndex = [saveGameList getEntryIndex:selectedEntry];
		OSSG_ASSERT(entryIndex >= 0 && entryIndex < MAX_LIST_MENU_ENTRIES);

		NSString * saveFileName = saveFiles[entryIndex];
		OSSG_ASSERT(saveFileName != nil);

		if ([saveFileName isEqualToString:EMPTY_SAVE_SLOT_TEXT] ||
		   ![Session startNewGameWithSaveFile:saveFileName])
		{
			[SoundSys playSoundAsync:SND_ALERT];
			return NO;
		}
		else
		{
			// Selected a valid slot, Proceed to next menu.
			return YES;
		}
	};
}

- (void) initPreferencesMenu
{
	const float buttonSpacing = 20;
	float x, y;

	[preferencesMenu setRightSideLabel:@"PREFERENCES" fontSize:SIDE_LABEL_SIZE_SMALL];
	[preferencesMenu setPrevMenu:homeMenu];

	CustomControl * enableSoundBtn = [preferencesMenu newSwitchButton:@"Enable Sound"];
	x = ([MenuScreen borderX] + ([enableSoundBtn getBoundingBox].size.width / 2) - 50);
	y = [MenuScreen height] - [MenuScreen borderY];
	[enableSoundBtn setPosition:ccp(x, y)];

	enableSoundBtn.switchButtonState = Session.enableSound;
	enableSoundBtn.onToggle = ^void(CustomControl * button, BOOL state) {
		if (state == NO)
		{
			[SoundSys stopEverything];
		}
		Session.enableSound = state;
		[SoundSys playBgMusic:BGM_MENU withVolume:MENU_BACKGROUND_MUSIC_VOLUME looping:YES];
	};

	CustomControl * showPerfStatsBtn = [preferencesMenu newSwitchButton:@"Show Geeky Stats"];
	y -= ([showPerfStatsBtn getBoundingBox].size.height + buttonSpacing);
	[showPerfStatsBtn setPosition:ccp(x, y)];

	showPerfStatsBtn.switchButtonState = Session.showPerfStats;
	showPerfStatsBtn.onToggle = ^void(CustomControl * button, BOOL state) {
		Session.showPerfStats = state;
	};

	CustomControl * godModeBtn = [preferencesMenu newSwitchButton:@"God Mode"];
	y -= ([godModeBtn getBoundingBox].size.height + buttonSpacing);
	[godModeBtn setPosition:ccp(x, y)];

	godModeBtn.switchButtonState = Session.godMode;
	godModeBtn.onToggle = ^void(CustomControl * button, BOOL state) {
		Session.godMode = state;
	};

	// Special "delete all saved games" button:
	{
		CustomControl * deleteSavesBtn = [preferencesMenu newSwitchButton:@"Delete All Saves"];
		y -= ([deleteSavesBtn getBoundingBox].size.height + buttonSpacing);
		[deleteSavesBtn setPosition:ccp(x, y)];

		deleteSavesBtn.switchButtonState = NO;
		deleteSavesBtn.onToggle = ^void(CustomControl * button, BOOL state) {
			ShowCancelContinueSystemDlg(
				@"Delete all saved games?",
				@"Attention, this cannot be undone. Are you sure?",
				^void(BOOL selection) {
					if (selection == YES) // Continue
					{
						[Session deleteAllSaveGames];
					}
					[button resetStates];
					button.switchButtonState = NO;
				}
			);
		};
	}
}

- (void) initSelectShipMenu
{
	const float buttonSpacing = 80;
	float x, y;

	[selectShipMenu setRightSideLabel:@"SELECT SHIP" fontSize:SIDE_LABEL_SIZE_SMALL];
	[selectShipMenu setPrevMenu:newGameMenu];
	[selectShipMenu setNextMenu:upgradesMenu];

	__block CustomControl * btnShipLightFighter = [selectShipMenu newSwitchButton:@"Light Fighter:\nFast and deadly" withIcon:@"icon_light_fighter"];
	x = ([MenuScreen borderX] + ([btnShipLightFighter getBoundingBox].size.width / 2) - 50);
	y = [MenuScreen height] - [MenuScreen borderY] - buttonSpacing;
	[btnShipLightFighter setPosition:ccp(x, y)];
	btnShipLightFighter.alwaysShowIcon = YES;

	__block CustomControl * btnShipJuggernaut = [selectShipMenu newSwitchButton:@"The Juggernaut:\nHeavyweight killing machine" withIcon:@"icon_juggernaut"];
	x = ([MenuScreen borderX] + ([btnShipJuggernaut getBoundingBox].size.width / 2) - 50);
	y -= ([btnShipJuggernaut getBoundingBox].size.height + buttonSpacing);
	[btnShipJuggernaut setPosition:ccp(x, y)];
	btnShipJuggernaut.alwaysShowIcon = YES;

	// Only allow one section at a time.
	// First ship is the default selection.

	btnShipLightFighter.switchButtonState = YES;
	btnShipLightFighter.onToggle = ^void(CustomControl * button, BOOL state) {
		btnShipJuggernaut.switchButtonState = NO;
	};

	btnShipJuggernaut.switchButtonState = NO;
	btnShipJuggernaut.onToggle = ^void(CustomControl * button, BOOL state) {
		btnShipLightFighter.switchButtonState = NO;
	};

	selectShipMenu.onNext = ^BOOL(MenuScreen * menu, int nextIndex) {
		if (btnShipLightFighter.switchButtonState == YES)
		{
			return [Session startNewGameWithShip:@"light_fighter"];
		}
		else if (btnShipJuggernaut.switchButtonState == YES)
		{
			return [Session startNewGameWithShip:@"juggernaut"];
		}
		else // Need to select one of 'em
		{
			[SoundSys playSoundAsync:SND_ALERT];
			return NO;
		}
	};
}

- (void) initUpgradesMenu
{
	const float buttonSpacing = 80;
	float x, y;

	[upgradesMenu setRightSideLabel:@"UPGRADES" fontSize:SIDE_LABEL_SIZE_SMALL];
	[upgradesMenu setPrevMenu:homeMenu];

	// Top label with player credits:
	[upgradesMenu setTopSideLabel:@"CREDITS: 0   NEXT STAGE: " fontSize:TOP_LABEL_SIZE];

	// Upgrade selection buttons:
	NSString * text;

	// Missile upgrade button:
	text = @"> Upgrade Missiles <\ncost: 00\ncurrent dmg: 00 next: 00";
	__block CustomControl * upgradeMissileBtn = [upgradesMenu newSwitchButton:text withIcon:@"icon_missile_alternate"];
	x = ([MenuScreen borderX] + ([upgradeMissileBtn getBoundingBox].size.width / 2) - 50);
	y = [MenuScreen height] - [MenuScreen borderY] - buttonSpacing;
	[upgradeMissileBtn setPosition:ccp(x, y)];
	upgradeMissileBtn.alwaysShowIcon = YES;

	// Shield upgrade button:
	text = @"> Upgrade Shield <\ncost: 00\ncurrent str: 00 next: 00";
	__block CustomControl * upgradeShieldBtn = [upgradesMenu newSwitchButton:text withIcon:@"icon_shield"];
	x = ([MenuScreen borderX] + ([upgradeShieldBtn getBoundingBox].size.width / 2) - 50);
	y -= ([upgradeShieldBtn getBoundingBox].size.height + buttonSpacing);
	[upgradeShieldBtn setPosition:ccp(x, y)];
	upgradeShieldBtn.alwaysShowIcon = YES;

	// Set actions:
	__block void(^dynamicTextRefresh)(MenuScreen *) = ^void(MenuScreen * menu) {
		// Update dynamic texts:
		[menu getTopSideLabel].string = [NSString stringWithFormat:@"CREDITS: %d   NEXT STAGE: %d",
			Session.playerCredits, Session.stageNum];

		NSString * str;
		int currentLevel;
		int upgradeCost;

		upgradeCost  = [Session getPlayerMissileUpgradeCost];
		currentLevel = [Session getPlayerMissileUpgradeLevel];
		str = [NSString stringWithFormat:@"> Upgrade Missiles <\ncost: %d\ncurrent dmg: %d next: %d",
			 upgradeCost, currentLevel, currentLevel + [Session getPlayerMissileUpgradeIncrement]];
		[upgradeMissileBtn setTooltipText:str];

		upgradeCost  = [Session getPlayerShieldUpgradeCost];
		currentLevel = [Session getPlayerShieldUpgradeLevel];
		str = [NSString stringWithFormat:@"> Upgrade Shield <\ncost: %d\ncurrent str: %d next: %d",
			 upgradeCost, currentLevel, currentLevel + [Session getPlayerShieldUpgradeIncrement]];
		[upgradeShieldBtn setTooltipText:str];
	};

	// On entering the menu, we must refresh the dynamic texts:
	upgradesMenu.onMenuEnter = dynamicTextRefresh;

	// Buy upgrade button:
	CustomControl * buyUpgradeBtn = [upgradesMenu newButton:@"Upgrade" smallButton:YES];
	x = ([MenuScreen borderX] + ([buyUpgradeBtn getBoundingBox].size.width / 2) - 50) * 2;
	[buyUpgradeBtn setPosition:ccp(x, [MenuScreen borderY])];

	// Start game button:
	CustomControl * startGameBtn = [upgradesMenu newButton:@"<< Start >>" smallButton:YES];
	x += ([MenuScreen borderX] + ([startGameBtn getBoundingBox].size.width / 2) - 50);
	[startGameBtn setPosition:ccp(x, [MenuScreen borderY])];

	// Bit of a hack: add self so we can add the custom button above:
	[upgradesMenu setNextMenu:upgradesMenu index:0 withButton:startGameBtn];

	// Set button actions:
	upgradeMissileBtn.switchButtonState = NO;
	upgradeMissileBtn.onToggle = ^void(CustomControl * button, BOOL state) {
		upgradeShieldBtn.switchButtonState = NO;
	};
	upgradeShieldBtn.switchButtonState = NO;
	upgradeShieldBtn.onToggle = ^void(CustomControl * button, BOOL state) {
		upgradeMissileBtn.switchButtonState = NO;
	};

	// Buy selected upgrade:
	buyUpgradeBtn.button.block = ^void(id sender) {
		[SoundSys playSoundAsync:SND_BTN_CLICK];

		// Buy upgrades:
		if (upgradeMissileBtn.switchButtonState == YES) // Missiles
		{
			if ([Session tryUpgradePlayerMissiles])
			{
				[SoundSys playSoundAsync:SND_UPGRADE];
			}
			else // No money!
			{
				[SoundSys playSoundAsync:SND_ALERT];
			}
		}
		else if (upgradeShieldBtn.switchButtonState == YES) // Shield
		{
			if ([Session tryUpgradePlayerShield])
			{
				[SoundSys playSoundAsync:SND_UPGRADE];
			}
			else // No money!
			{
				[SoundSys playSoundAsync:SND_ALERT];
			}
		}
		else // No upgrade selected.
		{
			[SoundSys playSoundAsync:SND_ALERT];
		}

		// Refresh button labels and dynamic text.
		dynamicTextRefresh(upgradesMenu);
	};

	// This block will disable a menu switch and direct to the game stage.
	upgradesMenu.onNext = ^BOOL(MenuScreen * menu, int nextIndex) {
		// Reset UI elements:
		upgradeMissileBtn.switchButtonState = NO;
		upgradeShieldBtn.switchButtonState  = NO;
		[Gui beginStage];
		// Don't switch. beginStage already posted a message with CCDirector.
		return NO;
	};

	// Clear button state on exit:
	upgradesMenu.onPrev = ^BOOL(MenuScreen * menu) {
		upgradeMissileBtn.switchButtonState = NO;
		upgradeShieldBtn.switchButtonState  = NO;
		return YES; // Can now switch menus
	};
}

- (void) initMenus
{
	OSSG_LOG(@"Initializing all menus...");

	// Create all instances first. The init methods will
	// cross reference the menus for prev/next chaining.
	homeMenu         = [MenuScreen new];
	newGameMenu      = [MenuScreen new];
	continueGameMenu = [MenuScreen new];
	preferencesMenu  = [MenuScreen new];
	selectShipMenu   = [MenuScreen new];
	upgradesMenu     = [MenuScreen new];

	[self initHomeMenu];
	[self initNewGameMenu];
	[self initContinueGameMenu];
	[self initPreferencesMenu];
	[self initSelectShipMenu];
	[self initUpgradesMenu];

	OSSG_LOG(@"Menus ready!");
}

- (void) beginStage
{
	Stage = [GameStage new];

	// Replace menus with a new game stage:
	[[CCDirector sharedDirector] replaceScene:Stage withTransition:
		[CCTransition transitionRevealWithDirection:CCTransitionDirectionUp duration:1]];

	[SoundSys playSoundAsync:SND_MENU_CHANGE];

	// Stop menu background music and play the stage music:
	[SoundSys stopBgMusic];
	[SoundSys playBgMusic:BGM_IN_GAME withVolume:STAGE_BACKGROUND_MUSIC_VOLUME looping:YES];
}

- (void) endStage
{
	Stage = nil;

	// Return to the upgrades menu screen:
	[upgradesMenu resetAllControls];
	[[CCDirector sharedDirector] replaceScene:upgradesMenu withTransition:
		[CCTransition transitionMoveInWithDirection:CCTransitionDirectionDown duration:1]];

	[SoundSys playSoundAsync:SND_MENU_CHANGE];

	// Stop stage music and play menu background music:
	[SoundSys stopBgMusic];
	[SoundSys playBgMusic:BGM_MENU withVolume:MENU_BACKGROUND_MUSIC_VOLUME looping:YES];
}

- (MenuScreen *) startHomeMenu
{
	[SoundSys playBgMusic:BGM_MENU withVolume:MENU_BACKGROUND_MUSIC_VOLUME looping:YES];
	return homeMenu;
}

@end
