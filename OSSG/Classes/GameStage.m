//
//  GameStage.m
//  OSSG
//
//  Created by Guilherme R. Lampert on 27/03/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "GameStage.h"

// ======================================================
// Constants / locals:
// ======================================================

// Added to every touch.x to position the player's ship
// a bit further ahead of where the user touched.
#define PLAYER_SHIP_MOVE_X_OFFSET 80

#define ENEMY_SPAWN_Y_PAD   40
#define NUM_ENEMY_TEMPLATES 4

typedef enum eFormationType {
	FORMATION_COLUMN,
	FORMATION_SWARM,
	FORMATION_INTERLEAVED,
	FORMATION_STEPS_LEFT,
	FORMATION_STEPS_RIGHT,
	FORMATION_ARROW_LEFT,
	FORMATION_ARROW_RIGHT,
	NUM_FORMATIONS
} FormationType;

// ======================================================
// GameStage implementation:
// ======================================================

@implementation GameStage
{
	// All Game Entities active for this stage.
	NSMutableArray * entities;

	// Enemy wave counters:
	int waveCount;
	int maxWaves;

	// Wave times:
	double lastTimeWaveSpawned;
	double timeBetweenWaves;

	// Template enemies that serve as prototype to all others.
	// The higher the index the more powerful the enemy is.
	// E.g.: enemyTemplates[3] is tougher than enemyTemplates[0]
	GameEntity * enemyTemplates[NUM_ENEMY_TEMPLATES];

	// Controls the number of enemies for each formation in this stage.
	int numEnemiesPerFormation[NUM_FORMATIONS];

	// Weapons used by enemies:
	ProjectileDescriptor * commonWeaponDescriptor;
	ProjectileDescriptor * missileDescriptor;
}

//
// Methods:
//

- (void) loadEnemies
{
	const int diff = Session.stageNum;
	OSSG_ASSERT(diff > 0);

	// Boss in this stage?
	_hasBoss = (Session.stagesToNextBoss == 1);

	waveCount = 0;
	maxWaves  = (int)RandInRange(3, 6) + (int)log((double)Session.stageNum);

	timeBetweenWaves = (int)RandInRange(5, 20);
	lastTimeWaveSpawned = -1; // Start immediately

	numEnemiesPerFormation[FORMATION_COLUMN]      = diff * diff / 1000;
	numEnemiesPerFormation[FORMATION_SWARM]       = diff * diff / 1000;
	numEnemiesPerFormation[FORMATION_INTERLEAVED] = 4 + diff / 10;
	numEnemiesPerFormation[FORMATION_STEPS_LEFT]  = diff * diff / 500;
	numEnemiesPerFormation[FORMATION_STEPS_RIGHT] = diff * diff / 500;
	numEnemiesPerFormation[FORMATION_ARROW_LEFT]  = 3 + diff / 10;
	numEnemiesPerFormation[FORMATION_ARROW_RIGHT] = 3 + diff / 10;

	// Load enemies from configs:
	enemyTemplates[0] = [GameEntity spawnWithFile:@"enemy_mine"];
	enemyTemplates[1] = [GameEntity spawnWithFile:@"enemy1_weak"];
	enemyTemplates[2] = [GameEntity spawnWithFile:@"enemy_bouncer"];
	enemyTemplates[3] = [GameEntity spawnWithFile:@"enemy1_strong"];

	// Common/main weapon:
	commonWeaponDescriptor = [ProjectileDescriptor new];
	commonWeaponDescriptor.spriteName       = @"shot";
	commonWeaponDescriptor.trailSpriteName  = nil;
	commonWeaponDescriptor.aiType           = AI_NONE;
	commonWeaponDescriptor.sound            = SND_FIRE_MAIN_WEAP;
	commonWeaponDescriptor.spriteScale      = 1.0f;
	commonWeaponDescriptor.batteriesCost    = 0;
	commonWeaponDescriptor.missileCost      = 0;
	commonWeaponDescriptor.damageInflicted  = 5;
	commonWeaponDescriptor.fireCoolDownTime = 0.5f;
	commonWeaponDescriptor.lastTimeFired    = 0;
	commonWeaponDescriptor.acceleration     = 1000;
	commonWeaponDescriptor.turnSpeed        = 10;

	// Missile launcher:
	missileDescriptor = [ProjectileDescriptor new];
	missileDescriptor.spriteName       = @"missile";
	missileDescriptor.trailSpriteName  = @"streak";
	missileDescriptor.aiType           = AI_NONE;
	missileDescriptor.sound            = SND_FIRE_MISSILE;
	missileDescriptor.spriteScale      = 0.7f;
	missileDescriptor.batteriesCost    = 0;
	missileDescriptor.missileCost      = 1;
	missileDescriptor.damageInflicted  = 20;
	missileDescriptor.fireCoolDownTime = 1.0f;
	missileDescriptor.lastTimeFired    = 0;
	missileDescriptor.acceleration     = 1000;
	missileDescriptor.turnSpeed        = 10;

	static const char streakFileName[] = "streak";
	missileDescriptor.trailParams = (TrailRenderParams) {
		/* baseTexture = */ streakFileName,
		/* rgba        = */ 0.0f,0.0f,0.0f,0.5f,
		/* fadeTimeSec = */ 0.5f,
		/* minSegments = */ 2,
		/* width       = */ 4,
		/* fastMode    = */ YES
	};
}

- (void) spawnEnemyFormation: (GameEntity *) enemyTemplate maxInstances: (int) maxEnemies type: (FormationType) formationType
{
	OSSG_ASSERT(enemyTemplate != nil);

	if (maxEnemies <= 0)
	{
		maxEnemies = 1;
	}

	const CGRect bbox = [enemyTemplate getRenderEntity].boundingBox;
	const int maxWorldHeight = (int)(WorldBounds.size.height - WorldBounds.origin.y - (ENEMY_SPAWN_Y_PAD * 2));

	const int enemySpacingY = abs((maxWorldHeight / maxEnemies) - (int)bbox.size.height);
	const int enemySizeYMax = (int)bbox.size.height + enemySpacingY;

	int numEnemies = (maxWorldHeight / enemySizeYMax);
	if (numEnemies > maxEnemies)
	{
		numEnemies = maxEnemies;
	}

	const int enemySpawnStartX = (Session.stageNum <= 5) ? (rand() % 50) : (rand() % 20);

	int j = 0;
	float xAdjust = 0;
	const int threshold = (numEnemies >= 2) ? (numEnemies / 2) : numEnemies;
	if ((formationType == FORMATION_STEPS_LEFT) || (formationType == FORMATION_ARROW_LEFT))
	{
		j = numEnemies - 1;
	}

	for (int i = 0; i < numEnemies; ++i)
	{
		switch (formationType)
		{
		case FORMATION_COLUMN :
			xAdjust = 0;
			break;

		case FORMATION_SWARM :
			xAdjust = (enemySizeYMax * sin(i * 4.5) * 4.0);
			break;

		case FORMATION_INTERLEAVED :
			xAdjust = (enemySizeYMax * j);
			j = !j;
			break;

		case FORMATION_STEPS_LEFT :
			xAdjust = (enemySizeYMax * j);
			j--;
			break;

		case FORMATION_STEPS_RIGHT :
			xAdjust = (enemySizeYMax * i);
			break;

		case FORMATION_ARROW_LEFT :
			xAdjust = (enemySizeYMax * j);
			if (i < threshold) { j--; } else { j++; }
			break;

		case FORMATION_ARROW_RIGHT :
			xAdjust = (enemySizeYMax * j);
			if (i >= threshold) { j--; } else { j++; }
			break;

		default :
			OSSG_ERROR(@"Invalid FormationType enum!");
			break;
		} // switch (formationType)

		const float x = WorldBounds.size.width + bbox.size.width + enemySpawnStartX + xAdjust;
		const float y = WorldBounds.origin.y + (enemySizeYMax * i) + ENEMY_SPAWN_Y_PAD;

		GameEntity * enemyInstance = [enemyTemplate clone];
		[enemyInstance setPosition:ccp(x, y + (enemySpacingY / 2))];

		const int sel = rand() % 3;
		if (sel == 0) // Get a common weapon
		{
			[enemyInstance setProjectileDescriptor:commonWeaponDescriptor forWeapon:WEAP_MAIN];
		}
		else if ((sel == 1) && (Session.stageNum > 5)) // Get a missile launcher (after the first 5 stages only)
		{
			[enemyInstance setNumMissiles:(rand() % 8)];
			[enemyInstance setProjectileDescriptor:missileDescriptor forWeapon:WEAP_SECONDARY1];
		}
		// else gets nothing

		[self linkEntity:enemyInstance];
	}
}

- (void) spawnBoss
{
	GameEntity * boss = [GameEntity spawnWithFile:@"enemy_boss"];

	[boss setDamageInflicted:30]; // If the boss runs over the player.
	[boss setProjectileDescriptor:commonWeaponDescriptor forWeapon:WEAP_MAIN];
	[boss setProjectileDescriptor:missileDescriptor forWeapon:WEAP_SECONDARY1];

	const float x = WorldBounds.size.width + [[boss getRenderEntity] boundingBox].size.width + (rand() % 20);
	const float y = WorldBounds.origin.y + (rand() % (int)(WorldBounds.size.height - [[boss getRenderEntity] boundingBox].size.height));

	[boss setPosition:ccp(x, y)];
	[boss setNumMissiles:(rand() % 10)];

	[self linkEntity:boss];
}

- (id) init
{
	self = [super init];
	if (!self)
	{
		return nil;
	}

	// Set stage pointer so that entities can now reference it.
	Stage = self;

	// Enable touch handling on this scene node:
	self.userInteractionEnabled = YES;
	self.multipleTouchEnabled   = YES;
	self.exclusiveTouch         = NO;

	// Seed random:
	SeedRandGen();

	// Create the array object:
	entities = [NSMutableArray new];

	// Load template enemy instances:
	[self loadEnemies];

	// Do this AFTER loading sprite sheets!
	[RenderEntity perStageSetup:self];

	// Link player and other global entities with the CCScene:
	[Session perStageSetup:self];

	// Link UI elements with the CCScene:
	[Gui perStageSetup:self];

	OSSG_LOG(@"New GameStage created. Stage num: %d", Session.stageNum);
	OSSG_LOG(@"WorldBounds = (x:%f, y:%f, w:%f, h:%f)",
			WorldBounds.origin.x,   WorldBounds.origin.y,
			WorldBounds.size.width, WorldBounds.size.height);

	return self;
}

- (void) dealloc
{
	OSSG_LOG(@"Deallocating Game Stage...");

	[self removeAllChildren];
	[entities removeAllObjects];
	entities = nil;
}

- (void) spawnNewEnemyWave
{
	if (_hasBoss && ([self wavesLeft] == 1))
	{
		[self spawnBoss];
	}
	else // Common enemy wave
	{
		const int formationType = rand() % NUM_FORMATIONS; // [0, NUM_FORMATIONS-1]
		OSSG_ASSERT(formationType < NUM_FORMATIONS);

		const int enemyType = rand() % NUM_ENEMY_TEMPLATES; // [0, NUM_ENEMY_TEMPLATES-1]
		OSSG_ASSERT(enemyType < NUM_ENEMY_TEMPLATES);

		const int numEnemies = numEnemiesPerFormation[formationType];
		[self spawnEnemyFormation:enemyTemplates[enemyType] maxInstances:numEnemies type:(FormationType)formationType];
	}

	waveCount++;
}

- (void) update: (CCTime) deltaTime
{
	// Update global timer:
	Time.deltaTime   = deltaTime;
	Time.currentTime = AbsoluteTimeSeconds();

	// One function to update them all!
	for (unsigned int i = 0; i < [entities count]; ++i)
	{
		GameEntity * ent = [entities objectAtIndex:i];
		OSSG_ASSERT(ent != nil);
		[ent frameUpdate];
	}

	// GUI update:
	[Gui frameUpdate];

	// Check game states for victory, defeat, etc.
	[Session frameUpdate];

	// Spawn more enemies if necessary:
	if ((lastTimeWaveSpawned < 0) ||
		(((Time.currentTime - lastTimeWaveSpawned) >= timeBetweenWaves) && (waveCount < maxWaves)))
	{
		lastTimeWaveSpawned = Time.currentTime;
		[self spawnNewEnemyWave];
	}
}

- (BOOL) checkHits: (GameEntity *) projectile
{
	OSSG_ASSERT(projectile != nil);
	BOOL hitSomething = NO;

	for (unsigned int i = 0; i < [entities count]; ++i)
	{
		GameEntity * ent = [entities objectAtIndex:i];

		// Don't try to hit self:
		if ((ent == projectile) || ([ent uid] == [projectile uid]))
		{
			continue;
		}

		if ([ent intersects:projectile])
		{
			hitSomething = [ent receiveHit:projectile];
			if ([projectile getHealth] <= 0)
			{
				break;
			}
		}
	}

	return hitSomething;
}

- (BOOL) stageCleared
{
	if ([self wavesLeft] == 0)
	{
		if ([self entityCount] == 1)
		{
			// Just the player left, we are done.
			if ([self isEntityLinked:Session.player])
			{
				return YES;
			}
		}
		else if ([self entityCount] == 2)
		{
			// Only the player and its shield are still linked,
			// stage was cleared of enemies.
			if ([self isEntityLinked:Session.player] &&
				[self isEntityLinked:[Session.player getShield]])
			{
				return YES;
			}
		}
	}

	return NO;
}

- (int) entityCount
{
	return (int)[entities count];
}

- (int) wavesLeft
{
	return maxWaves - waveCount;
}

- (BOOL) isEntityLinked: (GameEntity *) ent;
{
	if (ent == nil)
	{
		return NO;
	}

	return [entities containsObject:ent];
}

- (void) linkEntity: (GameEntity *) ent
{
	// Must be a valid object, of course
	if ((ent == nil) || ([ent getRenderEntity] == nil))
	{
		return;
	}

	// No duplicates allowed!
	OSSG_ASSERT(![self isEntityLinked:ent]);

	// Add to array:
	[entities addObject:ent];

	// And add to Cocos scene:
	[[ent getRenderEntity] stageLink:self];
}

- (void) unlinkEntity: (GameEntity *) ent
{
	if ((ent == nil) || ([ent getRenderEntity] == nil))
	{
		return;
	}

	// Remove from Cocos scene, so it will stop drawing it:
	[[ent getRenderEntity] stageUnlink:self];

	// Only removes if any object equal to 'ent' is found.
	// Searches by memory address.
	[entities removeObjectIdenticalTo:ent];

	// Will also remove from the session
	// (noting that the object might still be recycled)
	[GameEntity remove:ent];
}

-(void) touchBegan: (UITouch *) touch withEvent: (UIEvent *) event
{
	CGPoint touchLoc = [touch locationInNode:self];

	if (![Gui.mainWeapBtn    pointIntersects:touchLoc] &&
		![Gui.secondWeapBtn1 pointIntersects:touchLoc] &&
		![Gui.secondWeapBtn2 pointIntersects:touchLoc] &&
		![Gui.pauseBtn       pointIntersects:touchLoc])
	{
		// Touching anywhere besides a button: move the player ship.
		touchLoc.x += PLAYER_SHIP_MOVE_X_OFFSET;
		[Session.player moveToPosition:touchLoc setTargetAngle:NO];
	}
}

- (void) touchMoved: (UITouch *) touch withEvent: (UIEvent *) event
{
	CGPoint touchLoc = [touch locationInNode:self];

	if (![Gui.mainWeapBtn    pointIntersects:touchLoc] &&
		![Gui.secondWeapBtn1 pointIntersects:touchLoc] &&
		![Gui.secondWeapBtn2 pointIntersects:touchLoc] &&
		![Gui.pauseBtn       pointIntersects:touchLoc])
	{
		// Touching anywhere besides a button: move the player ship.
		touchLoc.x += PLAYER_SHIP_MOVE_X_OFFSET;
		[Session.player moveToPosition:touchLoc setTargetAngle:NO];
	}
}

- (void) touchEnded: (UITouch *) touch withEvent: (UIEvent *) event
{
	const CGPoint touchLoc = [touch locationInNode:self];

	if (![Gui.mainWeapBtn    pointIntersects:touchLoc] &&
		![Gui.secondWeapBtn1 pointIntersects:touchLoc] &&
		![Gui.secondWeapBtn2 pointIntersects:touchLoc] &&
		![Gui.pauseBtn       pointIntersects:touchLoc])
	{
		[Session.player stopMoving];
	}
}

@end
