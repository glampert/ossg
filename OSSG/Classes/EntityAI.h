//
//  EntityAI.h
//  OSSG
//
//  Created by Guilherme R. Lampert on 19/04/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "OSSG.h"

// ======================================================
// Enums / constants / helpers:
// ======================================================

// AI implementations available:
typedef enum eAIType {
	AI_NONE,
	AI_HOMING_MISSILE, // Homing missile "AI". Can seek a target.
	AI_STRAIGHT_MOVER, // Moves in a strait line, from right to left.
	AI_WAVE_MOVER,     // Wavey mover. Dances up and down is a sine wave.
	AI_DRONE_MINE,     // Mine. Does not fires weapons. Damages player on collision.
	AI_BOSS
} AIType;

// ======================================================
// EntityAI interface:
// ======================================================

//
// Base interface for any entity that has some sort of
// automated behavior. AKA "Artificial Intelligence".
//
@interface EntityAI : NSObject

// Public properties, inherited by specialized classes:
@property(nonatomic, weak) GameEntity * owner;
@property(nonatomic) AIType type;

// Public interface:
+ (EntityAI *) createAIOfType: (AIType) tp;
- (id)         initWithType:   (AIType) tp;
- (void)       think;
- (BOOL)       acquireTarget;
- (EntityAI *) clone;

@end
