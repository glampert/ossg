//
//  EntityAI.m
//  OSSG
//
//  Created by Guilherme R. Lampert on 19/04/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "EntityAI.h"
#import "EnemyAIs.h"

// ======================================================
// EntityAI implementation:
// ======================================================

@implementation EntityAI

//
// Methods:
//

- (BOOL) acquireTarget
{
	// Try to acquire nearest/best target.
	// Returns YES/NO to tell caller if a
	// target was acquired or not.
	return NO;
}

+ (EntityAI *) createAIOfType: (AIType) tp
{
	switch (tp)
	{
	case AI_HOMING_MISSILE :
		return [[AIHomingMissile alloc] initWithType:tp];

	case AI_STRAIGHT_MOVER :
		return [[AIStraightMover alloc] initWithType:tp];

	case AI_WAVE_MOVER :
		return [[AIWaveMover alloc] initWithType:tp];

	case AI_DRONE_MINE :
		return [[AIDroneMine alloc] initWithType:tp];

	case AI_BOSS :
		return [[AIBoss alloc] initWithType:tp];

	default :
		return nil;
	} // switch (tp)
}

- (id) initWithType: (AIType) tp
{
	self = [super init];
	if (!self)
	{
		return nil;
	}

	// Init instance variables:
	_owner = nil;
	_type  = tp;

	return self;
}

- (EntityAI *) clone
{
	EntityAI * ai = [EntityAI new];

	ai.owner = self.owner;
	ai.type  = self.type;

	return ai;
}

- (void) think
{
}

@end
