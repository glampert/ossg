//
//  GameGUI.h
//  OSSG
//
//  Created by Guilherme R. Lampert on 22/04/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "OSSG.h"

// ======================================================
// Enums / forward declarations:
// ======================================================

@class GameStage;

// UI text labels/strings:
typedef enum eUILabelId {
	UI_LBL_STAGE_NUM,
	UI_LBL_PLAYER_CREDITS,
	UI_LBL_SBAR_BATTERIES,
	UI_LBL_SBAR_MISSILES,
	UI_LBL_PERF_STATS,
	UI_NUM_LABELS
} UILabelId;

// ======================================================
// CustomControl interface:
// ======================================================

//
// Custom user interface control.
// Usually an interactive push button or a on/off switch button.
//
@interface CustomControl : NSObject

// Properties:
@property(nonatomic, readonly) CCButton   * button;
@property(nonatomic, readonly) CCSprite   * backgroundNormal;
@property(nonatomic, readonly) CCSprite   * backgroundHighlighted;
@property(nonatomic, readonly) CCSprite   * buttonIcon;
@property(nonatomic, readonly) CCLabelTTF * tooltip;
@property(nonatomic) BOOL hidden; // By default: NO

// on/off switch buttons:
@property(nonatomic) BOOL isSwitchButton;    // By default: NO
@property(nonatomic) BOOL alwaysShowIcon;    // For switch buttons, always display the icon, even if not selected. By default: NO
@property(nonatomic) BOOL switchButtonState; // State of switch buttons. By default: NO
@property(nonatomic, copy) void(^onToggle)(CustomControl * button, BOOL state); // For toggleable switch buttons: Called when the switch state changes.

// Create button controls:
+ (CustomControl *) createButton: (NSString *)title frontName: (NSString *)fName fontSize: (float) fSize spriteName: (NSString *)sName;
+ (CustomControl *) createButtonWithBgOnly: (NSString *)spriteName scaleX: (float)sx scaleY: (float)sy;

// Misc instance methods:
- (CGRect) getBoundingBox;
- (void)   resetStates;
- (void)   frameUpdate;
- (void)   addButtonIcon:       (NSString *)iconSprite scaleX: (float)sx scaleY: (float)sy;
- (void)   addTooltip:          (NSString *) text fontSize: (float) size;
- (BOOL)   pointIntersects:     (CGPoint) pt;
- (void)   setPosition:         (CGPoint) pos;
- (void)   setBackgroundScaleX: (float) sx andY: (float) sy;
- (void)   setButtonIconScaleX: (float) sx andY: (float) sy;
- (void)   setTooltipScaleX:    (float) sx andY: (float) sy;
- (void)   setTextOffsetX:      (float) x  andY: (float) y;
- (void)   setTextColor:        (CCColor  *) c;
- (void)   setTooltipText:      (NSString *) text;
- (void)   linkToScene:         (CCScene  *) gs;
- (void)   removeFromScene:     (CCScene  *) gs;

@end

// ======================================================
// CustomTextField interface:
// ======================================================

//
// Custom text input field.
// Extends CCTextField to add our own layout.
//
@interface CustomTextField : CCTextField

// Create new text filed with the game's layout.
+ (CustomTextField *) createTextFieldWithInitialText: (NSString *)text;

@end

// ======================================================
// ListMenu interface:
// ======================================================

// Max entries in a ListMenu.
#define MAX_LIST_MENU_ENTRIES 4

// Max length in chars of the text string of a ListMenu entry.
#define MAX_LIST_MENU_ENTRY_TEXT_LEN 15

//
// List of item menu.
// Each entry is also a switch button control.
//
@interface ListMenu : NSObject

// Properties:
@property(nonatomic, readonly) CCSprite * background;

// Instance methods:
- (id)     init;
- (void)   frameUpdate;
- (CGRect) getBoundingBox;
- (void)   setPosition:         (CGPoint) pos;
- (void)   setBackgroundScaleX: (float) sx andY: (float) sy;
- (void)   resetAllControls;
- (void)   linkToScene:         (CCScene *) gs;
- (void)   removeFromScene:     (CCScene *) gs;

// Returns the first entry that has a selected state, or nil if none.
- (CustomControl *) getFirstSelectedEntry;

// Access a given entry:
- (CustomControl *) getEntry: (int) index;

// Get the index of the given entry. -1 if entry not found.
- (int) getEntryIndex: (const CustomControl *) entry;

// Enables an entry in the list.
- (void) setEntry: (int) index withText: (NSString *) text andCallback: (void(^)(CustomControl *, BOOL)) onToggle;

// Cap text to MAX_LIST_MENU_ENTRY_TEXT_LEN.
+ (NSString *) truncateTextToFitListEntry: (NSString *) text;

@end

// ======================================================
// MenuScreen interface:
// ======================================================

// Up to four successors menus for each MenuScreen.
#define MAX_NEXT_MENUS 4

//
// Container of a menu and its controls.
// Used outside of a GameStage. Inherits from CCScene.
//
@interface MenuScreen : CCScene

// Class (static) methods:
+ (float) width;
+ (float) height;
+ (float) borderX;
+ (float) borderY;

// If these blocks are, they are executed before switching to the next/prev menus.
// If the block returns NO, the menu switch is canceled.
@property(nonatomic, copy) BOOL(^onNext)(MenuScreen * menu, int nextIndex);
@property(nonatomic, copy) BOOL(^onPrev)(MenuScreen * menu);

// Blocks called when the menu is entered and exited.
// Passed parameter is the menu being entered/exited.
@property(nonatomic, copy) void(^onMenuEnter)(MenuScreen * menu);
@property(nonatomic, copy) void(^onMenuExit)(MenuScreen * menu);

// Instance methods:
- (id)   init;
- (void) update: (CCTime) deltaTime;   // Implemented from CCScene.
- (BOOL) swithToPrevMenu;              // Does nothing if no prev menu to switch to.
- (BOOL) swithToNextMenu: (int) index; // Does nothing if no next menu to switch to.
- (void) resetAllControls;

// Big label that can be added to the right of the menu panel.
- (void) setRightSideLabel: (NSString *) text fontSize: (float) size;
- (void) setRightSideLabel: (NSString *) text fontSize: (float) size position: (CGPoint) pos;
- (CCLabelTTF *) getRightSideLabel;

// Small label that can be added at the top of the menu panel.
- (void) setTopSideLabel: (NSString *) text fontSize: (float) size;
- (void) setTopSideLabel: (NSString *) text fontSize: (float) size position: (CGPoint) pos;
- (CCLabelTTF *) getTopSideLabel;

// Create and link a new standard button to the menu:
- (CustomControl *) newButton: (NSString *) title smallButton: (BOOL) small;

// Create and link a two-state switch button. Useful for toggleable configs.
- (CustomControl *) newSwitchButton: (NSString *) tooltip;
- (CustomControl *) newSwitchButton: (NSString *) tooltip withIcon: (NSString *) iconName;

// Create and link a list menu to this menu panel. Only one list menu per menu screen!
- (ListMenu *) newListMenu;

// Create, link and position a new text field.
- (CustomTextField *) newTextFieldAt: (CGPoint) pos withText: (NSString *) text andTitle: (NSString *) title;

// Set next/prev menus:
- (void) setNextMenu: (MenuScreen *) next; // Takes menu slot 0
- (void) setPrevMenu: (MenuScreen *) prev;

// Same as the setters above, but allow the use of a custom button, instead of the default one.
- (void) setNextMenu: (MenuScreen *) next index: (int) i withButton: (CustomControl *) btn;
- (void) setPrevMenu: (MenuScreen *) prev withButton: (CustomControl *) btn;

@end

// ======================================================
// GameGUI interface:
// ======================================================

//
// Game UI manager.
//
// GameGUI is a singleton. The unique global instance
// must be accessed via the 'Gui' global variable.
// If you try to create an instance of GameGUI yourself,
// you'll get and assertion.
//
@interface GameGUI : NSObject

// In-game buttons:
@property(nonatomic, readonly, strong) CustomControl * mainWeapBtn;    // Main weapon
@property(nonatomic, readonly, strong) CustomControl * secondWeapBtn1; // Secondary weapon 1
@property(nonatomic, readonly, strong) CustomControl * secondWeapBtn2; // Secondary weapon 2
@property(nonatomic, readonly, strong) CustomControl * pauseBtn;       // Call pause menu from the game

// Methods:
- (id)   init;
- (void) labelPrint: (UILabelId) labelId format: (NSString *) text, ...;
- (void) perStageSetup: (GameStage *) gs;
- (void) frameUpdate;
- (void) fadeScreen;

// Pause menu popup:
- (void) showPauseMenu;
- (void) hidePauseMenu;
- (BOOL) isPauseMenuOpen;

// End-stage menu popup:
- (void) showEndGameMenu: (BOOL) successfulStage;
- (void) hideEndGameMenu;
- (BOOL) isEndGameMenuOpen;

@end
