//
//  CustomGLProgs.m
//  OSSG - Inline GLSL shaders.
//
//  Created by Guilherme R. Lampert on 30/04/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "OSSG.h"

// ===================================================================================================

//
// GLPROG_COLOR_REPLACE:
//

// ======================================================
// Vertex Shader:
// ======================================================

const char GLPROG_COLOR_REPLACE_vs[] =
"\n\
precision lowp float;\n\
\n\
attribute mediump vec4 a_position;\n\
attribute mediump vec2 a_texCoord;\n\
attribute lowp    vec4 a_color;\n\
\n\
uniform mediump mat4 u_MVPMatrix;\n\
\n\
varying lowp vec4 v_fragmentColor;\n\
varying mediump vec2 v_texCoord;\n\
\n\
void main()\n\
{\n\
    gl_Position     = (u_MVPMatrix * a_position);\n\
    v_fragmentColor = a_color;\n\
    v_texCoord      = a_texCoord;\n\
}\n\
";

// ======================================================
// Fragment Shader:
// ======================================================

const char GLPROG_COLOR_REPLACE_fs[] =
"\n\
precision mediump float;\n\
\n\
const float c_zero         =  0.0;\n\
const float c_one          =  1.0;\n\
const float c_three        =  3.0;\n\
const float c_minusOne     = -1.0;\n\
const float c_twoOverThree = (2.0 / 3.0);\n\
\n\
vec3 rgb2hsv(vec3 c)\n\
{\n\
    vec4 K = vec4(c_zero, c_minusOne / c_three, c_twoOverThree, c_minusOne);\n\
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));\n\
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));\n\
    float d = q.x - min(q.w, q.y);\n\
    float e = 1.0e-10;\n\
    return (vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x));\n\
}\n\
\n\
vec3 hsv2rgb(vec3 c)\n\
{\n\
    vec4 K = vec4(c_one, c_twoOverThree, c_one / c_three, c_three);\n\
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);\n\
    return (c.z * mix(K.xxx, clamp(p - K.xxx, c_zero, c_one), c.y));\n\
}\n\
\n\
varying lowp    vec4 v_fragmentColor;\n\
varying mediump vec2 v_texCoord;\n\
\n\
uniform mediump float u_colorReplaceThreshold;\n\
uniform mediump vec3  u_hsvToReplace;\n\
uniform mediump vec3  u_hsvReplaceWith;\n\
\n\
// Maintain compatibility with Cocos2d by using their variable name prefix (CC_)\n\
uniform sampler2D CC_Texture0;\n\
\n\
void main()\n\
{\n\
    vec4 finalColor = v_fragmentColor * texture2D(CC_Texture0, v_texCoord);\n\
    vec3 hsv = rgb2hsv(finalColor.rgb);\n\
    if (abs(hsv.x - u_hsvToReplace.x) <= u_colorReplaceThreshold)\n\
    {\n\
        hsv.x = u_hsvReplaceWith.x;\n\
    }\n\
    finalColor.rgb = hsv2rgb(hsv);\n\
    gl_FragColor = finalColor;\n\
}\n\
";

// ======================================================
// Vertex Attributes:
// ======================================================

static const VertexAttrib GLPROG_COLOR_REPLACE_attr0 = { "a_position", 0 };
static const VertexAttrib GLPROG_COLOR_REPLACE_attr1 = { "a_color",    1 };
static const VertexAttrib GLPROG_COLOR_REPLACE_attr2 = { "a_texCoord", 2 };
const VertexAttrib * GLPROG_COLOR_REPLACE_vtxAttribs[] = {
    &GLPROG_COLOR_REPLACE_attr0,
    &GLPROG_COLOR_REPLACE_attr1,
    &GLPROG_COLOR_REPLACE_attr2,
    NULL };

// ===================================================================================================

//
// GLPROG_FADE_SCREEN:
//

// ======================================================
// Vertex Shader:
// ======================================================

const char GLPROG_FADE_SCREEN_vs[] =
"\n\
precision lowp float;\n\
attribute lowp vec2 a_vertexPositionNDC;\n\
void main()\n\
{\n\
    gl_Position = vec4(a_vertexPositionNDC, 0.0, 1.0);\n\
}\n\
";

// ======================================================
// Fragment Shader:
// ======================================================

const char GLPROG_FADE_SCREEN_fs[] =
"\n\
precision lowp float;\n\
void main()\n\
{\n\
    gl_FragColor = vec4(0.0, 0.0, 0.0, 0.5);\n\
}\n\
";

// ======================================================
// Vertex Attributes:
// ======================================================

static const VertexAttrib GLPROG_FADE_SCREEN_attr0 = { "a_vertexPositionNDC", 0 };
const VertexAttrib * GLPROG_FADE_SCREEN_vtxAttribs[] = { &GLPROG_FADE_SCREEN_attr0, NULL };

// ===================================================================================================

//
// GLPROG_BACKGROUND_OBJECTS:
//

// ======================================================
// Vertex Shader:
// ======================================================

const char GLPROG_BACKGROUND_OBJECTS_vs[] =
"\n\
precision mediump float;\n\
\n\
attribute highp vec3 a_position;\n\
attribute highp vec3 a_normal;\n\
attribute highp vec2 a_texCoord;\n\
\n\
uniform highp mat4  u_MVPMatrix;\n\
uniform highp vec3  u_lightPos;\n\
uniform highp vec3  u_ambientContrib;\n\
uniform highp float u_specularConcentration;\n\
uniform highp float u_materialScale;\n\
\n\
varying lowp vec3 v_diffuseLight;\n\
varying lowp vec3 v_specularLight;\n\
varying mediump vec2 v_texCoord;\n\
\n\
void main()\n\
{\n\
    gl_Position = u_MVPMatrix * vec4(a_position, 1.0);\n\
\n\
    highp vec3 lightDirection = normalize(u_lightPos - a_position);\n\
    v_diffuseLight  = vec3(max(dot(a_normal, lightDirection), 0.0)) + u_ambientContrib;\n\
    v_specularLight = vec3(max((v_diffuseLight.x - u_specularConcentration) * u_materialScale, 0.0));\n\
\n\
    v_texCoord = a_texCoord;\n\
}\n\
";

// ======================================================
// Fragment Shader:
// ======================================================

const char GLPROG_BACKGROUND_OBJECTS_fs[] =
"\n\
precision lowp float;\n\
\n\
uniform sampler2D s_baseMap;\n\
\n\
varying lowp    vec3 v_diffuseLight;\n\
varying lowp    vec3 v_specularLight;\n\
varying mediump vec2 v_texCoord;\n\
\n\
void main()\n\
{\n\
    lowp vec4 texColor = texture2D(s_baseMap, v_texCoord);\n\
    lowp vec3 color    = (texColor.rgb * v_diffuseLight) + v_specularLight;\n\
    gl_FragColor       = vec4(color, min(texColor.a + 0.2, 1.0));\n\
}\n\
";

// ======================================================
// Vertex Attributes:
// ======================================================

static const VertexAttrib GLPROG_BACKGROUND_OBJECTS_attr0 = { "a_position", 0 };
static const VertexAttrib GLPROG_BACKGROUND_OBJECTS_attr1 = { "a_normal",   1 };
static const VertexAttrib GLPROG_BACKGROUND_OBJECTS_attr2 = { "a_texCoord", 2 };
const VertexAttrib * GLPROG_BACKGROUND_OBJECTS_vtxAttribs[] = {
    &GLPROG_BACKGROUND_OBJECTS_attr0,
    &GLPROG_BACKGROUND_OBJECTS_attr1,
    &GLPROG_BACKGROUND_OBJECTS_attr2,
    NULL };

// ===================================================================================================

//
// GLPROG_BACKGROUND_IMAGE:
//

// ======================================================
// Vertex Shader:
// ======================================================

const char GLPROG_BACKGROUND_IMAGE_vs[] =
"\n\
precision lowp float;\n\
const vec2 scale = vec2(0.5, 0.5);\n\
attribute lowp vec2 a_vertexPositionNDC;\n\
varying   lowp vec2 v_texCoords;\n\
void main()\n\
{\n\
    gl_Position = vec4(a_vertexPositionNDC, 0.0, 1.0);\n\
    v_texCoords = a_vertexPositionNDC * scale + scale;\n\
}\n\
";

// ======================================================
// Fragment Shader:
// ======================================================

const char GLPROG_BACKGROUND_IMAGE_fs[] =
"\n\
precision lowp float;\n\
uniform sampler2D s_baseMap;\n\
varying lowp vec2 v_texCoords;\n\
void main()\n\
{\n\
    gl_FragColor = texture2D(s_baseMap, v_texCoords);\n\
}\n\
";

// ======================================================
// Vertex Attributes:
// ======================================================

static const VertexAttrib GLPROG_BACKGROUND_IMAGE_attr0 = { "a_vertexPositionNDC", 0 };
const VertexAttrib * GLPROG_BACKGROUND_IMAGE_vtxAttribs[] = { &GLPROG_BACKGROUND_IMAGE_attr0, NULL };

// ===================================================================================================
