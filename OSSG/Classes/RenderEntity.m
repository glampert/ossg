//
//  RenderEntity.m
//  OSSG
//
//  Created by Guilherme R. Lampert on 27/03/2014.
//
//  This source code is released under the MIT License.
//  Copyright (c) 2014 Guilherme R. Lampert.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import <math.h>
#import "RenderEntity.h"
#import "CCAnimation.h"
#import "rgb2hsv.h"

// ======================================================
// Local helpers:
// ======================================================

// Initial capacity of animation dictionaries:
#define ANIM_DICT_INITIAL_CAP         5
#define SPRITE_SHEET_DICT_INITIAL_CAP 5
#define ACTION_TAG_ANIM               1    /* Tag used for CCActionAnimate */
#define COLOR_REPLACE_THRESHOLD       1.0f /* For damage effects [0,1]     */

// Set default clear screen color.
#define SetDefaultBackgroundColor() glClearColor(0.0f, 0.0f, 0.0f, 1.0f)

// Global sprites sheets currently loaded.
// Initialized by first call to [RenderEntity loadSpriteSheet].
static NSMutableDictionary * spriteSheets = nil;

// ======================================================
// Custom damage effect rendering:
// ======================================================

// Constants:
static const vec4_t cRed   = {1, 0, 0, 1};
static const vec4_t cGreen = {0, 1, 0, 1};
static const vec4_t cBlue  = {0, 0, 1, 1};

// Locals:
static float hsvToReplace[3];
static GLint u_MVPMatrix;
static GLint u_hsvToReplace;
static GLint u_hsvReplaceWith;
static GLint u_colorReplaceThreshold;

// Set custom shader prog and GL states:
static void SetDamageEffectRender(float damageAmount, const vec4_t clrA, const vec4_t clrB)
{
	// Set custom shader program:
	[GLProgMgr enableProgram:GLPROG_COLOR_REPLACE];

	// Set model-view-projection matrix:
	kmMat4 matrixP;
	kmMat4 matrixMV;
	kmMat4 matrixMVP;
	kmGLGetMatrix(KM_GL_PROJECTION, &matrixP);
	kmGLGetMatrix(KM_GL_MODELVIEW,  &matrixMV);
	kmMat4Multiply(&matrixMVP, &matrixP, &matrixMV);
	[GLProgMgr setProgramParam:u_MVPMatrix floatMat4:matrixMVP.mat];

	// Compute new color for damage effect:
	float hsvReplaceWith[3];
	const vec4_t cFinal = mix(clrA, clrB, damageAmount);
	rgb2hsv(cFinal.x, cFinal.y, cFinal.z, hsvReplaceWith);
	[GLProgMgr setProgramParam:u_hsvReplaceWith floatVec3:hsvReplaceWith];
}

// Return to Cocos2d drawing:
static void RestoreDefaultRender(void)
{
	// Restore Cocos2d shader program:
	[GLProgMgr restore];
}

// ======================================================
// Custom CCSpriteBatchNode:
// ======================================================

// This allows us to overwrite [CCSpriteBatchNode draw]
// and add our custom shader program for animated entities.
@interface MySpriteBatchNode : CCSpriteBatchNode
@property(nonatomic) int   entityType;
@property(nonatomic) float damageAmount;
@property(nonatomic) BOOL  displayDamageEffect;
@end

@implementation MySpriteBatchNode
- (void) draw
{
	if (self.displayDamageEffect)
	{
		SetDamageEffectRender(self.damageAmount, (self.entityType == GE_PLAYER) ? cGreen : cBlue, cRed);
		[super drawInternal];
		RestoreDefaultRender();
	}
	else
	{
		// Standard draw
		[super draw];
	}
}
@end

// ======================================================
// RenderEntity implementation:
// ======================================================

@implementation RenderEntity
{
	//
    // Private data:
	//
	CCMotionStreak      * trail;       // Optional trail render. Used by missiles and such.
	MySpriteBatchNode   * animBatch;   // Only used for animated sprites. 1 per entity!
	NSMutableDictionary * animations;  // Map of CCAnimations indexed by animation name.
	CGPoint               moveTarget;  // Target position. Lerp'd by [frameUpdate].
	float                 targetAngle; // Target rotation angle in radians. Lerp'd by [frameUpdate].
	BOOL                  addedToBatch;
}

//
// Methods:
//

+ (void) commonInit
{
	OSSG_ASSERT(GLProgMgr != nil);

	// Default clear screen color:
	SetDefaultBackgroundColor();

	// We can cache these stuff, since it never changes
	// during the app life cycle.

	rgb2hsv(cRed.x, cRed.y, cRed.z, hsvToReplace);

	u_MVPMatrix = [GLProgMgr getProgramParamHandle:GLPROG_COLOR_REPLACE paramName:@"u_MVPMatrix"];
	OSSG_ASSERT(u_MVPMatrix != -1);

	u_colorReplaceThreshold = [GLProgMgr getProgramParamHandle:GLPROG_COLOR_REPLACE paramName:@"u_colorReplaceThreshold"];
	OSSG_ASSERT(u_colorReplaceThreshold != -1);

	u_hsvToReplace = [GLProgMgr getProgramParamHandle:GLPROG_COLOR_REPLACE paramName:@"u_hsvToReplace"];
	OSSG_ASSERT(u_hsvToReplace != -1);

	u_hsvReplaceWith = [GLProgMgr getProgramParamHandle:GLPROG_COLOR_REPLACE paramName:@"u_hsvReplaceWith"];
	OSSG_ASSERT(u_hsvReplaceWith != -1);

	// These uniform variables won't change:
	[GLProgMgr enableProgram:GLPROG_COLOR_REPLACE];
		[GLProgMgr setProgramParam:u_colorReplaceThreshold floatValue:COLOR_REPLACE_THRESHOLD];
		[GLProgMgr setProgramParam:u_hsvToReplace floatVec3:hsvToReplace];
	[GLProgMgr restore];
}

+ (void) perStageSetup: (GameStage *) gs
{
	OSSG_ASSERT(gs != nil);

	// Link all sprite batches with the stage:
	for (NSString * sheetName in spriteSheets.allKeys)
	{
		[gs addChild:[spriteSheets objectForKey:sheetName] z:ESORT_ENTITY]; // Add to current stage.
	}
}

- (id) init
{
	self = [super init];
	if (!self)
	{
		return nil;
	}

	// Init instance variables:
	trail                    = nil;
	animBatch                = nil;
	animations               = nil;
	moveTarget               = CGPointZero;
	targetAngle              = 0.0f;
	addedToBatch             = NO;
	self.turnSpeed           = 0.0f;
	self.damageAmount        = 0.0f;
	self.acceleration        = 0.0f;
	self.rotation            = 0.0f;
	self.position            = CGPointZero;
	self.displayDamageEffect = NO;
	self.allowMovement       = YES;
	self.constraintToWorld   = NO;
	self.allowRotation       = YES;
	self.entityType          = GE_UNKNOWN;
	[self setEntitySort:ESORT_ENTITY];

	return self;
}

- (RenderEntity *) clone
{
	//
	// WARNING: This will not work for trail renderers and
	// animated entities! (animBatch/trail are ignored!)
	//

	RenderEntity * re = [RenderEntity createWithSpriteName:self.name];

	re->moveTarget         = moveTarget;
	re->targetAngle        = targetAngle;
	re.turnSpeed           = self.turnSpeed;
	re.damageAmount        = self.damageAmount;
	re.acceleration        = self.acceleration;
	re.rotation            = self.rotation;
	re.scale               = self.scale;
	re.position            = self.position;
	re.displayDamageEffect = self.displayDamageEffect;
	re.allowMovement       = self.allowMovement;
	re.constraintToWorld   = self.constraintToWorld;
	re.allowRotation       = self.allowRotation;
	re.entityType          = self.entityType;

	[re setEntitySort:[self getEntitySort]];

	return re;
}

+ (id) createEmpty
{
	return [RenderEntity emptySprite];
}

+ (id) createWithSpriteName: (NSString *) spriteName;
{
	OSSG_ASSERT(spriteName != nil);

	// Append default extension if not present:
	NSString * fileExt = [spriteName pathExtension];
	if ([fileExt caseInsensitiveCompare:STD_SPRITE_SHEET_FILE_EXT] != NSOrderedSame)
	{
		spriteName = [spriteName stringByAppendingFormat:@".%@", STD_SPRITE_SHEET_FILE_EXT];
	}

	if (!ResourceExists(spriteName))
	{
		OSSG_ERROR(@"File '%@' is not present / does not name a file!", spriteName);
		return [RenderEntity createEmpty];
	}

	RenderEntity * renderEntity = [RenderEntity new];
	if (![renderEntity initWithImageNamed:spriteName])
	{
		OSSG_ERROR(@"Failed to load graphics from file '%@'!", spriteName);
		return [RenderEntity createEmpty];
	}

	renderEntity.name = spriteName;
	return renderEntity;
}

+ (BOOL) loadSpriteSheet: (NSString *) sheetName
{
	OSSG_ASSERT(sheetName != nil);

	if ((spriteSheets != nil) && ([spriteSheets objectForKey:sheetName] != nil))
	{
		// Already loaded, do nothing.
		return YES;
	}

	NSString * sheetNameTex = [NSString stringWithFormat:@"%@.%@", sheetName, STD_SPRITE_SHEET_FILE_EXT];
	if (!ResourceExists(sheetNameTex))
	{
		OSSG_ERROR(@"File '%@' is not present / does not name a file!", sheetName);
		return NO;
	}

	MySpriteBatchNode * batch = [MySpriteBatchNode batchNodeWithFile:sheetNameTex];
	if (!batch)
	{
		OSSG_ERROR(@"Failed to load sprite sheet from file '%@'!", sheetName);
		return NO;
	}

	if (spriteSheets == nil)
	{
		spriteSheets = [NSMutableDictionary dictionaryWithCapacity:SPRITE_SHEET_DICT_INITIAL_CAP];
	}

	[spriteSheets setObject:batch forKey:sheetName];
	return YES;
}

+ (void) unloadAllSpriteSheets
{
	// ARC should free the object.
	[spriteSheets removeAllObjects];
	spriteSheets = nil;
}

- (BOOL) loadAnimation: (NSArray *) frameNames spriteSheet: (NSString *) sheet animName: (NSString *) anim frameDelay: (float) seconds bindPose: (BOOL) yesNo
{
	OSSG_ASSERT(frameNames != nil);
	OSSG_ASSERT(sheet      != nil);
	OSSG_ASSERT(anim       != nil);
	OSSG_ASSERT([frameNames count] > 0);

	MySpriteBatchNode * batch = [spriteSheets objectForKey:sheet];
	if (batch == nil)
	{
		OSSG_ERROR(@"Sprite sheet not found! Did you [loadSpriteSheet] first?");
		return NO;
	}

	if (animBatch != nil)
	{
		if (animBatch != batch)
		{
			OSSG_ERROR(@"Each RenderEntity can only reference a single sprite sheet!");
			return NO;
		}
	}

	NSString * sheetNamePList = [NSString stringWithFormat:@"%@.plist", sheet];
	[[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:sheetNamePList];

	CCSpriteFrameCache * cache = [CCSpriteFrameCache sharedSpriteFrameCache];
	NSMutableArray * frameList = [NSMutableArray array];
	for (NSString * frameName in frameNames)
	{
		id sf = [cache spriteFrameByName:frameName];
		if (!sf)
		{
			// Sprite sheet PLIST and entity definition PLIST are probably not in synch!
			OSSG_ERROR(@"Sprite frame '%@' not found inside PLIST!", frameName);
		}
		[frameList addObject:sf];
	}

	CCAnimation * animation = [CCAnimation animationWithSpriteFrames:frameList delay:seconds];

	// Makes the first frame of this animation the "bind pose"
	// of the entity. I.e. The default frame.
	if (/* bindPose */ yesNo)
	{
		self.spriteFrame = [frameList objectAtIndex:0];
	}

	// Init animation dictionary for the first time if needed:
	if (animations == nil)
	{
		animations = [NSMutableDictionary dictionaryWithCapacity:ANIM_DICT_INITIAL_CAP];
	}

	[animations setObject:animation forKey:anim];

	animBatch = batch;
	animBatch.entityType = self.entityType;
	return YES;
}

- (BOOL) playAnimation: (NSString *) animName withMode: (AnimPlayMode) mode
{
	OSSG_ASSERT(animName != nil);

	if (animations == nil)
	{
		return NO;
	}

	CCAnimation * animation = [animations objectForKey:animName];
	if (animation == nil)
	{
		return NO;
	}

	CCAction * action;
	switch (mode)
	{
	case ANIM_PLAY_ONCE :
		action = [CCActionAnimate actionWithAnimation:animation];
		action.tag = ACTION_TAG_ANIM;
		[self runAction:action];
		return YES;

	case ANIM_PLAY_LOOP :
		action = [CCActionRepeatForever actionWithAction:[CCActionAnimate actionWithAnimation:animation]];
		action.tag = ACTION_TAG_ANIM;
		[self runAction:action];
		return YES;

	default :
		OSSG_ERROR(@"Invalid AnimPlayMode!");
		return NO;
	} // switch (mode)
}

- (void) stopAnimation
{
	[self stopActionByTag:ACTION_TAG_ANIM];
}

- (void) enableTrailRender: (const TrailRenderParams *) params
{
	if (params)
	{
		// Append default extension if not present:
		NSString * spriteName = [NSString stringWithCString:params->textureFilename encoding:NSUTF8StringEncoding];
		NSString * fileExt = [spriteName pathExtension];
		if ([fileExt caseInsensitiveCompare:STD_SPRITE_SHEET_FILE_EXT] != NSOrderedSame)
		{
			spriteName = [spriteName stringByAppendingFormat:@".%@", STD_SPRITE_SHEET_FILE_EXT];
		}

		// Create the streak object and add it to the scene:
		trail = [CCMotionStreak streakWithFade:params->fadeTimeSec
		                                minSeg:params->minSegments
		                                 width:params->width
		                                 color:[CCColor colorWithRed:params->r green:params->g blue:params->b alpha:params->a]
		                       textureFilename:spriteName];

		trail.fastMode = params->fastMode;
		trail.zOrder   = (NSInteger)ESORT_TRAIL;
		[Stage addChild:trail];
	}
	else
	{
		// Trail was temporarily disabled and is being enabled again
		if (trail != nil)
		{
			trail.zOrder = (NSInteger)ESORT_TRAIL;
			[Stage addChild:trail];
		}
	}
}

- (void) disableTrailRender: (BOOL) fullCleanup
{
	if (trail == nil)
	{
		return;
	}

	[Stage removeChild:trail cleanup:YES];

	if (fullCleanup)
	{
		trail = nil;
		// Let ARC free the object
	}
}

- (BOOL) isTrailRenderEnabled
{
	return trail != nil;
}

- (void) frameUpdate
{
	if (self.allowMovement)
	{
		// Constraint movement to world area:
		if (self.constraintToWorld)
		{
			const CGRect bbox = [self boundingBox];

			if ((moveTarget.y + bbox.size.height) > WorldBounds.size.height)
			{
				moveTarget.y = WorldBounds.size.height - bbox.size.height/2;
			}
			if ((moveTarget.y - bbox.size.height) < WorldBounds.origin.y)
			{
				moveTarget.y = WorldBounds.origin.y + bbox.size.height/2;
			}
			if ((moveTarget.x + bbox.size.width) > WorldBounds.size.width)
			{
				moveTarget.x = WorldBounds.size.width - bbox.size.width/2;
			}
			if ((moveTarget.x - bbox.size.width) < WorldBounds.origin.x)
			{
				moveTarget.x = WorldBounds.origin.x + bbox.size.width/2;
			}
		}

		// Lerp:
		const CGPoint currentPos = self.position;
		if (!PointEq(&currentPos, &moveTarget))
		{
			CGPoint newPos;
			PointLerp(&newPos, &currentPos, &moveTarget, (Time.deltaTime * self.acceleration));
			self.position = newPos;
		}
	}

	if (self.allowRotation)
	{
		float currentAngle = CC_DEGREES_TO_RADIANS(self.rotation);
		if (currentAngle != targetAngle)
		{
			// Based on StackOverflow question
			// "How do I lerp between values that loop":
			// http://gamedev.stackexchange.com/questions/72348/how-do-i-lerp-between-values-that-loop-such-as-hue-or-rotation

			float dtheta = (targetAngle - currentAngle);
			if (dtheta > M_PI)
			{
				currentAngle += (2 * M_PI);
			}
			else if (dtheta < -M_PI)
			{
				currentAngle -= (2 * M_PI);
			}

			const float easing   = 0.5f;
			const float maxAccel = (Time.deltaTime * self.turnSpeed);

			// Cap acceleration to eliminate jerkiness:
			const float targetVel = (targetAngle - currentAngle) * easing;
			const float velocity  = ClampF(targetVel, -maxAccel, +maxAccel);

			// Move with clipped velocity:
			currentAngle += velocity;
			self.rotation = CC_RADIANS_TO_DEGREES(currentAngle);
		}
	}

	if (trail != nil)
	{
		trail.position = self.position;
	}

	if (animBatch != nil)
	{
		animBatch.displayDamageEffect = self.displayDamageEffect;
		animBatch.damageAmount = self.damageAmount;
	}
}

- (void) moveToPosition: (CGPoint) pos setTargetAngle: (BOOL) yesNo
{
	moveTarget = pos;

	if (/* setTargetAngle */ yesNo)
	{
		const float dx = (moveTarget.x - self.position.x);
		const float dy = (moveTarget.y - self.position.y);
		targetAngle = atan2(dx, dy);
	}
}

- (void) stopMoving
{
	moveTarget = self.position;
}

- (void) setTargetAngle: (float) degrees
{
	targetAngle = CC_DEGREES_TO_RADIANS(degrees);
}

- (void) setEntitySort: (EntitySortOrder) sort
{
	self.zOrder = (NSInteger)sort;
}

- (EntitySortOrder) getEntitySort
{
	return (EntitySortOrder)self.zOrder;
}

- (void) getDamageColorRed: (float *)r green: (float *)g blue: (float *)b
{
	if (self.entityType == GE_ITEM_POWERUP)
	{
		*r = 0.0f;
		*g = 0.8f;
		*b = 0.7f;
	}
	else
	{
		const vec4_t * clrB = &cRed;
		const vec4_t * clrA = (self.entityType == GE_PLAYER) ? &cGreen : &cBlue;
		const vec4_t cFinal = mix(*clrA, *clrB, self.damageAmount);
		*r = cFinal.x;
		*g = cFinal.y;
		*b = cFinal.z;
	}
}

- (void) stageLink: (GameStage *) gs
{
	OSSG_ASSERT(gs != nil);

	// Cocos2d particularity:
	// When using a sprite batch/sheet, we need to attach
	// the sprite to the batch node, not the scene.
	if (animBatch != nil)
	{
		if (!addedToBatch)
		{
			[animBatch addChild:self];
			addedToBatch = YES;
		}
	}
	else
	{
		[gs addChild:self];
	}
}

- (void) stageUnlink: (GameStage *) gs
{
	OSSG_ASSERT(gs != nil);

	if (animBatch != nil)
	{
		if (addedToBatch)
		{
			[animBatch removeChild:self cleanup:YES];
			addedToBatch = NO;
		}
	}
	else
	{
		[gs removeChild:self cleanup:YES];
	}
}

- (void) draw
{
	if (self.displayDamageEffect)
	{
		// Custom draw:
		SetDamageEffectRender(self.damageAmount, (self.entityType == GE_PLAYER) ? cGreen : cBlue, cRed);
		[super drawInternal];
		RestoreDefaultRender();
	}
	else
	{
		// Standard draw:
		[super draw];
	}
}

@end
