//
//  main.m
//  Application entry point.
//
//  Created by Guilherme R. Lampert on 27/03/2014.
//  Copyright Guilherme R. Lampert 2014. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char * argv[])
{
	@autoreleasepool
	{
		return UIApplicationMain(argc, argv, nil, @"AppDelegate");
	}
}
