
# OSSG - Oldschool Space Shooter Game

----

## About:

This is a small Objective-C/iOS Space Shooter Game I've written for a University class assignment.
It was my first attempt at Objective-C and iOS development.

The only dependency is [Cocos2d-iphone](https://github.com/cocos2d/cocos2d-iphone),
which is included in the project and comes with its own license.

Here is a screenshot of the game:

![OSSG gameplay](https://bytebucket.org/glampert/ossg/raw/e3707daabda16d5c89b370436dc4a88f695375f7/Screens/iOS-simulator-screenshot-4.png "OSSG Gameplay")

And [this link](https://www.youtube.com/watch?v=YQTbddA4IYw) has a gameplay video.

----

This game was developed for academic and non-commercial purposes. Some art assets and
sounds used in this game, however, might be copyrighted and unsuitable for commercial use.
Bare that in mind if copying any of them.

## License:

This project's source code is released under the [MIT License](http://opensource.org/licenses/MIT).

Space ship sprites are from [MillionthVector](http://millionthvector.blogspot.de).
Work released under the [*Creative Commons BY* License](https://creativecommons.org/licenses/by/4.0/).

